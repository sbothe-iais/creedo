package de.unibonn.creedo.ui.core;

import java.util.List;

import org.springframework.ui.Model;

/**
 * Components that can be added to instances of {@link DefaultPageFrame}.
 * 
 * @author mboley
 *
 */
public interface UiComponent {
	
	public abstract int getId();

	public abstract String getView();

	public abstract Model getModel();

	public abstract List<UiComponent> getComponents();

}

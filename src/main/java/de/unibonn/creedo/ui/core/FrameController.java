package de.unibonn.creedo.ui.core;

import java.util.Arrays;

import de.unibonn.creedo.Creedo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
public class FrameController {

	@Autowired
	private HttpSession session;

	@RequestMapping(value = "/showFrame.htm", method = RequestMethod.GET)
	public ModelAndView showFrame(@RequestParam(value = "frameId") int frameId) {

		UiComponent component = Creedo.getCreedoSession(session)
				.getUiComponent(frameId);

		if (!(component instanceof Frame)) {
			throw new IllegalArgumentException("No frame found with id "
					+ frameId);
		}

		return ((Frame) component).getModelAndView();
	}

	@RequestMapping(value = "/performAction.htm", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	public ResponseEntity<String> performAction(
			@RequestParam(value = "frameId") int actionProviderId,
			@RequestParam(value = "linkId") int actionId,
			@RequestParam(value = "parameter[]", required = false) String[] params) {
//		System.out.println("peforming " + actionId + " of provider "
//				+ actionProviderId
//				+ ((params != null) ? " with parameters " + Arrays.asList(params) : ""));
		UiComponent component = Creedo.getCreedoSession(session)
				.getUiComponent(actionProviderId);
		if (!(component instanceof ActionProvider)) {
			throw new IllegalArgumentException(
					"Component does not provide actions.");
		}
		return ((ActionProvider) component).performAction(actionId, params);
	}
}

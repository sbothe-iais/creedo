package de.unibonn.creedo.ui.core;

import org.springframework.web.servlet.ModelAndView;

public interface Frame extends UiComponent {

	public ModelAndView getModelAndView();

	public int getId();

}

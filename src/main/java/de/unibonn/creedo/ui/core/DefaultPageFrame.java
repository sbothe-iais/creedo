package de.unibonn.creedo.ui.core;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.servlet.ModelAndView;

import de.unibonn.creedo.common.ConfigurationProperties;
import de.unibonn.creedo.ui.standard.Footer;
import de.unibonn.creedo.ui.standard.Navbar;

/**
 * Default frame that can display pages and links to actions in a navbar and a
 * footer.
 * 
 * @author mboley
 *
 */
public class DefaultPageFrame implements PageFrame, ActionProvider {

	private final int id;

	private final List<UiComponent> components;

	private final PageContainer pageContainer;

	public DefaultPageFrame(int id, PageContainer pageContainer, Navbar navbar,
			Footer footer) {
		this.id = id;
		this.pageContainer = pageContainer;

		this.components = Arrays.asList((UiComponent) navbar,
				(UiComponent) pageContainer, (UiComponent) footer);
	}

	@Override
	public ModelAndView getModelAndView() {
		return new ModelAndView(getView(), getModel().asMap());
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public Collection<Integer> getActionIds() {
		throw new UnsupportedOperationException();
	}

	@Override
	public ResponseEntity<String> performAction(int id, String... params) {
		for (UiComponent component : components) {
			if (component instanceof ActionProvider
					&& ((ActionProvider) component).isActionAvailable(id)) {
				return ((ActionProvider) component).performAction(id);
			}
		}
		throw new IllegalArgumentException("No action with id " + id);
	}

	@Override
	public boolean isActionAvailable(int id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void loadPage(Page page) {
		this.pageContainer.loadPage(page);
	}

	@Override
	public String getView() {
		return "static/contentPageFrame";
	}

	@Override
	public Model getModel() {
		ConfigurationProperties config = ConfigurationProperties.get();

		Model model = new BindingAwareModelMap();
		model.addAttribute("frameId", id);
		model.addAttribute("applicationTitlePrefix", config.TITLE_PREFIX);
		model.addAttribute("components", components);

		for (UiComponent component : components) {
			model.addAllAttributes(component.getModel().asMap());
		}
		return model;
	}

	@Override
	public List<UiComponent> getComponents() {
		return components;
	}

}

package de.unibonn.creedo.ui.repository;

import java.util.Arrays;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import de.unibonn.creedo.common.DefaultSubCollectionParameter;
import de.unibonn.creedo.repositories.Repository;
import de.unibonn.creedo.repositories.RepositoryEntry;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;

public class RepositoryParameterUpdateAction<T> implements Action {

	private final int id;
	private final String pageBuilderId;
	private final Parameter<?> parameter;
	private final Repository<T> repository;

	// private Repository<T>

	public RepositoryParameterUpdateAction(int id, Repository<T> repository,
			String containerId, Parameter<?> parameter) {
		this.id = id;
		this.pageBuilderId = containerId;
		this.parameter = parameter;
		this.repository = repository;
	}

	@Override
	public String getReferenceName() {
		return "Update";
	}

	/**
	 * Assume that params[0] contains the value for all parameters but for
	 * subcollection parameters. For subcollection params[0],...,params[n] is
	 * supposed to contain the string representation of selected elements. In
	 * particular params is null if no element is select.
	 * 
	 * TODO: this contract is very shaky. Ideally params[0] should always
	 * contain the string representation
	 */
	@Override
	public ResponseEntity<String> activate(String... params) {
		// String pageBuilderId = actionIdEntryIdMap.get(id);
		// Parameter<?> parameter = actionIdToParameterMap.get(id);

		if (pageBuilderId == null || parameter == null) {
			throw new IllegalArgumentException("No action with id: " + id
					+ " found.");
		}
		// if (params.length != 1) {
		// throw new IllegalArgumentException(
		// "Expecting exactly one parameter.");
		// }

		RepositoryEntry<T> entry = repository.get(pageBuilderId);
		T content = entry.getContent();

		String value = null;
		if (parameter instanceof DefaultSubCollectionParameter) {
			if (params == null) {
				value = "[]";
			} else {
				value = Arrays.asList(params).toString();
			}
		} else {
			value = params[0];
		}

		((ParameterContainer) content).findParameterByName(parameter.getName())
				.setByString(value);

		// System.out.println("set param " + parameter.getName() + " to "
		// + params[0]);
		//
		repository.update(entry);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
	public ClientWindowEffect getEffect() {
		return ClientWindowEffect.REFRESH;
	}

	@Override
	public int getId() {
		return id;
	}

}
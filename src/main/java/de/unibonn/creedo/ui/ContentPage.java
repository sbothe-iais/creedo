package de.unibonn.creedo.ui;

import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import de.unibonn.creedo.ui.core.Page;

/**
 * Default content page that displays arbitrary html content.
 *
 * @author bjacobs
 */
public class ContentPage implements Page {

	private final BindingAwareModelMap model;

	private final String title;

	private final String referenceName;

	public ContentPage(String title, String referenceName,
			String contentPageContentHtml) {
		model = new BindingAwareModelMap();
		this.title = title;
		this.referenceName = referenceName;
		model.addAttribute("mainContent", contentPageContentHtml);
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public String getViewImport() {
		return "contentPage.jsp";
	}

	@Override
	public Model getModel() {
		return model;
	}

	@Override
	public String getReferenceName() {
		return referenceName;
	}

}

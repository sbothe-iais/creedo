package de.unibonn.creedo.ui.indexpage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.ActionProvider;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.creedo.webapp.DashboardController;
import de.unibonn.creedo.webapp.DemoProvider;
import de.unibonn.creedo.webapp.User;
import de.unibonn.creedo.webapp.studies.StudyEngine;

/**
 * Page that displays some static content as well as links to all demos in db to
 * a logged in user (or a log-in invitation when user is not logged in).
 * 
 * @author mboley
 *
 */
public class DefaultIndexPage implements Page, ActionProvider {

	public static class OpenLinkAction implements Action {

		private final DashboardLink dashboardLink;

		private final int id;

		private final CreedoSession creedoSession;

		public DashboardLink getDashboardLink() {
			return dashboardLink;
		}

		private OpenLinkAction(CreedoSession session, int actionId,
				DashboardLink link) {
			this.dashboardLink = link;
			this.id = actionId;
			this.creedoSession = session;
		}

		@Override
		public String getReferenceName() {
			return dashboardLink.getTitle();
		}

		@Override
		public ResponseEntity<String> activate(String... params) {
			creedoSession.createMiningDashboard(dashboardLink
					.getAnalyticsDashboardBuilder());
			return new ResponseEntity<String>(
					DashboardController.GET_DASHBOARD_URL, HttpStatus.OK);
		}

		@Override
		public ClientWindowEffect getEffect() {
			return ClientWindowEffect.POPUP;
		}

		@Override
		public int getId() {
			return id;
		}

	}

	private final String title;

	private final Model model;

	private final String referenceName;

	private final Map<Integer, OpenLinkAction> actionIdToOpenLinkAction;

	private final List<OpenLinkAction> dashboardLinkLinks;

	private final CreedoSession session;

	private final String loginInviteHtml;

	public DefaultIndexPage(CreedoSession session, String title,
			String referenceName, String contentPageContent, String loginInvite) {
		this.session = session;
		this.actionIdToOpenLinkAction = new HashMap<>();
		this.dashboardLinkLinks = new ArrayList<>();
		this.model = new BindingAwareModelMap();
		this.title = title;
		this.referenceName = referenceName;
		this.model.addAttribute("mainContent", contentPageContent);
		this.loginInviteHtml = loginInvite;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public String getViewImport() {
		return "indexPage.jsp";
	}

	@Override
	public Model getModel() {
		User user = session.getUser();
		dashboardLinkLinks.clear();
		if (!user.equals(User.DEFAULT_USER)) {
			List<DashboardLink> dashboardLinks = new ArrayList<>();
			StudyEngine engine = new StudyEngine();
			List<DashboardLink> surveys = engine.getSurveyDashboardLinks(user);
			dashboardLinks.addAll(DemoProvider.INSTANCE.build(user));
			dashboardLinks.addAll(surveys);
			this.model.addAttribute("demos", dashboardLinks);
//			int nextActionIdSuffix = 0;
			for (DashboardLink link : dashboardLinks) {
				// String actionId = "openDashoardLink" + nextActionIdSuffix++;
				Integer actionId = session.getUiRegister().getNextId();
				OpenLinkAction openLinkAction = new OpenLinkAction(session,
						actionId, link);
				dashboardLinkLinks.add(openLinkAction);
				actionIdToOpenLinkAction.put(actionId, openLinkAction);

			}
			this.model.addAttribute("actions", dashboardLinkLinks);
		} else {
			model.addAttribute("loginInvitationHtml", this.loginInviteHtml);
		}
		return model;
	}

	@Override
	public String getReferenceName() {
		return referenceName;
	}

	@Override
	public Collection<Integer> getActionIds() {
		return actionIdToOpenLinkAction.keySet();
	}

	@Override
	public ResponseEntity<String> performAction(int id, String... params) {
		OpenLinkAction openLinkAction = actionIdToOpenLinkAction.get(id);
		if (openLinkAction == null) {
			throw new IllegalArgumentException("No action with id " + id);
		}
		return openLinkAction.activate(params);
		// DashboardLink link = actionIdToOpenLinkAction.get(id);
		// if (link == null) {
		// throw new IllegalArgumentException("No action with id " + id);
		// }
		//
		// this.session.createMiningDashboard(link.getAnalyticsDashboardBuilder());
		// return new ResponseEntity<String>(
		// DashboardController.GET_DASHBOARD_URL, HttpStatus.OK);
	}

	@Override
	public boolean isActionAvailable(int id) {
		return actionIdToOpenLinkAction.containsKey(id);
	}

}

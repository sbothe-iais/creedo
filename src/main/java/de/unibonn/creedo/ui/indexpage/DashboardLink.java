package de.unibonn.creedo.ui.indexpage;

import de.unibonn.creedo.webapp.dashboard.AnalyticsDashboardBuilder;

/**
 * Link for predefined analytics dashboards that can be opened from the index page.
 * 
 * @author bjacobs, mboley
 * 
 * @see DefaultIndexPage
 * 
 */
public class DashboardLink {
	private final String title;
	private final String description;
	private final String imgPath;
	private final String imgCredits;
	private final AnalyticsDashboardBuilder analyticsDashboardBuilder;

	public DashboardLink(String title, String description, String imgPath, String imgCredits, AnalyticsDashboardBuilder analyticsDashboardBuilder) {
		this.title = title;
		this.description = description;
		this.imgPath = imgPath;
		this.imgCredits = imgCredits;
		this.analyticsDashboardBuilder = analyticsDashboardBuilder;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getImgPath() {
		return imgPath;
	}

	public String getImgCredits() {
		return imgCredits;
	}

	public AnalyticsDashboardBuilder getAnalyticsDashboardBuilder() {
		return analyticsDashboardBuilder;
	}
}

package de.unibonn.creedo.ui;

import de.unibonn.creedo.common.ConfigurationProperties;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.ui.core.UiComponent;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Page that allows the user to upload resources (any files currently)
 * to the Creedo instance. The files are saved in the resource directory
 * that is configured in the application properties file.
 *
 * @author bjacobs
 */

public class ResourceUploadPage implements Page, UiComponent {

    private final int componentId;
    private boolean uploadPerformed = false;
    private List<String> fileNamesOfLastUpload;

    public ResourceUploadPage(int componentId) {
        this.componentId = componentId;
    }

    @Override
    public String getTitle() {
        return "Upload resources";
    }

    @Override
    public String getReferenceName() {
        return "Resources";
    }

    @Override
    public String getViewImport() {
        return "resourceUploadPage.jsp";
    }

    @Override
    public int getId() {
        return componentId;
    }

    @Override
    public String getView() {
        return getViewImport();
    }

    @Override
    public Model getModel() {
        BindingAwareModelMap map = new BindingAwareModelMap();
        map.put("componentId", componentId);

        if (uploadPerformed) {
            map.put("uploadOk", true);
            map.put("uploadMsg", "Uploaded " + fileNamesOfLastUpload.size() + " files");

            uploadPerformed = false;
            fileNamesOfLastUpload = null;
        }

        return map;
    }

    @Override
    public List<UiComponent> getComponents() {
        return Collections.emptyList();
    }

    public void fileUploadPerformed(MultipartFile[] files) throws IOException {
        List<String> fileNames = new ArrayList<>();

        for (MultipartFile file : files) {
            String originalName = file.getOriginalFilename();
            file.transferTo(new File(ConfigurationProperties.get().USER_FOLDER
                    + "/" + originalName));

            fileNames.add(originalName);
        }

        uploadPerformed = true;
        fileNamesOfLastUpload = fileNames;
    }

}

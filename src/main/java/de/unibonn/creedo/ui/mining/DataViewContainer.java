package de.unibonn.creedo.ui.mining;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.webapp.viewmodels.DataTableViewModel;
import de.unibonn.creedo.webapp.viewmodels.MetaDataTableModel;
import de.unibonn.creedo.webapp.viewmodels.PointCloudViewModel;
import de.unibonn.realkd.data.DataWorkspace;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.DataTable;

/**
 * UI component that aggregates several views on a data workspace.
 * 
 * @author mboley
 *
 */
public class DataViewContainer implements UiComponent {

	private final int id;

	private DataTableViewModel dataTableViewModel;

	private MetaDataTableModel metaDataModel;

	private PointCloudViewModel pointCloudViewModel;

	private final DataWorkspace dataWorkspace;

	private final HttpSession httpSession;

	public DataViewContainer(int id, HttpSession httpSession,
			DataWorkspace dataWorkspace) {
		this.id = id;
		this.httpSession = httpSession;
		this.dataWorkspace = dataWorkspace;
	}

	public DataTable getDataTable() {
		return dataWorkspace.getAllDatatables().get(0);
	}

	public PropositionalLogic getPropositionalLogic() {
		return dataWorkspace.getAllPropositionalLogics().get(0);
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getView() {
		return "../static/dataViewContainer.jsp";
	}

	@Override
	public Model getModel() {
		BindingAwareModelMap model = new BindingAwareModelMap();
		model.addAttribute("datasetName", getDataTable().getName());
		model.addAttribute("dataViewContainerId", this.getId());
		model.addAttribute("dataTableViewModel", getDataTableViewModel());
		model.addAttribute("metaDataModel", getMetaDataModel());
		return model;
	}

	@Override
	public List<UiComponent> getComponents() {
		return Arrays.asList();
	}

	public PointCloudViewModel getPointCloudViewModel() {
		if (pointCloudViewModel == null) {
			pointCloudViewModel = new PointCloudViewModel(getDataTable());
		}
		return pointCloudViewModel;
	}

	public DataTableViewModel getDataTableViewModel() {
		if (dataTableViewModel == null) {
			dataTableViewModel = new DataTableViewModel(getDataTable(),
					httpSession);
		}
		return dataTableViewModel;
	}

	public MetaDataTableModel getMetaDataModel() {
		if (metaDataModel == null) {
			metaDataModel = new MetaDataTableModel(getPropositionalLogic());
		}
		return metaDataModel;
	}

}

package de.unibonn.creedo.ui.signup;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.ActionProvider;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.ui.core.UiComponent;

/**
 * Page that renders and provides input means for two actions: login with an
 * existing account as well as load a page for creating a new account.
 * 
 * @author mboley
 *
 */
public class DefaultLoginPage implements Page, ActionProvider, UiComponent {

	private final Action loadAccountRequestPage;

	private final Action loginAction;

	private final int id;

	public DefaultLoginPage(int componentId, Action loginAction,
			Action loadAccountRequestPageAction) {
		checkNotNull(loadAccountRequestPageAction);
		id = componentId;
		this.loadAccountRequestPage = loadAccountRequestPageAction;
		this.loginAction = loginAction;
	}

	@Override
	public String getTitle() {
		return "Login";
	}

	@Override
	public String getReferenceName() {
		return "Login";
	}

	@Override
	public String getViewImport() {
		return "loginPage.jsp";
	}

	@Override
	public Model getModel() {
		Model model = new BindingAwareModelMap();
		model.addAttribute("loadAccountRequestPageAction",
				loadAccountRequestPage);
		model.addAttribute("loginAction", loginAction);
		model.addAttribute("loginPageComponentId", getId());
		return model;
	}

	@Override
	public Collection<Integer> getActionIds() {
		return new HashSet<Integer>(
				Arrays.asList(loadAccountRequestPage.getId()));
	}

	@Override
	public ResponseEntity<String> performAction(int id, String... params) {
		if (id==loadAccountRequestPage.getId()) {
			return loadAccountRequestPage.activate();
		}
		if (id==loginAction.getId()) {
			return loginAction.activate(params);
		}
		throw new IllegalArgumentException("No action with id " + id);
	}

	@Override
	public boolean isActionAvailable(int id) {
		return loadAccountRequestPage.getId()==id;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getView() {
		return getViewImport();
	}

	@Override
	public List<UiComponent> getComponents() {
		return Arrays.asList();
	}

}

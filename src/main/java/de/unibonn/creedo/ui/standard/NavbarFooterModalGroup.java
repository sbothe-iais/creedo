package de.unibonn.creedo.ui.standard;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.ActionLink;
import de.unibonn.creedo.ui.core.UiRegister;

/**
 * Utility class that can be instantiated based on two collections of actions
 * that are converted into a navbar and a footer ui component, respectively,
 * which are coupled with respect to their link emphasis behavior.
 * 
 * @author mboley
 *
 */
public class NavbarFooterModalGroup {

	private final Navbar navbar;

	private final Footer footer;

	public NavbarFooterModalGroup(UiRegister uiRegister,
			List<Action> navbarActions, List<Action> footerActions) {
		List<ActionLink> navBarLinks = new ArrayList<>();
		List<ActionLink> footerLinks = new ArrayList<>();

		for (Action action : navbarActions) {
			navBarLinks.add(new ActionLink(action));
		}
		for (Action action : footerActions) {
			footerLinks.add(new ActionLink(action));
		}

		List<ActionLink> modalGroup = new ArrayList<>();
		modalGroup.addAll(navBarLinks);
		modalGroup.addAll(footerLinks);
		for (ActionLink link : modalGroup) {
			link.setModalSiblings(modalGroup);
		}

		navbar = new Navbar(uiRegister.getNextId(), navBarLinks);
		footer = new Footer(uiRegister.getNextId(), footerLinks);
	}

	public Navbar getNavbar() {
		return navbar;
	}

	public Footer getFooter() {
		return footer;
	}

}

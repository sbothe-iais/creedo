package de.unibonn.creedo.ui;

import java.util.List;

import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.webapp.User;
import de.unibonn.creedo.webapp.dbaccess.UserDAO;

public class UserAdminPage implements Page {

	@Override
	public String getTitle() {
		return "User Administration";
	}

	@Override
	public String getReferenceName() {
		return "Users";
	}

	@Override
	public String getViewImport() {
		return "adminPage.jsp";
	}

	@Override
	public Model getModel() {
		Model model=new BindingAwareModelMap();
		UserDAO userDAO = new UserDAO();
		List<User> allUsers = userDAO.getAllUsers();

		model.addAttribute("users", allUsers);
		return model;
	}

}

package de.unibonn.creedo;

/**
 * Central constants that are referred to in the web content. This includes
 * paths to pages, timeout constants and keys that refer to certain pages.
 * 
 * @author bjacobs
 * 
 */
public class WebConstants {
	/*
	 * Keys used in Session management
	 */

	public static final String UI_SESSION_KEY = "uisession";

	/*
	 * Resource paths
	 */
	public static final String APPLICATION_ROOT = "/Creedo";
	public static final String HOME_PATH = "/index.htm";
	public static final String PING_PATH = "/ping.htm";

	/*
	 * Pages keys
	 * 
	 * These have a special meaning in connection with the database table
	 * pages_map. In pages_map, each key is mapped to an id that refers to a
	 * specific page. This page information is used for content rendering.
	 */
	public static final String INDEX_PAGE_KEY = "indexpage";
	public static final String CONTACT_PAGE_KEY = "contactpage";

	/*
	 * Timeout handling
	 */
	public static final String TIMEOUT_HANDLER_KEY = "TimeoutHandler";

	// Timeout check interval
	public static final int TIMEOUT_CHECK_INTERVAL_MILLIS = 1000;

	// Number of minutes after session is invalidated
	public static final int HARD_TIMEOUT_MILLIS = 5 * 60 * 1000; // 5 minutes
																	// without
																	// ping
}

package de.unibonn.creedo;

import java.nio.file.FileSystems;
import java.nio.file.Path;

import de.unibonn.creedo.common.ConfigurationProperties;
import de.unibonn.creedo.common.ContextProvider;
import de.unibonn.creedo.repositories.Repository;
import de.unibonn.creedo.repositories.filesystimpl.FileSystemBasedRepository;
import de.unibonn.creedo.repositories.mybatisimpl.MyBatisRepository;
import de.unibonn.creedo.studies.StudyBuilder;
import de.unibonn.creedo.studies.designs.EvaluationScheme;
import de.unibonn.creedo.studies.designs.StudyDesignBuilder;
import de.unibonn.creedo.studies.designs.SystemSpecBuilder;
import de.unibonn.creedo.studies.designs.TaskSpecBuilder;
import de.unibonn.creedo.ui.ContentPageBuilder;
import de.unibonn.creedo.ui.core.Frame;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.ui.indexpage.DashboardLinkBuilder;
import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.creedo.webapp.dashboard.mining.MiningSystemBuilder;
import de.unibonn.realkd.common.RuntimeBuilder;

public class ApplicationRepositories {

	public static Repository<RuntimeBuilder<Page, CreedoSession>> PAGE_REPOSITORY = new MyBatisRepository<>(
			"Pages", "content__pages", "content__pages_parameters");

	public static Repository<RuntimeBuilder<Frame, CreedoSession>> FRAME_REPOSITORY = new MyBatisRepository<>(
			"Frames", "content__frames", "content__frames_parameters");

	public static Repository<DashboardLinkBuilder> DEMO_REPOSITORY = new MyBatisRepository<DashboardLinkBuilder>(
			"Demos", "content__demos", "content__demos_parameters");

	public static Repository<MiningSystemBuilder> MINING_SYSTEM_REPOSITORY = new MyBatisRepository<MiningSystemBuilder>(
			"Mining Systems", "mining__mining_systems",
			"mining__mining_systems_parameters");

	public static Repository<StudyDesignBuilder> STUDY_DESIGN_REPOSITORY = new MyBatisRepository<StudyDesignBuilder>(
			"Study Designs", "content__study_designs",
			"content__study_designs_parameters");

	public static Repository<StudyBuilder> STUDY_REPOSITORY = new MyBatisRepository<StudyBuilder>(
			"Studies", "content__studies", "content__studies_parameters");

	public static Repository<SystemSpecBuilder> STUDY_SYSTEM_SPECIFICATION_REPOSITORY = new MyBatisRepository<SystemSpecBuilder>(
			"System Specifications", "content__study_system_specifications",
			"content__study_system_specifications_parameters");

	public static Repository<TaskSpecBuilder> STUDY_TASK_SPECIFICATION_REPOSITORY = new MyBatisRepository<TaskSpecBuilder>(
			"Task Specifications", "content__study_task_specifications",
			"content__study_task_specifications_parameters");
	
	public static Repository<EvaluationScheme> STUDY_EVALUATION_SCHEME_REPOSITORY = new MyBatisRepository<EvaluationScheme>(
			"Evaluation Schemes", "content__study_evaluation_schemes",
			"content__study_evaluation_schemes_parameters");

	public static Repository<Path> CONTENT_FOLDER_REPOSITORY = new FileSystemBasedRepository(
			"Ressources", FileSystems.getDefault().getPath(
					ConfigurationProperties.get().USER_FOLDER));

	public static Repository<Path> PLUGIN_FOLDER_REPOSITORY = new FileSystemBasedRepository(
			"Ressources", FileSystems.getDefault().getPath(
					ContextProvider.getServletContext().getRealPath(
							ServerPaths.PLUGIN_BASE_DIR + "/"
									+ ServerPaths.SELECTED_PLUGIN_DIR)));

}

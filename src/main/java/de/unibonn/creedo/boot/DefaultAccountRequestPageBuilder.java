package de.unibonn.creedo.boot;

import java.util.List;
import java.util.Map;

import de.unibonn.creedo.ui.signup.RequestAccountAction;
import de.unibonn.creedo.ui.signup.RequestAccountPage;
import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.realkd.common.RuntimeBuilder;
import de.unibonn.realkd.common.parameter.DefaultParameter;
import de.unibonn.realkd.common.parameter.DefaultParameterContainer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.StringParser;
import de.unibonn.realkd.common.parameter.ValueValidator;

public class DefaultAccountRequestPageBuilder implements
		RuntimeBuilder<RequestAccountPage, CreedoSession>, ParameterContainer {

	private Parameter<String> noteParameter;
	private final DefaultParameterContainer parameterContainer;

	public DefaultAccountRequestPageBuilder() {
		this.parameterContainer = new DefaultParameterContainer(
				"Account request page parameters");
		this.noteParameter = new DefaultParameter<String>(
				"Account request note",
				"A note that is displayed by page together with the sign up form",
				String.class, "", StringParser.TAKE_FIRST_STRING_PARSER,
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");
		this.parameterContainer.addParameter(noteParameter);
	}

	@Override
	public RequestAccountPage build(CreedoSession context) {
		RequestAccountAction requestAccountAction = new RequestAccountAction(
				context.getUiRegister().getNextId());
		return new RequestAccountPage(context.getUiRegister().getNextId(),
				noteParameter.getCurrentValue(), requestAccountAction);
	}

	@Override
	public Parameter<?> findParameterByName(String name) {
		return this.parameterContainer.findParameterByName(name);
	}

	@Override
	public List<Parameter> getTopLevelParameters() {
		return this.parameterContainer.getTopLevelParameters();
	}

	@Override
	public boolean isStateValid() {
		return this.parameterContainer.isStateValid();
	}

	@Override
	public void passValuesToParameters(Map<String, String[]> nameValueMap) {
		this.parameterContainer.passValuesToParameters(nameValueMap);
	}

	@Override
	public void unloadMapValuesToParameters(Map<String, String[]> crossOutMap) {
		this.unloadMapValuesToParameters(crossOutMap);
	}

}

package de.unibonn.creedo.boot;

import static de.unibonn.realkd.common.logger.LogChannel.UI;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.WebConstants;
import de.unibonn.creedo.repositories.IdentifierListOfRepositoryParameterFactory;
import de.unibonn.creedo.repositories.RepositoryEntry;
import de.unibonn.creedo.ui.PageBuilder;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.DefaultPageFrame;
import de.unibonn.creedo.ui.core.Frame;
import de.unibonn.creedo.ui.core.LoadPageAction;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.ui.core.PageContainer;
import de.unibonn.creedo.ui.core.PageFrame;
import de.unibonn.creedo.ui.signup.DefaultLoginPage;
import de.unibonn.creedo.ui.signup.LoginAction;
import de.unibonn.creedo.ui.signup.RegistrationRequestConfirmationPage;
import de.unibonn.creedo.ui.signup.RequestAccountPage;
import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.creedo.webapp.User;
import de.unibonn.creedo.webapp.handler.TimeoutHandler;
import de.unibonn.realkd.common.RuntimeBuilder;
import de.unibonn.realkd.common.logger.LogMessageType;
import de.unibonn.realkd.common.parameter.DefaultParameterContainer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;

/**
 * <p>
 * Creates and configures a frame object with all features necessary for the
 * Creedo mainframe.
 * </p>
 * 
 * <p>
 * WARNING: As of now this class is referenced directly in a db-based
 * repository. So make sure to update init scripts when moving or renaming this
 * class as well when you change its declared parameters.
 * </p>
 * 
 * @author mboley
 *
 */
public class DefaultMainFrameBuilder implements ParameterContainer,
		RuntimeBuilder<PageFrame, CreedoSession> {

	private final DefaultParameterContainer defaultParameterContainer;

	private final SubCollectionParameter<String, List<String>> optionalNavbarContents;

	private final SubCollectionParameter<String, List<String>> optionalFooterContents;

	public DefaultMainFrameBuilder() {
		this.defaultParameterContainer = new DefaultParameterContainer(
				"Main frame parameters");
		this.optionalNavbarContents = IdentifierListOfRepositoryParameterFactory.getIdentifierListOfRepositoryParameter(
				"Navbar entries",
				"List of optional content to be linked from the navbar of frame.",
				ApplicationRepositories.PAGE_REPOSITORY);
		this.optionalFooterContents = IdentifierListOfRepositoryParameterFactory.getIdentifierListOfRepositoryParameter(
				"Footer entries",
				"List of optional content to be linked from the footer of frame.",
				ApplicationRepositories.PAGE_REPOSITORY);
		this.defaultParameterContainer.addParameter(optionalNavbarContents);
		this.defaultParameterContainer.addParameter(optionalFooterContents);
	}

	public PageFrame build(final CreedoSession creedoSession) {
		PageContainer pageContainer = new PageContainer(creedoSession
				.getUiRegister().getNextId());

		RequestAccountPage requestAccountPage = (RequestAccountPage) ApplicationRepositories.PAGE_REPOSITORY
				.get("signuppage").getContent().build(creedoSession);

		creedoSession.getUiRegister().registerUiComponent(requestAccountPage);
		Action loadAccountRequestPage = new LoadPageAction(creedoSession
				.getUiRegister().getNextId(), requestAccountPage, pageContainer);

		Page accountRequestConfirmationPage = new RegistrationRequestConfirmationPage();
		requestAccountPage.addRegistrationSuccessAction(new LoadPageAction(
				creedoSession.getUiRegister().getNextId(),
				accountRequestConfirmationPage, pageContainer));

		Action loginAction = new LoginAction(creedoSession.getUiRegister()
				.getNextId(), creedoSession);

		DefaultLoginPage loginPage = new DefaultLoginPage(creedoSession
				.getUiRegister().getNextId(), loginAction,
				loadAccountRequestPage);
		creedoSession.getUiRegister().registerUiComponent(loginPage);

		Page indexPage = ((RuntimeBuilder<Page, CreedoSession>) ApplicationRepositories.PAGE_REPOSITORY
				.get("indexpage").getContent()).build(creedoSession);

		List<Action> navbarActions = new ArrayList<>();
		List<Action> footerActions = new ArrayList<>();
		// String loadIndexPageActionId = "loadIndexPage";
		navbarActions.add(new LoadPageAction(creedoSession.getUiRegister()
				.getNextId(), indexPage, pageContainer));

		if (creedoSession.getUser().isAdministrator()) {

			navbarActions.add(new Action() {

				private final int id = creedoSession.getUiRegister()
						.getNextId();

				@Override
				public String getReferenceName() {
					return "Administration";
				}

				@Override
				public ResponseEntity<String> activate(String... params) {
					Frame adminFrame = new AdminFrameBuilder()
							.build(creedoSession);

					return new ResponseEntity<String>("showFrame.htm?frameId="
							+ adminFrame.getId(), HttpStatus.OK);
				}

				@Override
				public ClientWindowEffect getEffect() {
					return ClientWindowEffect.POPUP;
				}

				@Override
				public int getId() {
					return id;
				}
			});
		}

		if (creedoSession.getUser().isDeveloper()) {
			navbarActions.add(new LoadPageAction(creedoSession.getUiRegister()
					.getNextId(), creedoSession
					.getCustomDashboardCreationPage(), pageContainer));

//			navbarActions.add(new LoadPageAction(creedoSession.getUiRegister()
//					.getNextId(), creedoSession.getDatabaseUploadPage(),
//					pageContainer));
		}

		for (String contentId : optionalNavbarContents.getCurrentValue()) {
			RepositoryEntry<RuntimeBuilder<Page, CreedoSession>> entry = ApplicationRepositories.PAGE_REPOSITORY
					.get(contentId);
			// PageBuilder pageBuilder = pagesDAO.getPageBuilder(pageId);
			PageBuilder pageBuilder = (PageBuilder) entry.getContent();
			Page page = pageBuilder.build(creedoSession);

			navbarActions.add(new LoadPageAction(creedoSession.getUiRegister()
					.getNextId(), page, pageContainer));

		}
		for (String contentId : optionalFooterContents.getCurrentValue()) {
			RepositoryEntry<RuntimeBuilder<Page, CreedoSession>> entry = ApplicationRepositories.PAGE_REPOSITORY
					.get(contentId);
			PageBuilder pageBuilder = (PageBuilder) entry.getContent();
			Page page = pageBuilder.build(creedoSession);
			footerActions.add(new LoadPageAction(creedoSession.getUiRegister()
					.getNextId(), page, pageContainer));
		}

		if (creedoSession.getUser() == User.DEFAULT_USER) {
			navbarActions.add(new LoadPageAction(creedoSession
					.getLoadLoginPageActionId(), loginPage, pageContainer));
		} else {
			Action logOutAction = new Action() {

				private final int id = creedoSession.getUiRegister()
						.getNextId();

				@Override
				public String getReferenceName() {
					return "Logout";
				}

				@Override
				public ResponseEntity<String> activate(String... params) {
					creedoSession.tearDownDashboard();
					Object timeoutHandlerObj = creedoSession.getHttpSession()
							.getAttribute(WebConstants.TIMEOUT_HANDLER_KEY);
					if (timeoutHandlerObj instanceof TimeoutHandler) {
						((TimeoutHandler) timeoutHandlerObj).stop();
						UI.log("Stopped TimeoutHandler.",
								LogMessageType.GLOBAL_STATE_CHANGE);
					}
					creedoSession.getHttpSession().invalidate();
					return new ResponseEntity<String>(HttpStatus.OK);
					// createNewCreedoSession(User.DEFAULT_USER, httpSession);
				}

				@Override
				public ClientWindowEffect getEffect() {
					return ClientWindowEffect.REFRESH;
				}

				@Override
				public int getId() {
					return id;
				}

			};
			navbarActions.add(logOutAction);
		}
		DefaultPageFrame result = creedoSession
				.getUiRegister()
				.createDefaultFrame(pageContainer, navbarActions, footerActions);
		// result.performAction(loadIndexPageActionId);

		return result;
	}

	@Override
	public Parameter<?> findParameterByName(String name) {
		return this.defaultParameterContainer.findParameterByName(name);
	}

	@Override
	public List<Parameter> getTopLevelParameters() {
		return this.defaultParameterContainer.getTopLevelParameters();
	}

	@Override
	public boolean isStateValid() {
		return this.defaultParameterContainer.isStateValid();
	}

	@Override
	public void passValuesToParameters(Map<String, String[]> nameValueMap) {
		this.passValuesToParameters(nameValueMap);
	}

	@Override
	public void unloadMapValuesToParameters(Map<String, String[]> crossOutMap) {
		this.unloadMapValuesToParameters(crossOutMap);
	}

}

package de.unibonn.creedo;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

import de.unibonn.creedo.common.ConfigurationProperties;
import de.unibonn.creedo.common.ContextProvider;

/**
 * Contains static members that allow to resolve pathes on the creedo server.
 * 
 * @author mboley, bjacobs
 *
 */
public class ServerPaths {

	public static final String PLUGIN_BASE_DIR = "plugin";

	public static final String SELECTED_PLUGIN_DIR = ConfigurationProperties
			.get().CREEDO_ACTIVE_PLUGIN;

	public static Path ABS_PATH_TO_SELECTED_PLUGIN_DIR = FileSystems
			.getDefault().getPath(
					ContextProvider.getServletContext().getRealPath(
							ServerPaths.PLUGIN_BASE_DIR + "/"
									+ ServerPaths.SELECTED_PLUGIN_DIR));

	public static Path ABS_PATH_TO_CONFIGURED_RESSOURCE_DIR = FileSystems
			.getDefault().getPath(ConfigurationProperties.get().USER_FOLDER);

//	public static String getContentPageContent(int pageId) {
//		String pageFileName = new WebContentDAO().getPageFilenameForId(pageId);
//
//		return getContentPageContent(pageFileName);
//	}

	public static String getContentPageContent(String pageFileName) {

		// InputStream resourceAsStream = ContextProvider.getServletContext()
		// .getResourceAsStream(directory + "/" + pageFileName);

		try {
			InputStream resourceAsStream = Files
					.newInputStream(ApplicationRepositories.CONTENT_FOLDER_REPOSITORY
							.get(pageFileName).getContent());
			Scanner scanner = new Scanner(resourceAsStream, "UTF-8");
			String pageContent = scanner.useDelimiter("\\A").next();
			scanner.close();
			return pageContent;
		} catch (IOException e) {
			throw new IllegalStateException("could not access ressource file",
					e);
		}
	}

}

package de.unibonn.creedo.studies;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.studies.designs.StudyDesign;
import de.unibonn.creedo.ui.indexpage.DashboardLink;
import de.unibonn.creedo.webapp.User;

/**
 * Class representing a study
 * 
 * @author bjacobs, mboley, bkang
 * 
 */
public class NewStudy {
	// private final NewStudyConfiguration config;

	private final String name;
	private final String description;
	private final String imgUrl;
	private final String imgCredits;

	private final String designName;
	private final StudyState state;
	private final List<Integer> participantIds;
	private final List<Integer> evaluatorIds;

	public NewStudy(String name, String description, String imgUrl,
			String imgCredits, String designName, StudyState state,
			List<Integer> participantIds, List<Integer> evaluatorIds) {
		this.name = name;
		this.description = description;
		this.imgUrl = imgUrl;
		this.imgCredits = imgCredits;
		this.designName = designName;
		this.state = state;
		this.participantIds = participantIds;
		this.evaluatorIds = evaluatorIds;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public String getImgCredits() {
		return imgCredits;
	}

	public String getDesignName() {
		return designName;
	}

	public StudyState getState() {
		return state;
	}

	public List<Integer> getParticipantIds() {
		return participantIds;
	}

	public List<Integer> getEvaluatorIds() {
		return evaluatorIds;
	}

	public List<DashboardLink> getDashboardLinks(User user) {
		List<DashboardLink> dashboardLinks = new ArrayList<>();
		StudyDesign studyDesign = ApplicationRepositories.STUDY_DESIGN_REPOSITORY
				.get(designName).getContent().build();
		if (participantIds.contains(user.getId())) {
			dashboardLinks.addAll(studyDesign.getAssignmentScheme()
					.getTrialAssignments(studyDesign.getSystemSpecifications(),
							studyDesign.getTaskSpecifications(), name,
							description, imgUrl, imgCredits, state, user));
		}

		if (evaluatorIds.contains(user.getId())) {
			dashboardLinks.addAll(studyDesign.getAssignmentScheme()
					.getEvaluationAssignments(
							studyDesign.getTaskSpecifications(), name,
							description, imgUrl, imgCredits, state, user));
		}

		return dashboardLinks;
	}
}

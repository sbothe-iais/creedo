package de.unibonn.creedo.studies;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface RatingMapper {

	public List<Integer> getRatingIdsGivenByUserInStudy(
			@Param("user_id") int userId, @Param("study_name") String studyName);

	public void saveRating(@Param("user_id") int userId, @Param("study_name") String studyName, 
			@Param("result_id") int resultId, @Param("metric") String metric,
			@Param("value") double value);
}

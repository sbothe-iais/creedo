package de.unibonn.creedo.studies;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface EvaluationSchemeMapper {
	
	public List<Integer> getAllIds();
	
	public String getMetricsInScheme(@Param("evaluation_scheme_id") int evaluationSchemeId);
	
	public List<String> getInstructionsOfId(@Param("evaluation_scheme_id") int evaluationSchemeId);
}

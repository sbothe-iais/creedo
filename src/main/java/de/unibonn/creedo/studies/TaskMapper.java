package de.unibonn.creedo.studies;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface TaskMapper {
	
	public List<String> getInstructionPageContentsOfTask(@Param("task_id") int taskId);
	
	public List<Integer> getAllIds();
	
	public int getDatatableIdOfTask(@Param("task_id") int taskId);
	
	public String getRatingMetricsOfTask(@Param("task_id") int taskId); 
	
	public List<String> getInstructionPageContentsOfEvaluationSchemeInTask(@Param("task_id") int taskId);

}

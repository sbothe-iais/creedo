package de.unibonn.creedo.studies;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import de.unibonn.creedo.common.ConnectionFactory;
import de.unibonn.creedo.webapp.studies.PatternBuilder;
import de.unibonn.creedo.webapp.studies.Result;
import de.unibonn.creedo.webapp.studies.ResultDataContainer;

/**
 * Access database table "study__sessions".
 * 
 * @author bjacobs, bkang
 */
public class NewSessionDAO {

	public static final NewSessionDAO INSTANCE = new NewSessionDAO();

	private class PatternBuilderInstanceCreator implements
			JsonDeserializer<PatternBuilder>, JsonSerializer<PatternBuilder> {

		public static final String CLASS_PROPERTY_NAME = "class";
		public static final String CONTENT_PROPERTY_NAME = "content";

		@Override
		public PatternBuilder deserialize(JsonElement jsonElement, Type type,
				JsonDeserializationContext context) throws JsonParseException {
			JsonObject jsonObject = jsonElement.getAsJsonObject();
			String patternBuilderClass = jsonObject.get(CLASS_PROPERTY_NAME)
					.getAsString();
			System.out.println("Deserialize: " + jsonElement);
			try {
				Class<?> clazz = Class.forName(patternBuilderClass);
				return context.deserialize(
						jsonObject.get(CONTENT_PROPERTY_NAME), clazz);

			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		public JsonElement serialize(PatternBuilder patternBuilder, Type type,
				JsonSerializationContext context) {
			JsonObject jsonObject = new JsonObject();
			jsonObject.addProperty(CLASS_PROPERTY_NAME, patternBuilder
					.getClass().getName());
			jsonObject.add(CONTENT_PROPERTY_NAME,
					context.serialize(patternBuilder));
			return jsonObject;
		}
	}

	private final SqlSessionFactory sqlSessionFactory;
	private final Gson gson;

	private NewSessionDAO() {
		this.sqlSessionFactory = ConnectionFactory.get().getSession();
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(PatternBuilder.class,
				new PatternBuilderInstanceCreator());
		gson = gsonBuilder.create();
	}

	public void saveSession(String studyName, int userId, String taskName,
			String systemName, List<PatternBuilder> patternBuilders,
			List<Integer> listOfSecondsUntilSaved) {
		SqlSession session = sqlSessionFactory.openSession();

		NewSessionMapper sessionMapper = session
				.getMapper(NewSessionMapper.class);
		try {

			// Create new Session entry
			Date date = new Date();
			sessionMapper.saveSessionInfo(studyName, userId, taskName,
					systemName, date);
			int sessionId = sessionMapper.getLastInsertId();
			for (int i = 0; i < patternBuilders.size(); i++) {
				PatternBuilder builder = patternBuilders.get(i);
				String builderContent = gson.toJson(builder,
						PatternBuilder.class);
				sessionMapper.saveResult(sessionId, builderContent,
						listOfSecondsUntilSaved.get(i));
			}

		} catch (PersistenceException pe) {
			pe.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
	}
	
	public Integer getIdOfLastSessionByUser(String studyName, int userId) {
		SqlSession sqlSession = sqlSessionFactory.openSession();
		NewSessionMapper sessionMapper = sqlSession
				.getMapper(NewSessionMapper.class);
		try {
			return sessionMapper.getIdOfLastSessionByUser(studyName, userId);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			return null;
		} finally {
			sqlSession.commit();
			sqlSession.close();
		}
	}

	public List<String> getTaskNamesPerformedByUserInStudy(int userId,
			String studyName) {
		SqlSession sqlSession = sqlSessionFactory.openSession();
		NewSessionMapper sessionMapper = sqlSession
				.getMapper(NewSessionMapper.class);
		try {
			return sessionMapper.getTaskNamesPerformedByUserInStudy(userId,
					studyName);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			return null;
		} finally {
			sqlSession.commit();
			sqlSession.close();
		}
	}

	public Integer getNumberOfPerformedSessionsOfTaskInStudy(String taskName,
			String studyName) {
		return getPerformedSeesionIdsOfTaskInStudy(taskName, studyName).size();
	}

	public List<Integer> getPerformedSeesionIdsOfTaskInStudy(String taskName,
			String studyName) {
		SqlSession sqlSession = sqlSessionFactory.openSession();
		NewSessionMapper sessionMapper = sqlSession
				.getMapper(NewSessionMapper.class);
		try {
			return sessionMapper.getPerformedSeesionIdsOfTaskInStudy(taskName,
					studyName);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			return null;
		} finally {
			sqlSession.commit();
			sqlSession.close();
		}
	}

	public Integer getNumberOfPerformedSessionsWithTaskAndAnalyticsDB(
			String studyName, String taskName, String systemName) {
		SqlSession sqlSession = sqlSessionFactory.openSession();
		NewSessionMapper sessionMapper = sqlSession
				.getMapper(NewSessionMapper.class);
		try {
			return sessionMapper
					.getNumberOfPerformedSessionsWithTaskAndAnalyticsDB(
							studyName, taskName, systemName);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			return null;
		} finally {
			sqlSession.commit();
			sqlSession.close();
		}
	}

	public List<Result> getResultsInSavedSession(int sessionId) {
		SqlSession session = sqlSessionFactory.openSession();
		NewSessionMapper sessionMapper = session
				.getMapper(NewSessionMapper.class);

		try {
			List<Result> results = new ArrayList<>();
			List<ResultDataContainer> containers = sessionMapper
					.getResultsInSavedSession(sessionId);
			for (ResultDataContainer container : containers) {
				Object patternBuilder = gson.fromJson(
						container.getResultBuilderContent(),
						PatternBuilder.class);
				if (!(patternBuilder instanceof PatternBuilder)) {
					throw new IllegalArgumentException(
							"Invalid class found for result patternBuilder");
				}
				results.add(new Result(container.getResultId(),
						(PatternBuilder) patternBuilder));
			}
			return results;
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			return null;
		} finally {
			session.commit();
			session.close();
		}
	}
}

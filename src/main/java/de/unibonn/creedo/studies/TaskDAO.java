package de.unibonn.creedo.studies;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import de.unibonn.creedo.common.ConnectionFactory;
import de.unibonn.creedo.webapp.studies.rating.RatingMetric;
import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author bkang
 */

public class TaskDAO {
	
	public static final TaskDAO INSTANCE = new TaskDAO();
	
	private final SqlSessionFactory sqlSessionFactory;

	private TaskDAO() {
		this.sqlSessionFactory = ConnectionFactory.get().getSession();
	}
	
	
	public List<String> getInstructionPageContentsOfTask(int taskId) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
			TaskMapper mapper = session.getMapper(TaskMapper.class);
			return mapper.getInstructionPageContentsOfTask(taskId); 
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
	
	public List<Integer> getAllIds() {
		SqlSession session = sqlSessionFactory.openSession();
		try {
			TaskMapper mapper = session.getMapper(TaskMapper.class);
			return mapper.getAllIds();
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
	
	public Integer getDataTableIdOfTask(int taskId) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
			TaskMapper mapper = session.getMapper(TaskMapper.class);
			return mapper.getDatatableIdOfTask(taskId);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
	
	public List<RatingMetric> getRatingMetricsOfTask(int taskId) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
			List<RatingMetric> results = new ArrayList<>();
			TaskMapper mapper = session.getMapper(TaskMapper.class);
			String jsonString = mapper.getRatingMetricsOfTask(taskId);
			System.out.println(jsonString);
			JsonObject jsonObject = new JsonParser().parse(jsonString).getAsJsonObject();
			JsonArray jsonArray = jsonObject.getAsJsonArray("metrics");
			for (JsonElement element : jsonArray) {
				results.add(RatingMetric.valueOf(element.getAsString()));
			}			
			return results;
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
	
	public List<String> getInstructionPageContentsOfEvaluationSchemeInTask(int taskId) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
			TaskMapper mapper = session.getMapper(TaskMapper.class);
			return mapper.getInstructionPageContentsOfEvaluationSchemeInTask(taskId); 
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
}

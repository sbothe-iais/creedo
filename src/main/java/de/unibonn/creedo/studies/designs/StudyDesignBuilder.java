package de.unibonn.creedo.studies.designs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.IdentifierListOfRepositoryParameterFactory;
import de.unibonn.creedo.studies.assignmentschemes.AssignmentScheme;
import de.unibonn.creedo.studies.assignmentschemes.TwoPhaseAssignment;
import de.unibonn.realkd.common.parameter.DefaultParameter;
import de.unibonn.realkd.common.parameter.DefaultParameterContainer;
import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter.RangeComputer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.StringParser;
import de.unibonn.realkd.common.parameter.ValueValidator;

public class StudyDesignBuilder implements ParameterContainer {

	public AssignmentScheme twoPhaseOption = new TwoPhaseAssignment();

	private final Parameter<String> nameParameter;

	private final Parameter<String> descriptionParameter;

	private final Parameter<List<String>> systemSpecParameter;

	private final Parameter<List<String>> taskSpecParameter;

	private final RangeEnumerableParameter<AssignmentScheme> assignmentSchemeParameter;

	private final DefaultParameterContainer defaultParameterContainer;

	public StudyDesignBuilder() {
		nameParameter = new DefaultParameter<String>("Name",
				"The name of this study design.", String.class, "",
				StringParser.TAKE_FIRST_STRING_PARSER,
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		descriptionParameter = new DefaultParameter<String>("Description",
				"A descripton that explains the study desgin.", String.class,
				"", StringParser.TAKE_FIRST_STRING_PARSER,
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		systemSpecParameter = IdentifierListOfRepositoryParameterFactory
				.getIdentifierListOfRepositoryParameter(
						"System Specifications",
						"Select a set of system specifications that specify the systems will be evaluated in study.",
						ApplicationRepositories.STUDY_SYSTEM_SPECIFICATION_REPOSITORY);

		taskSpecParameter = IdentifierListOfRepositoryParameterFactory
				.getIdentifierListOfRepositoryParameter(
						"Task Specifications",
						"Select a set of task specifications which specify the trial/evaluation tasks that study participants/evaluators need to perform.",
						ApplicationRepositories.STUDY_TASK_SPECIFICATION_REPOSITORY);


		assignmentSchemeParameter = new DefaultRangeEnumerableParameter<AssignmentScheme>(
				"Assignment Scheme",
				"Select an assignment scheme that assigns trial/evaluation tasks to study participants/evaluators ",
				AssignmentScheme.class, new RangeComputer<AssignmentScheme>() {
					@Override
					public List<AssignmentScheme> computeRange() {
						return Arrays.asList(twoPhaseOption);
					}
				});

		defaultParameterContainer = new DefaultParameterContainer(
				"Study Design Specification");

		defaultParameterContainer.addAllParameters(Arrays.asList(
				(Parameter<?>) nameParameter, descriptionParameter,
				systemSpecParameter, taskSpecParameter,
				assignmentSchemeParameter));
	}

	public StudyDesign build() {

		String name = nameParameter.getCurrentValue();

		String description = descriptionParameter.getCurrentValue();

		List<SystemSpecification> systemSpecifications = new ArrayList<>();
		for (String systemSpecificationId : systemSpecParameter
				.getCurrentValue()) {
			systemSpecifications
					.add(ApplicationRepositories.STUDY_SYSTEM_SPECIFICATION_REPOSITORY
							.get(systemSpecificationId).getContent().build());
		}

		List<TaskSpecification> taskSpecifications = new ArrayList<>();
		for (String taskSpecificationId : taskSpecParameter.getCurrentValue()) {
			taskSpecifications
					.add(ApplicationRepositories.STUDY_TASK_SPECIFICATION_REPOSITORY
							.get(taskSpecificationId).getContent().build());
		}

		return new StudyDesign(name, description, systemSpecifications,
				taskSpecifications, new SystemPerformanceMetric(),
				// AssignmentSchemeImplementations
				// .valueOf(assignmentSchemeParameter.getCurrentValue())
				//
				assignmentSchemeParameter.getCurrentValue());
	}

	@Override
	public Parameter<?> findParameterByName(String name) {
		return defaultParameterContainer.findParameterByName(name);
	}

	@Override
	public List<Parameter> getTopLevelParameters() {
		return defaultParameterContainer.getTopLevelParameters();
	}

	@Override
	public boolean isStateValid() {
		return defaultParameterContainer.isStateValid();
	}

	@Override
	public void passValuesToParameters(Map<String, String[]> nameValueMap) {
		defaultParameterContainer.passValuesToParameters(nameValueMap);
	}

	@Override
	public void unloadMapValuesToParameters(Map<String, String[]> crossOutMap) {
		defaultParameterContainer.unloadMapValuesToParameters(crossOutMap);
	}

}

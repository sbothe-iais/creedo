package de.unibonn.creedo.studies.designs;

import java.util.List;

public class TaskSpecification {
	
	private final String name;

	private final String description;

	private final List<String> instructions;

	private final int resultDefinition;
	
	private final int dataTableId;
	
	private final EvaluationScheme evaluationScheme;
	
	public TaskSpecification(String name, String description, List<String> instructions, int resultDefinition, int dataTableId, EvaluationScheme evaluationScheme) {
		this.name = name;
		this.description = description;
		this.instructions = instructions;
		this.resultDefinition = resultDefinition;
		this.dataTableId = dataTableId;
		this.evaluationScheme = evaluationScheme;
	}
	
	public String getName() {
		return name;
	}


	public String getDescription() {
		return description;
	}


	public List<String> getInstructions() {
		return instructions;
	}


	public int getResultDefinition() {
		return resultDefinition;
	}


	public int getDataTableId() {
		return dataTableId;
	}


	public EvaluationScheme getEvaluationScheme() {
		return evaluationScheme;
	}

}

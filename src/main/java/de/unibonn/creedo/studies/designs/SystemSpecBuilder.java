package de.unibonn.creedo.studies.designs;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.IdentifierInRepositoryParameter;
import de.unibonn.creedo.webapp.dashboard.mining.MiningSystemBuilder;
import de.unibonn.realkd.common.parameter.DefaultParameter;
import de.unibonn.realkd.common.parameter.DefaultParameterContainer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.StringParser;
import de.unibonn.realkd.common.parameter.ValueValidator;

public class SystemSpecBuilder implements ParameterContainer {

	private final Parameter<String> nameParameter;

	private final Parameter<String> descriptionParameter;

	private final Parameter<String> analyticsDashboardIdParameter;

	// private final RangeEnumerableParameter<Integer>
	// analyticsDashboardIdParameter;

	private final DefaultParameterContainer defaultParameterContainer;

	public SystemSpecBuilder() {
		nameParameter = new DefaultParameter<String>("Name",
				"The name of system specification.", String.class, "",
				StringParser.TAKE_FIRST_STRING_PARSER,
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		descriptionParameter = new DefaultParameter<String>("Description",
				"A descripton that explains the system specification.",
				String.class, "", StringParser.TAKE_FIRST_STRING_PARSER,
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		// analyticsDashboardIdParameter = new
		// DefaultRangeEnumerableParameter<Integer>(
		// "Analytics Dashboard Builder",
		// "List of optional analytics dashboard builders.",
		// Integer.class, new RangeComputer<Integer>() {
		// @Override
		// public List<Integer> computeRange() {
		// return StudyAnalyticsDashboardDAO.INSTANCE.getAllIds();
		// }
		// });

		analyticsDashboardIdParameter = new IdentifierInRepositoryParameter<MiningSystemBuilder>(
				"Analytics Dashboard Builder", "Mining system",
				ApplicationRepositories.MINING_SYSTEM_REPOSITORY);

		defaultParameterContainer = new DefaultParameterContainer(
				"System Specification");

		defaultParameterContainer.addAllParameters(Arrays.asList(
				(Parameter<?>) nameParameter, descriptionParameter,
				analyticsDashboardIdParameter));
	}

	public SystemSpecification build() {
		return new SystemSpecification(nameParameter.getCurrentValue(),
				descriptionParameter.getCurrentValue(),
				ApplicationRepositories.MINING_SYSTEM_REPOSITORY.get(
						analyticsDashboardIdParameter.getCurrentValue())
						.getContent());
		// return new SystemSpecification(nameParameter.getCurrentValue(),
		// descriptionParameter.getCurrentValue(),
		// StudyAnalyticsDashboardDAO.INSTANCE
		// .getMiningSystemOfAnalyticsDB(analyticsDashboardIdParameter.getCurrentValue()));
	}

	@Override
	public Parameter<?> findParameterByName(String name) {
		return defaultParameterContainer.findParameterByName(name);
	}

	@Override
	public List<Parameter> getTopLevelParameters() {
		return defaultParameterContainer.getTopLevelParameters();
	}

	@Override
	public boolean isStateValid() {
		return defaultParameterContainer.isStateValid();
	}

	@Override
	public void passValuesToParameters(Map<String, String[]> nameValueMap) {
		defaultParameterContainer.passValuesToParameters(nameValueMap);
	}

	@Override
	public void unloadMapValuesToParameters(Map<String, String[]> crossOutMap) {
		defaultParameterContainer.unloadMapValuesToParameters(crossOutMap);
	}

}

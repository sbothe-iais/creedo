package de.unibonn.creedo.studies.designs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.ServerPaths;
import de.unibonn.creedo.common.DefaultSubCollectionParameter;
import de.unibonn.creedo.common.DefaultSubCollectionParameter.CollectionComputer;
import de.unibonn.creedo.repositories.IdentifierListOfRepositoryParameterFactory;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.creedo.webapp.studies.rating.RatingMetric;
import de.unibonn.realkd.common.parameter.DefaultParameter;
import de.unibonn.realkd.common.parameter.DefaultParameterContainer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.StringParser;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;
import de.unibonn.realkd.common.parameter.ValueValidator;

public class EvaluationScheme implements ParameterContainer {

	private final Parameter<String> nameParameter;

	private final Parameter<String> descriptionParameter;

	private final SubCollectionParameter<String, List<String>> instructionParameter;

	private final SubCollectionParameter<RatingMetric, List<RatingMetric>> ratingMetricParameter;

	private final DefaultParameterContainer defaultParameterContainer;

	// private final int numberOfResultsForEvaluation;

	public EvaluationScheme() {

		nameParameter = new DefaultParameter<String>("Name",
				"The name of this evaluation scheme.", String.class, "",
				StringParser.TAKE_FIRST_STRING_PARSER,
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		descriptionParameter = new DefaultParameter<String>("Description",
				"A descripton that explains this evalaution scheme.",
				String.class, "", StringParser.TAKE_FIRST_STRING_PARSER,
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		instructionParameter = IdentifierListOfRepositoryParameterFactory
				.getIdentifierListOfRepositoryParameter(
						"Instructions",
						"Select a list of instruction pages for the corresponding evaluation task.",
						ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
						Repositories
								.getIdIsFilenameWithExtensionPredicate(Repositories.HTML_FILE_EXTENSIONS));

		ratingMetricParameter = DefaultSubCollectionParameter
				.getDefaultSubListParameter(
						"Rating Metrics",
						"Select a set of rating metrics that evaluators will use to rate the reuslts from trial tasks.",
						new CollectionComputer<List<RatingMetric>>() {
							@Override
							public List<RatingMetric> computeCollection() {
								return Arrays.asList(RatingMetric
										.values());
							}
						});

		defaultParameterContainer = new DefaultParameterContainer(
				"Evaluation Scheme");

		defaultParameterContainer.addAllParameters(Arrays.asList(
				(Parameter<?>) nameParameter, descriptionParameter,
				instructionParameter, ratingMetricParameter));

	}

	public List<String> getInstructions() {
		List<String> instructions = new ArrayList<>();
		for (String fileName : instructionParameter.getCurrentValue()) {
			instructions.add(ServerPaths.getContentPageContent(fileName));
		}
		return instructions;
	}

	public List<RatingMetric> getRatingMetrics() {
		return ratingMetricParameter.getCurrentValue();
		
	}

	public String getName() {
		return nameParameter.getCurrentValue();
	}

	public String getDescription() {
		return descriptionParameter.getCurrentValue();
	}

	@Override
	public Parameter<?> findParameterByName(String name) {
		return defaultParameterContainer.findParameterByName(name);
	}

	@Override
	public List<Parameter> getTopLevelParameters() {
		return defaultParameterContainer.getTopLevelParameters();
	}

	@Override
	public boolean isStateValid() {
		return defaultParameterContainer.isStateValid();
	}

	@Override
	public void passValuesToParameters(Map<String, String[]> nameValueMap) {
		defaultParameterContainer.passValuesToParameters(nameValueMap);
	}

	@Override
	public void unloadMapValuesToParameters(Map<String, String[]> crossOutMap) {
		defaultParameterContainer.unloadMapValuesToParameters(crossOutMap);
	}

}

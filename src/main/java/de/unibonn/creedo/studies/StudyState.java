package de.unibonn.creedo.studies;

public enum StudyState {
	SETUP, TRIAL, EVALUATION, CLOSED;
}

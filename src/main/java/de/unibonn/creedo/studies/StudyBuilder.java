package de.unibonn.creedo.studies;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.common.DefaultSubCollectionParameter;
import de.unibonn.creedo.common.DefaultSubCollectionParameter.CollectionComputer;
import de.unibonn.creedo.repositories.IdentifierInRepositoryParameter;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.creedo.webapp.dbaccess.UserDAO;
import de.unibonn.realkd.common.parameter.DefaultParameter;
import de.unibonn.realkd.common.parameter.DefaultParameterContainer;
import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter.RangeComputer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.StringParser;
import de.unibonn.realkd.common.parameter.ValueValidator;

public class StudyBuilder implements ParameterContainer {

	private final Parameter<String> nameParameter;

	private final Parameter<String> descriptionParameter;

	private final Parameter<String> imgUrlParameter;

	private final Parameter<String> imgCreditsParameter;

	private final RangeEnumerableParameter<String> studyDesignParameter;

	private final RangeEnumerableParameter<StudyState> statusParameter;

	private final DefaultSubCollectionParameter<Integer, List<Integer>> participantsParamter;

	private final DefaultSubCollectionParameter<Integer, List<Integer>> evaluatorsParamter;

	private final DefaultParameterContainer defaultParameterContainer;

	public StudyBuilder() {
		nameParameter = new DefaultParameter<String>("Name",
				"The name of study.", String.class, "",
				StringParser.TAKE_FIRST_STRING_PARSER,
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		descriptionParameter = new DefaultParameter<String>("Description",
				"A descripton that explains current study.", String.class, "",
				StringParser.TAKE_FIRST_STRING_PARSER,
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		imgUrlParameter = new IdentifierInRepositoryParameter<Path>(
				"ImgUrl",
				"File with the image shown in study description.",
				ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
				Repositories
						.getIdIsFilenameWithExtensionPredicate(Repositories.IMAGE_FILE_EXTENSIONS));

		imgCreditsParameter = new DefaultParameter<String>("ImgCredits",
				"The credits of the image used in study description.",
				String.class, "", StringParser.TAKE_FIRST_STRING_PARSER,
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		studyDesignParameter = new DefaultRangeEnumerableParameter<String>(
				"Design", "The corresponding study design of current study.",
				String.class, new RangeComputer<String>() {
					@Override
					public List<String> computeRange() {
						return ApplicationRepositories.STUDY_DESIGN_REPOSITORY
								.getAllIds();
					}
				});

		statusParameter = new DefaultRangeEnumerableParameter<StudyState>("State",
				"The status of the current study", StudyState.class,
				new RangeComputer<StudyState>() {
					@Override
					public List<StudyState> computeRange() {
						return Arrays.asList(StudyState.values());
					}
				});

		participantsParamter = DefaultSubCollectionParameter
				.getDefaultSubListParameter("Participants",
						"Set of users that participant the study.",
						new CollectionComputer<List<Integer>>() {
							@Override
							public List<Integer> computeCollection() {
								return new UserDAO().getAllUserIds();
							}
						});
		evaluatorsParamter = DefaultSubCollectionParameter
				.getDefaultSubListParameter("Evaluators",
						"Set of users that evaluate the study results.",
						new CollectionComputer<List<Integer>>() {
							@Override
							public List<Integer> computeCollection() {
								return new UserDAO().getAllUserIds();
							}
						});

		defaultParameterContainer = new DefaultParameterContainer("Study");

		defaultParameterContainer.addAllParameters(Arrays.asList(
				(Parameter<?>) nameParameter, descriptionParameter,
				imgUrlParameter, imgCreditsParameter, studyDesignParameter,
				statusParameter, participantsParamter, evaluatorsParamter));
	}

	public NewStudy build() {
		return new NewStudy(nameParameter.getCurrentValue(),
				descriptionParameter.getCurrentValue(),
				imgUrlParameter.getCurrentValue(),
				imgCreditsParameter.getCurrentValue(),
				studyDesignParameter.getCurrentValue(),
				statusParameter.getCurrentValue(),
				participantsParamter.getCurrentValue(),
				evaluatorsParamter.getCurrentValue());
	}

	@Override
	public Parameter<?> findParameterByName(String name) {
		return defaultParameterContainer.findParameterByName(name);
	}

	@Override
	public List<Parameter> getTopLevelParameters() {
		return defaultParameterContainer.getTopLevelParameters();
	}

	@Override
	public boolean isStateValid() {
		return defaultParameterContainer.isStateValid();
	}

	@Override
	public void passValuesToParameters(Map<String, String[]> nameValueMap) {
		defaultParameterContainer.passValuesToParameters(nameValueMap);
	}

	@Override
	public void unloadMapValuesToParameters(Map<String, String[]> crossOutMap) {
		defaultParameterContainer.unloadMapValuesToParameters(crossOutMap);
	}

}

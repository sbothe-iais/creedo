package de.unibonn.creedo.studies.assignmentschemes;

import java.util.List;

import de.unibonn.creedo.studies.StudyState;
import de.unibonn.creedo.studies.designs.SystemSpecification;
import de.unibonn.creedo.studies.designs.TaskSpecification;
import de.unibonn.creedo.ui.indexpage.DashboardLink;
import de.unibonn.creedo.webapp.User;

public interface AssignmentScheme {

	public abstract List<DashboardLink> getTrialAssignments(
			List<SystemSpecification> systemSpecs,
			List<TaskSpecification> taskSpecs, String studyName,
			String studyDescr, String imgURL, String imgCredits, StudyState state, User user);

	public abstract List<DashboardLink> getEvaluationAssignments(
			List<TaskSpecification> taskSpecs, String studyName,
			String studyDescr, String imgURL, String imgCredits, StudyState state, User user);

//	public abstract String getName();

}
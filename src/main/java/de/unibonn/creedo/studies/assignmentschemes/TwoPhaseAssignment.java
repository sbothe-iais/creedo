package de.unibonn.creedo.studies.assignmentschemes;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;

import de.unibonn.creedo.studies.NewSessionDAO;
import de.unibonn.creedo.studies.RatingDAO;
import de.unibonn.creedo.studies.StudyState;
import de.unibonn.creedo.studies.designs.SystemSpecification;
import de.unibonn.creedo.studies.designs.TaskSpecification;
import de.unibonn.creedo.ui.indexpage.DashboardLink;
import de.unibonn.creedo.webapp.User;
import de.unibonn.creedo.webapp.dashboard.AnalyticsDashboardBuilder;
import de.unibonn.creedo.webapp.dashboard.CloseHandler;
import de.unibonn.creedo.webapp.dashboard.closehandlers.CloseOperation;
import de.unibonn.creedo.webapp.dashboard.closehandlers.GeneralCloseHandler;
import de.unibonn.creedo.webapp.dashboard.closehandlers.MinMaxMinedPatternsCloseHanderOperation;
import de.unibonn.creedo.webapp.dashboard.mining.DataTableProvider;
import de.unibonn.creedo.webapp.dashboard.mining.MiningDashboardModelBuilderImplementation;
import de.unibonn.creedo.webapp.dashboard.multistep.MultiStepDashboardModelBuilder;
import de.unibonn.creedo.webapp.dashboard.study.InstructionPageDashboardModelBuilder;
import de.unibonn.creedo.webapp.dashboard.study.RatingDashboardModelBuilder;
import de.unibonn.creedo.webapp.studies.Result;
import de.unibonn.creedo.webapp.studies.StudyAnalysisTaskSessionCloseOperation;
import de.unibonn.creedo.webapp.studies.StudyRatingDashboardCloseOperation;
import de.unibonn.creedo.webapp.studies.result.PatternSetResultCreator;
import de.unibonn.realkd.common.parameter.DefaultParameter;
import de.unibonn.realkd.common.parameter.DefaultParameterContainer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.StringParser;
import de.unibonn.realkd.common.parameter.ValueValidator;

public class TwoPhaseAssignment implements AssignmentScheme, ParameterContainer {

	private final Parameter<Integer> numberOfResultsToEvaluateParameter;
	private final DefaultParameterContainer parameterContainer;

	public TwoPhaseAssignment() {
		this.numberOfResultsToEvaluateParameter = new DefaultParameter<Integer>(
				"Results per evaluator",
				"The number of results that every evaluator has to evaluate.",
				Integer.class, 1, StringParser.INTEGER_PARSER,
				new ValueValidator.LargerThanThresholdValidator<Integer>(
						new Integer(0)), "Choose integer larger than 0.");
		this.parameterContainer = new DefaultParameterContainer(
				"Parameter of two phase assignment scheme");
		this.parameterContainer.addAllParameters(Arrays
				.asList(numberOfResultsToEvaluateParameter));
	}

	public List<DashboardLink> getEvaluationAssignments(
			List<TaskSpecification> taskSpecs, String studyName,
			String studyDescr, String imgURL, String imgCredits,
			StudyState studyState, User user) {
		checkArgument(isStateValid(), "invalid state of assignment operator");
		List<DashboardLink> results = new ArrayList<>();
		// 0. check whether the current study state is "Evaluation"
		if (!studyState.equals(StudyState.EVALUATION)) {
			return results;
		}
		// 1. check whether this user has participated any evaluation.
		List<Integer> performedRatingIds = RatingDAO.INSTANCE
				.getRatingIdsGivenByUserInStudy(user.getId(), studyName);
		if (performedRatingIds.size() != 0) {
			return results;
		}

		// 2. decide the task to be evaluated by current user.
		List<TaskSpecification> availableTasksInStudy = new ArrayList<>();
		List<String> taskNamesPerformedByUser = NewSessionDAO.INSTANCE
				.getTaskNamesPerformedByUserInStudy(user.getId(), studyName);
		for (TaskSpecification taskSpec : taskSpecs) {
			boolean isPerformed = false;
			for (String taskName : taskNamesPerformedByUser) {
				if (taskSpec.getName().equals(taskName)) {
					isPerformed = true;
					break;
				}
			}
			if (!isPerformed) {
				availableTasksInStudy.add(taskSpec);
			}
		}

		if (availableTasksInStudy.size() == 0) {
			return results;
		}
		Random rand = new Random();
		TaskSpecification selectedTask = availableTasksInStudy.get(rand
				.nextInt(availableTasksInStudy.size()));

		// 3. check where there are at least NUM_RESULTS_FOR_EVALUATION
		// results submitted.
		// int numberOfResultsForEvaluation = selectedTask.getEvaluationScheme()
		// .getNumberOfResultsForEvaluation();
		int numberOfResultsForEvaluation = numberOfResultsToEvaluateParameter
				.getCurrentValue();
		if (NewSessionDAO.INSTANCE.getNumberOfPerformedSessionsOfTaskInStudy(
				selectedTask.getName(), studyName) < numberOfResultsForEvaluation) {
			return results;
		}

		// 4. Uniformly select NUM_RESULTS_FOR_EVALUATION from the available
		// results.
		List<Integer> performedSessionIdsOfTaskInStudy = NewSessionDAO.INSTANCE
				.getPerformedSeesionIdsOfTaskInStudy(selectedTask.getName(),
						studyName);
		Collections.shuffle(performedSessionIdsOfTaskInStudy);
		List<Integer> selectedSessionIds = performedSessionIdsOfTaskInStudy
				.subList(0, numberOfResultsForEvaluation);

		List<Result> resultsToEvaluate = new ArrayList<>(
				numberOfResultsForEvaluation);
		for (Integer sessionId : selectedSessionIds) {
			resultsToEvaluate.addAll(NewSessionDAO.INSTANCE
					.getResultsInSavedSession(sessionId));
		}

		// 5. build rating dashboard.

		RatingDashboardModelBuilder ratingDashboardModelBuilder = new RatingDashboardModelBuilder(
				selectedTask.getDataTableId(), resultsToEvaluate);

		// get from DB the metrics the evaluation scheme for the selected
		// task;
		ratingDashboardModelBuilder.setRatingMetrics(selectedTask
				.getEvaluationScheme().getRatingMetrics());

		ratingDashboardModelBuilder.setCloseHandler(new GeneralCloseHandler(
				new StudyRatingDashboardCloseOperation(studyName,
						numberOfResultsForEvaluation)));
		ratingDashboardModelBuilder.setResultsToEvaluate(resultsToEvaluate);

		List<AnalyticsDashboardBuilder> modelBuilders = new ArrayList<>();
		List<String> nextAndCloseButtonNames = new ArrayList<>();

		// Add rating instruction page
		List<String> instructionPages = selectedTask.getEvaluationScheme()
				.getInstructions();
		for (String instructionPage : instructionPages) {
			modelBuilders.add(new InstructionPageDashboardModelBuilder()
					.setInstructionHtml(instructionPage));
			nextAndCloseButtonNames.add("Next");
		}

		modelBuilders.add(ratingDashboardModelBuilder);
		nextAndCloseButtonNames.add("Submit ratings");

		MultiStepDashboardModelBuilder multiStepBuilder = new MultiStepDashboardModelBuilder(
				modelBuilders);
		multiStepBuilder.setNextAndCloseNames(nextAndCloseButtonNames);
		multiStepBuilder.setCloseHandler(new GeneralCloseHandler(
				new StudyRatingDashboardCloseOperation(studyName,
						numberOfResultsForEvaluation)));

		results.add(new DashboardLink(studyName
				+ " [Evaluation]", studyDescr, imgURL,
				imgCredits, multiStepBuilder));

		return results;
	}

	public List<DashboardLink> getTrialAssignments(
			List<SystemSpecification> systemSpecs,
			List<TaskSpecification> taskSpecs, String studyName,
			String studyDescr, String imgURL, String imgCredits,
			StudyState studyState, User user) {
		checkArgument(isStateValid(), "invalid state of assignment operator");
		List<DashboardLink> results = new ArrayList<DashboardLink>();

		// 0. check whether the current study state is "TRIAL"
		if (!studyState.equals(StudyState.TRIAL)) {
			return results;
		}

		// 1. check whether this user has participated.
		if (NewSessionDAO.INSTANCE.getIdOfLastSessionByUser(studyName,
				user.getId()) != null) {
			System.out.println("user " + user.getId() + " "
					+ user.getUsername() + " already Participated");
			return results;
		}

		// 2. list all the trials (task - system combinations)
		// TODO shouldn't this be an illegal state?
		if (systemSpecs.size() == 0 || taskSpecs.size() == 0) {
			return results;
		}

		List<List<Integer>> trialConfigs = new ArrayList<List<Integer>>();
		List<Integer> numPerformedSessionsPerTrial = new ArrayList<Integer>();
		for (TaskSpecification taskSpec : taskSpecs) {
			for (SystemSpecification systemSpec : systemSpecs) {
				List<Integer> temp = new ArrayList<Integer>();
				temp.add(taskSpecs.indexOf((taskSpec)));
				temp.add(systemSpecs.indexOf(systemSpec));
				trialConfigs.add(temp);

				/*
				 * Get the number of performed sessions for each trials. New
				 * Integer() is to make sure that every numPerformedSession has
				 * different reference id.
				 */
				numPerformedSessionsPerTrial
						.add(new Integer(
								NewSessionDAO.INSTANCE
										.getNumberOfPerformedSessionsWithTaskAndAnalyticsDB(
												studyName, taskSpec.getName(),
												systemSpec.getName())));
			}
		}

		// 3. find the available trials with smallest number of performed
		// sessions.
		List<Integer> sortedNumPerformedSessions = new ArrayList<>(
				numPerformedSessionsPerTrial);
		Collections.sort(sortedNumPerformedSessions);
		int min = sortedNumPerformedSessions.get(0);
		List<Integer> availalbeTrialIndices = new ArrayList<>();
		for (Integer numPerformedSessions : sortedNumPerformedSessions) {
			if (numPerformedSessions > min) {
				break;
			}
			for (int i = 0; i < numPerformedSessionsPerTrial.size(); i++) {
				// compares the references.
				if (numPerformedSessions == numPerformedSessionsPerTrial.get(i)) {
					availalbeTrialIndices.add(i);
				}
			}
		}

		// 4. uniformly choose a trial.
		Random rand = new Random();
		int selectedTrialIndex = availalbeTrialIndices.get(rand
				.nextInt(availalbeTrialIndices.size()));

		// 5. generate trial assignment.
		int selectedTaskSpecIndex = trialConfigs.get(selectedTrialIndex).get(0);
		int selectedSystemSpecIndex = trialConfigs.get(selectedTrialIndex).get(
				1);

		results.add(getMiningDashboardLink(
				taskSpecs.get(selectedTaskSpecIndex),
				systemSpecs.get(selectedSystemSpecIndex), studyName,
				studyDescr, imgURL, imgCredits, user));
		return results;
	}

	@Override
	public String toString() {
		return "TWO_PHASE_ASSIGNMENT";
	}

	private DashboardLink getMiningDashboardLink(TaskSpecification taskSpec,
			SystemSpecification systemSpec, String studyName,
			String studyDescr, String imgURL, String imgCredits, User user) {
		List<AnalyticsDashboardBuilder> modelBuilders = new ArrayList<>();
		List<String> nextAndCloseButtonNames = new ArrayList<>();

		// Add instruction page
		List<String> instructions = taskSpec.getInstructions();
		for (String instruction : instructions) {
			modelBuilders.add(new InstructionPageDashboardModelBuilder()
					.setInstructionHtml(instruction));
			nextAndCloseButtonNames.add("Next");
		}

		// Get builder from task, attach StudyAnalysisTaskSessionCloseHandler
		// and add
		CloseOperation operation = new StudyAnalysisTaskSessionCloseOperation(
				studyName, user.getId(), taskSpec.getName(),
				systemSpec.getName(), new PatternSetResultCreator());
		CloseOperation operation2 = new MinMaxMinedPatternsCloseHanderOperation(
				3, 3);
		CloseHandler closeHandler = new GeneralCloseHandler(operation,
				operation2);

		DataTableProvider tableProvider = new DataTableProvider.DbBasedDataTableProvider(
				taskSpec.getDataTableId());

		AnalyticsDashboardBuilder analyticsDashboardBuilder = new MiningDashboardModelBuilderImplementation(
				tableProvider, systemSpec.getMiningSystemBuilder());
		analyticsDashboardBuilder.setCloseHandler(closeHandler);

		modelBuilders.add(analyticsDashboardBuilder);
		nextAndCloseButtonNames.add("Submit results");

		MultiStepDashboardModelBuilder builder = new MultiStepDashboardModelBuilder(
				modelBuilders);
		builder.setNextAndCloseNames(nextAndCloseButtonNames);
		builder.setCloseHandler(closeHandler);

		return new DashboardLink(studyName + " [Trial]",
				studyDescr, imgURL, imgCredits, builder);
	}

	@Override
	public Parameter<?> findParameterByName(String name) {
		return parameterContainer.findParameterByName(name);
	}

	@Override
	public List<Parameter> getTopLevelParameters() {
		return parameterContainer.getTopLevelParameters();
	}

	@Override
	public boolean isStateValid() {
		return parameterContainer.isStateValid();
	}

	@Override
	public void passValuesToParameters(Map<String, String[]> nameValueMap) {
		parameterContainer.passValuesToParameters(nameValueMap);
	}

	@Override
	public void unloadMapValuesToParameters(Map<String, String[]> crossOutMap) {
		parameterContainer.unloadMapValuesToParameters(crossOutMap);
	};

}

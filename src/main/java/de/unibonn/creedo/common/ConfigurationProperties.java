package de.unibonn.creedo.common;

import org.springframework.stereotype.Component;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.utils.StringUtils;

import java.util.List;
import java.util.Properties;

/**
 * Central entity that represents the configuration entries in the user-provided
 * properties file at runtime.
 *
 * @author bjacobs
 */
@Component
public class ConfigurationProperties {
	public static ConfigurationProperties get() {
		return (ConfigurationProperties) ContextProvider
				.getApplicationContext().getBean("configurationProperties");
	}

	private static String DEFAULT_TITLE_PREFIX = "Creedo";
	private static String DEFAULT_COPYRIGHT_NOTICE = Creedo.getCopyright();
	private static String DEFAULT_PRIMARY_LOGO_PATH = "client/images/creedo_logo_horizontal_nomargin.png";
	private static String DEFAULT_PRIMARY_LOGO_LINK = "https://bitbucket.org/realKD/creedo";

	public final String TITLE_PREFIX;
	public final String COPYRIGHT_NOTICE;
	public final String PRIMARY_LOGO_PATH;
	public final String PRIMARY_LOGO_LINK;
	public final String SECONDARY_LOGO_PATH;
	public final String SECONDARY_LOGO_LINK;
	public final String USER_FOLDER;
	public final String CREEDO_DB_DRIVER;
	public final String CREEDO_DB_SYSTEM;
	public final String CREEDO_DB_HOST;
	public final String CREEDO_DB_PORT;
	public final String CREEDO_DB_SCHEMA;
	public final String CREEDO_DB_USER;
	public final String CREEDO_DB_PASS;
	public final String CREEDO_SETUP_PASS;
	public final String CREEDO_ACTIVE_PLUGIN;
	public final List<String> MINING_SYSTEM_BUILDER_CLASSES;

	public ConfigurationProperties(Properties properties) {
		String specifiedTitlePrefix = properties
				.getProperty("creedo.app.titleprefix");
		TITLE_PREFIX = (specifiedTitlePrefix != null ? specifiedTitlePrefix
				: DEFAULT_TITLE_PREFIX);

		String specifiedCopyright = properties.getProperty("creedo.app.copyright");
		COPYRIGHT_NOTICE = (specifiedCopyright != null ? specifiedCopyright
				: DEFAULT_COPYRIGHT_NOTICE);

		String specifiedPrimaryLogoUrl = properties
				.getProperty("creedo.app.primarylogopath");
		PRIMARY_LOGO_PATH = (specifiedPrimaryLogoUrl != null ? specifiedPrimaryLogoUrl
				: DEFAULT_PRIMARY_LOGO_PATH);
		String specifiedPrimaryLogoLink = properties
				.getProperty("creedo.app.primarylogolink");
		PRIMARY_LOGO_LINK = (specifiedPrimaryLogoLink != null ? specifiedPrimaryLogoLink
				: DEFAULT_PRIMARY_LOGO_LINK);
		SECONDARY_LOGO_PATH = properties
				.getProperty("creedo.app.secondarylogopath");
		SECONDARY_LOGO_LINK = properties
				.getProperty("creedo.app.secondarylogolink");

		MINING_SYSTEM_BUILDER_CLASSES = StringUtils
				.jsonArrayToStringList(properties
						.getProperty("creedo.app.miningsystembuilders"));

		// Root
		CREEDO_ACTIVE_PLUGIN = properties.getProperty("creedo.root.activeplugin");
		USER_FOLDER = properties.getProperty("creedo.root.userfolder");
		CREEDO_SETUP_PASS = properties.getProperty("creedo.root.setuppassword");


		// Database
		CREEDO_DB_DRIVER = properties.getProperty("creedo.db.driver");
		CREEDO_DB_SYSTEM = properties.getProperty("creedo.db.system");
		CREEDO_DB_HOST = properties.getProperty("creedo.db.host");
		CREEDO_DB_PORT = properties.getProperty("creedo.db.port");
		CREEDO_DB_SCHEMA = properties.getProperty("creedo.db.schema");
		CREEDO_DB_USER = properties.getProperty("creedo.db.user");
		CREEDO_DB_PASS = properties.getProperty("creedo.db.pass");
	}
}

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.creedo.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import de.unibonn.creedo.repositories.IdentifierListOfRepositoryParameterFactory;
import de.unibonn.creedo.utils.StringUtils;
import de.unibonn.realkd.common.parameter.DefaultParameter;
import de.unibonn.realkd.common.parameter.DependentParameter;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterListener;
import de.unibonn.realkd.common.parameter.StringParser;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;
import de.unibonn.realkd.common.parameter.ValueValidator;
import static com.google.common.base.Preconditions.checkArgument;

/**
 * @author mboley
 * 
 */
public class DefaultSubCollectionParameter<G, T extends Collection<G>>
		implements SubCollectionParameter<G, T>, DependentParameter<T>,
		ParameterListener {

	/**
	 * Implementation of StringParser that parses a single string in js format
	 * ([e1,e2,...]) to the sub-collection of objects of getCollection whose
	 * string representation corresponds to e1, e2,...
	 */
	public static class ElementOfRangeToListParser<G, T extends Collection<G>>
			implements StringParser<T> {

		private CollectionComputer<T> collectionComputer;

		public ElementOfRangeToListParser(
				CollectionComputer<T> collectionComputer) {
			this.collectionComputer = collectionComputer;
		}

		private G findByString(String searchString) {
			for (G element : collectionComputer.computeCollection()) {
				if (element.toString().equals(searchString)) {
					return element;
				}
			}
			throw new IllegalArgumentException("Could not find element '"
					+ searchString + "' in collection.");
		}

		@Override
		public T parse(String... strings) {
			checkArgument(strings.length == 1,
					"Can parse only exactly one string argument.");
			String argument = strings[0];
			
			List<String> argumentAsStrings=StringUtils.jsonArrayToStringList(argument);
						
			T result = (T) new ArrayList<>();

			for (String string: argumentAsStrings) {
				result.add(findByString(string));
			}
			
			return result;
		}
	}

	public static class ValidateIsSubcollection<G, T extends Collection<G>>
			implements ValueValidator<T> {

		private CollectionComputer<T> listComputer;

		public ValidateIsSubcollection(CollectionComputer<T> listComputer) {
			this.listComputer = listComputer;
		}

		@Override
		public boolean valid(T value) {
			for (G element : value) {
				if (!listComputer.computeCollection().contains(element)) {
					return false;
				}
			}
			return true;
		}

	}

	public interface CollectionComputer<T> {

		/**
		 * 
		 * @return non-null range
		 */
		public T computeCollection();

	}

	private static final String HINT = "Choose an element from list";

	private final DefaultParameter<T> defaultParameter;

	private final CollectionComputer<T> rangeComputer;

	// private List<T> currentRange = Arrays.asList();

	/**
	 * Calls range computation for the first time if context is valid and
	 * buffers its result. Also, if range is non-empty and current value is
	 * null, sets first option as current value.
	 */
	private DefaultSubCollectionParameter(String name, String description,
			Class<?> type, CollectionComputer<T> rangeComputer,
			StringParser<T> parser, ValueValidator<T> validator,
			Parameter<?>... dependenParams) {
		for (Parameter<?> parameter : dependenParams) {
			parameter.addListener(this);
		}
		this.rangeComputer = rangeComputer;
		this.defaultParameter = new DefaultParameter<T>(name, description,
				type, (T) Arrays.asList(), parser, validator, HINT, dependenParams);
	}

	public static <G> DefaultSubCollectionParameter<G, List<G>> getDefaultSubListParameter(
			String name, String description,
			CollectionComputer<List<G>> rangeComputer) {
		return new DefaultSubCollectionParameter<G, List<G>>(name, description,
				List.class, rangeComputer,
				new ElementOfRangeToListParser<G, List<G>>(rangeComputer),
				new ValidateIsSubcollection<G, List<G>>(rangeComputer));
		// new ElementOfRangeToListParser<G, List<G>>(rangeComputer),
		// new ValidateIsSubcollection<G, List<G>>(rangeComputer));
	}

	/**
	 * Returns the buffered range.
	 */
	@Override
	public final T getCollection() {
		if (!isContextValid()) {
			return (T) Arrays.asList();
		}
		return rangeComputer.computeCollection();
	}

	@Override
	public final boolean isContextValid() {
		return this.defaultParameter.isContextValid();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public final List<Parameter> getDependsOnParameters() {
		return this.defaultParameter.getDependsOnParameters();
	}

	@Override
	public final boolean isValid() {
		return this.defaultParameter.isValid();
	}

	@Override
	public final String getValueCorrectionHint() {
		return this.defaultParameter.getValueCorrectionHint();
	}

	@Override
	public final String getName() {
		return this.defaultParameter.getName();
	}

	@Override
	public final String getDescription() {
		return this.defaultParameter.getDescription();
	}

	@Override
	public final Class<?> getType() {
		return this.defaultParameter.getType();
	}

	@Override
	public final void set(T value) {
		this.defaultParameter.set(value);
	}

	@Override
	public final void setByString(String... value) {
		this.defaultParameter.setByString(value);
	}

	@Override
	public final T getCurrentValue() {
		return defaultParameter.getCurrentValue();
	}

	@Override
	public final void addListener(final ParameterListener listener) {
		// adding referrer that notifies listener with update for this parameter
		// (instead of wrapped)
		this.defaultParameter.addListener(new ParameterListener() {
			@Override
			public void notifyValueChanged(Parameter<?> parameter) {
				listener.notifyValueChanged(DefaultSubCollectionParameter.this);
			}
		});
	}

	/**
	 * Refreshes the buffered range.
	 */
	public final void notifyValueChanged(Parameter<?> parameter) {
		;
	}

}

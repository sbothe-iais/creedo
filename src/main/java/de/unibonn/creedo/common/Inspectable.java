package de.unibonn.creedo.common;

import java.util.List;

/**
 * 
 * Unified interface for classes that want to expose internal state for
 * inspection. Information is provided by a name and a {@link InspectableObjState}.
 * 
 * Due to the class hierarchies, an inspectable can have several
 * subInspectables, then the information can be retrieved in a recursive manner.
 * 
 * @author mboley, bkang
 * 
 */
public interface Inspectable {

	public String getName();
	
	public List<String> getDescriptions();

	public List<Inspectable> getSubInspectables();

	public InspectableObjState getInspectableState();
}

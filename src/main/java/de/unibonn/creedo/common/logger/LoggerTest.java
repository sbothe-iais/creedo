package de.unibonn.creedo.common.logger;

import de.unibonn.realkd.common.logger.LogChannel;
import de.unibonn.realkd.common.logger.LogMessageType;
import de.unibonn.realkd.common.logger.Logger;

public class LoggerTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Logger.log("Promotion", "3,4,5,6",
				LogMessageType.INTRA_COMPONENT_MESSAGE);
		Logger.log("Survey", "Promotion", "3,4,5,6", LogMessageType.DEBUG_MESSAGE);
		Logger.log("Survey", "uid1234-Promotion", "3,4,5,6", LogMessageType.INTRA_COMPONENT_MESSAGE);

		Logger.log("Test", "TestMsg", LogMessageType.GLOBAL_STATE_CHANGE);

		LogChannel.DEFAULT.log("TestFromLogChannel", "TestFromLogChannel", LogMessageType.GLOBAL_STATE_CHANGE);
	}

}

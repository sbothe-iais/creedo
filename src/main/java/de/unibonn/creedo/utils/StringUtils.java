package de.unibonn.creedo.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StringUtils {

	/**
	 * Converts a string in the form [e1, e2, e3,...] to a list of strings with
	 * entries e1, e2, e3,...
	 * 
	 */
	public static List<String> jsonArrayToStringList(String argument) {
		argument = argument.substring(1, argument.length() - 1);
		if (argument.trim().length() == 0) {
			return Arrays.asList();
		}
		String[] split = argument.split(",");
		List<String> result = new ArrayList<>();
		for (String string : split) {
			String trimmed = string.trim();
			result.add(trimmed);
		}
		return result;
	}

}

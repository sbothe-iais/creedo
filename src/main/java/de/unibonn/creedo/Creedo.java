package de.unibonn.creedo;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.creedo.webapp.User;

/**
 * 
 * @author mboley, bjacobs, bkang, ptokmakov, eefendiyev, mktraudes, smoens,
 *         sdatta, rxu, mkamp
 *
 */
public class Creedo {

	private static final String NAME = "Creedo";

	private static final String COPYRIGHT = "&copy 2014-15 by the Creedo Contributors";

	private static String VERSION = "0.1";

	public static String getVersion() {
		return VERSION;
	}

	public static String getName() {
		return NAME;
	}

	public static String getCopyright() {
		return COPYRIGHT;
	}

	/**
	 * Returns creedo session object already bound to httpSession if one exists
	 * and otherwise creates a new UISession with a default user logged in,
	 * binds it to the httpSession, and returns it.
	 */
	public static CreedoSession getCreedoSession(HttpSession httpSession) {
		Object result = CreedoSession
				.retrieveObjectWithCreedoSessionKey(httpSession);
		if (!(result instanceof CreedoSession)) {
			CreedoSession
					.createNewCreedoSession(User.DEFAULT_USER, httpSession);
			result = CreedoSession
					.retrieveObjectWithCreedoSessionKey(httpSession);
		}

		return (CreedoSession) result;
	}

}

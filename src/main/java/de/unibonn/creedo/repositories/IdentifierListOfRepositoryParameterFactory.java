package de.unibonn.creedo.repositories;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import de.unibonn.creedo.common.DefaultSubCollectionParameter;
import de.unibonn.creedo.common.DefaultSubCollectionParameter.CollectionComputer;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;

/**
 * Sublist parameter that can take on all values that are sublists of the
 * identifier list of a given repository.
 * 
 * @see {@link SubCollectionParameter}, {@link Repository}
 * 
 * @author mboley
 *
 */
public final class IdentifierListOfRepositoryParameterFactory {

	public static <T> SubCollectionParameter<String, List<String>> getIdentifierListOfRepositoryParameter(
			final String name, final String description, final Repository<T> repository) {
		return getIdentifierListOfRepositoryParameter(name, description, repository, entry -> true); 
	}

	public static <T> SubCollectionParameter<String, List<String>> getIdentifierListOfRepositoryParameter(
			final String name, final String description, final Repository<T> repository,
			final Predicate<RepositoryEntry<T>> filterPredicate) {
		CollectionComputer<List<String>> collectionComputer = new CollectionComputer<List<String>>() {
			@Override
			public List<String> computeCollection() {
				return repository.getAll(filterPredicate).stream()
						.map(entry -> entry.getId())
						.collect(Collectors.toList());
			}
		};
		return DefaultSubCollectionParameter
				.getDefaultSubListParameter(name, description, collectionComputer);
	}
}

package de.unibonn.creedo.repositories.filesystimpl;

import static com.google.common.base.Preconditions.checkArgument;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.unibonn.creedo.repositories.Repository;
import de.unibonn.creedo.repositories.RepositoryEntry;
import de.unibonn.creedo.repositories.mybatisimpl.MyBatisRepositoryEntry;

public class FileSystemBasedRepository implements Repository<Path> {

	private final Path basePath;

	private final String name;

	public FileSystemBasedRepository(String name, Path basePath) {
		checkArgument(Files.isDirectory(basePath),
				"basePath %s does not exist", basePath);
		checkArgument(Files.exists(basePath),
				"basePath %s does not specify a directory", basePath);
		this.basePath = basePath;
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void add(String id, Path content) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void delete(String id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void update(RepositoryEntry<Path> entry) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<String> getAllIds() {
		List<String> result = new ArrayList<String>();
		DirectoryStream<Path> dirStream;
		try {
			dirStream = Files.newDirectoryStream(basePath);
			for (final Iterator<Path> it = dirStream.iterator(); it.hasNext();) {
				result.add(it.next().getFileName().toString());
			}
			return result;
		} catch (IOException e) {
			throw new IllegalStateException("could not access base path "
					+ basePath);
		}
	}

	@Override
	public RepositoryEntry<Path> get(String id) {
		// return FileSystems.getDefault().getPath(first, more)
		return new MyBatisRepositoryEntry<Path>(id, basePath.resolve(id));
	}

	@Override
	public List<RepositoryEntry<Path>> getAll() {
		List<RepositoryEntry<Path>> result = new ArrayList<>();
		DirectoryStream<Path> stream;
		try {
			stream = Files.newDirectoryStream(basePath);
			Iterator<Path> iterator = stream.iterator();
			while (iterator.hasNext()) {
				Path next = iterator.next();
				if (Files.isRegularFile(next)) {
					result.add(new MyBatisRepositoryEntry<Path>(next
							.getFileName().toString(), next));
				}
			}
		} catch (IOException e) {
			throw new IllegalStateException("Failure during directory access.",
					e);
		}
		return result;
	}

}

package de.unibonn.creedo.repositories;

import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Abstract interface for an indexed collection of an arbitrary type that a) can
 * be instantiated by a 0-argument constructor and that b) either completely
 * sets its state by this constructor or is an instance of
 * {@link ParameterContainer}.
 * 
 * @author mboley
 *
 * @param <T>
 *            the type of the repository entries
 * 
 */
public interface Repository<T> {

	public String getName();

	public void add(String id, T content);

	public void delete(String id);

	public void update(RepositoryEntry<T> entry);

	public List<String> getAllIds();

	public RepositoryEntry<T> get(String id);

	public Collection<RepositoryEntry<T>> getAll();

	public default Collection<RepositoryEntry<T>> getAll(
			Predicate<RepositoryEntry<T>> predicate) {
		Collection<RepositoryEntry<T>> all = this.getAll();
		return all.stream().filter(predicate).collect(Collectors.toList());
	}

	public default void clear() {
		getAllIds().forEach(new Consumer<String>() {
			@Override
			public void accept(String id) {
				delete(id);
			}
		});
	}

}

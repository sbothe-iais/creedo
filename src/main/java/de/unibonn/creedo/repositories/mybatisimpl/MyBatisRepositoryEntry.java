package de.unibonn.creedo.repositories.mybatisimpl;

import de.unibonn.creedo.repositories.RepositoryEntry;

public class MyBatisRepositoryEntry<T> implements RepositoryEntry<T> {

	private String id;
	private T content;

	public MyBatisRepositoryEntry(String id, T content) {
		this.id = id;
		this.content = content;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public T getContent() {
		return content;
	}

}

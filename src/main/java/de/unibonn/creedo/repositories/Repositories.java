package de.unibonn.creedo.repositories;

import java.util.Arrays;
import java.util.Iterator;
import java.util.function.Predicate;
import java.util.regex.Pattern;

//import com.google.common.base.Function;
//import com.google.common.base.Predicate;

/**
 * <p>
 * Provides static methods for working with {@link Repository} objects. In
 * particular, it allows to obtain predicates that can be used for filtering
 * repositories via {@link Repository#getAll(Predicate)}.
 * </p>
 * 
 * <p>
 * The predicates returned by methods of this class might allow the actual
 * repository implementation to provide a more efficient filtering than the
 * default fallback of naively iterating through through all repository
 * elements.
 * </p>
 * 
 * @author mboley
 *
 */
public class Repositories {

	// public static Pattern IMAGE_FILENAME_PATTERN = Pattern
	// .compile("([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)");
	//
	// public static Pattern FILENAME_WITH_HTML_EXT = Pattern
	// .compile("([^\\s]+(\\.(?i)(html))$)");

	public static String[] HTML_FILE_EXTENSIONS = { "htm", "html" };

	public static String[] IMAGE_FILE_EXTENSIONS = { "jpg", "png", "gif", "bmp" };

//	private static Predicate<RepositoryEntry> ENTRY_PREDICATE = new Predicate<RepositoryEntry>() {
//		@Override
//		public boolean apply(RepositoryEntry entry) {
//			return true;
//		}
//	};

//	@SuppressWarnings("rawtypes")
//	public static Function<RepositoryEntry, String> ENTRY_TO_ID_FUNCTION = new Function<RepositoryEntry, String>() {
//		@Override
//		public String apply(RepositoryEntry entry) {
//			return entry.getId();
//		}
//	};

//	public static Predicate<RepositoryEntry> getEntryPredicate() {
//		return ENTRY_PREDICATE;
//	}

	public static <T> Predicate<RepositoryEntry<T>> getIdMatchingRegexPredicate(
			final Pattern regexPattern) {
		return new Predicate<RepositoryEntry<T>>() {

			@Override
			public boolean test(RepositoryEntry<T> entry) {
				return regexPattern.matcher(entry.getId()).matches();
			}
		};
	}

	public static <T> Predicate<RepositoryEntry<T>> getIdIsFilenameWithExtensionPredicate(
			String... extensions) {
		StringBuilder extensionPart = new StringBuilder();
		Iterator<String> extIterator = Arrays.asList(extensions).iterator();
		while (extIterator.hasNext()) {
			extensionPart.append(extIterator.next());
			if (extIterator.hasNext()) {
				extensionPart.append("|");
			}
		}
		Pattern pattern = Pattern.compile("([^\\s]+(\\.(?i)("
				+ extensionPart.toString() + "))$)");

		return getIdMatchingRegexPredicate(pattern);
	}

}

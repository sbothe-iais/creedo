package de.unibonn.creedo.webapp.handler;

import static de.unibonn.realkd.common.logger.LogChannel.UI;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.WebConstants;
import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.realkd.common.logger.LogMessageType;

/**
 * A handler that internally uses a thread for registering Timeouts.
 * This class checks regularly if a PING from the Client was received.
 * If so, the timestamp of the last ping is updated. If the time between
 * the last ping exceeds the allowed time-frame, the tear-down of the
 * current AnalysisSession is initiated.
 * 
 * @author bjacobs
 */
public class TimeoutHandler {

	private static final int hardTimeoutTimeframe = WebConstants.HARD_TIMEOUT_MILLIS;
	private static final int checkInterval = WebConstants.TIMEOUT_CHECK_INTERVAL_MILLIS;
	private final TimeoutChecker timeoutChecker;
	private final CreedoSession uiSession;

	private volatile long lastPingTimeStamp;

	public TimeoutHandler(HttpSession session) {
		this.lastPingTimeStamp = System.currentTimeMillis();

		Object uiSessionObj = session.getAttribute(WebConstants.UI_SESSION_KEY);
		if (!(uiSessionObj instanceof CreedoSession)) {
			throw new IllegalArgumentException(
					"Given HttpSession object does not contain UISession object!");
		}

		uiSession = (CreedoSession) uiSessionObj;

		timeoutChecker = new TimeoutChecker();
		Thread timeoutCheckerThread = new Thread(timeoutChecker);
		timeoutCheckerThread.start();
	}

	public void ping() {
		UI.log("PING received!", LogMessageType.DEBUG_MESSAGE);
		lastPingTimeStamp = System.currentTimeMillis();
	}

	public void stop() {
		this.timeoutChecker.stop();
	}

	private class TimeoutChecker implements Runnable {

		private volatile boolean continueLoop = true;

		public TimeoutChecker() {
		}

		@Override
		public void run() {
			UI.log("Starting Timeout thread",
					LogMessageType.GLOBAL_STATE_CHANGE);
			while (continueLoop) {
				try {
					Thread.sleep(checkInterval);
				} catch (InterruptedException e) {
					e.printStackTrace();
					continueLoop = false;
				}

				long currentTime = System.currentTimeMillis();
				long difference = currentTime - lastPingTimeStamp;

				UI.log("Timeout?: " + difference + " > " + hardTimeoutTimeframe + "?",
						LogMessageType.DEBUG_MESSAGE);

				if (difference > hardTimeoutTimeframe) {
					// Tear-down AnalysisSession
					uiSession.tearDownDashboard();

					// and stop this thread
					continueLoop = false;
				}
			}
			UI.log("TimeoutHandler was requested to stop.",
					LogMessageType.GLOBAL_STATE_CHANGE);
		}

		public void stop() {
			continueLoop = false;
		}
	}
}

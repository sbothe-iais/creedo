package de.unibonn.creedo.webapp.studies;

import org.apache.ibatis.annotations.Param;

/**
 * User: bjacobs
 * Date: 30.10.14
 * Time: 15:01
 */

public interface ResultMapper {
	public void insert(
		@Param("session_id") int sessionId,
		@Param("result_builder_content") String resultBuilderContent,
		@Param("result_seconds_until_saved") int secondsUntilSaved);
}

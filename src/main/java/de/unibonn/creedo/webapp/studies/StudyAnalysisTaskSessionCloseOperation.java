package de.unibonn.creedo.webapp.studies;

import de.unibonn.creedo.studies.NewSessionDAO;
import de.unibonn.creedo.webapp.dashboard.DashboardModel;
import de.unibonn.creedo.webapp.dashboard.closehandlers.CloseOperation;
import de.unibonn.creedo.webapp.dashboard.mining.MiningDashboardModel;
import de.unibonn.creedo.webapp.studies.result.ResultCreator;

/**
 * @author bjacobs bkang
 */

public class StudyAnalysisTaskSessionCloseOperation implements CloseOperation {

	private final String studyName;
	private final int userId;
	private final String taskName;
	private final String systemName;

	private final ResultCreator resultCreator;

	/*
	 * public StudyAnalysisTaskSessionCloseOperation(Study study, User user,
	 * AnalysisTask task) { this.study = study; this.user = user; this.task =
	 * task; }
	 */

	public StudyAnalysisTaskSessionCloseOperation(String studyName, int userId,
			String taskName, String systemName, ResultCreator resultCreator) {
		this.studyName = studyName;
		this.userId = userId;
		this.taskName = taskName;
		this.systemName = systemName;
		this.resultCreator = resultCreator;
	}

	@Override
	public boolean isApplicable(DashboardModel dashboardModel) {
		return dashboardModel instanceof MiningDashboardModel;
	}

	@Override
	public void apply(DashboardModel dashboardModel) {
		// Get MiningDashboardModel
		if (!(dashboardModel instanceof MiningDashboardModel)) {
			throw new IllegalArgumentException(
					"can only be applied to mining dashboard");
		}
		MiningDashboardModel miningDashboardModel = (MiningDashboardModel) dashboardModel;
//		new SessionDAO().create(studyName, userId, taskName,
//				resultCreator.getPatternBuilders(miningDashboardModel),
//				resultCreator.getListOfSecondsUntilSaved(miningDashboardModel));
		NewSessionDAO.INSTANCE.saveSession(studyName, userId, taskName,
				systemName,
				resultCreator.getPatternBuilders(miningDashboardModel),
				resultCreator.getListOfSecondsUntilSaved(miningDashboardModel));
	}

	@Override
	public String getMessage() {
		return "Stores all patterns contained in results of MiningDashboardModel to DB";
	}
}

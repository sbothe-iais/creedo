package de.unibonn.creedo.webapp.studies;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.RepositoryEntry;
import de.unibonn.creedo.studies.NewStudy;
import de.unibonn.creedo.studies.StudyBuilder;
import de.unibonn.creedo.ui.indexpage.DashboardLink;
import de.unibonn.creedo.webapp.User;

/**
 * Builds all study-related dashboards for a user.
 *
 * @author mboley, bjacobs
 */
public class StudyEngine {

    public List<DashboardLink> getSurveyDashboardLinks(User user) {
		List<DashboardLink> results = new ArrayList<>();		
		for (RepositoryEntry<StudyBuilder> studyEntry : ApplicationRepositories.STUDY_REPOSITORY.getAll()) {
			NewStudy study = studyEntry.getContent().build();
			results.addAll(study.getDashboardLinks(user));
		}
		return results;
	}
}

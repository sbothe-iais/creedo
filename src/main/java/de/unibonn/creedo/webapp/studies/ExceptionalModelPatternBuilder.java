/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.creedo.webapp.studies;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collection;

import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.Description;
import de.unibonn.realkd.patterns.DescriptionBuilder;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.TargetAttributesBuilder;
import de.unibonn.realkd.patterns.emm.DefaultExceptionalModelPatternBuilder;
import de.unibonn.realkd.patterns.emm.ModelDistanceFunction;
import de.unibonn.realkd.patterns.emm.ModelFactory;

/**
 * @author bjacobs
 */
public class ExceptionalModelPatternBuilder implements PatternBuilder {
	private final DescriptionBuilder description;
	private final TargetAttributesBuilder targetAttributes;
	private final String modelFactoryClassName;
	private final String distanceFunctionClassName;

	public ExceptionalModelPatternBuilder(
			DescriptionBuilder descriptionBuilder,
			TargetAttributesBuilder targetAttributesBuilder,
			Class<? extends ModelFactory> modelFactoryClass,
			Class<? extends ModelDistanceFunction> functionClass) {
		this.description = descriptionBuilder;
		this.targetAttributes = targetAttributesBuilder;
		this.modelFactoryClassName = modelFactoryClass.getName();
		this.distanceFunctionClassName = functionClass.getName();
	}

	@Override
	public Pattern build(PropositionalLogic logic) {
		Description description = this.description.build(logic);
		Collection<Attribute> attributes = targetAttributes.build(logic
				.getDatatable());
		ModelFactory modelFactory;
		ModelDistanceFunction distanceFunction;

		try {
			Class<ModelFactory> modelFactoryClass = (Class<ModelFactory>) Class
					.forName(modelFactoryClassName);
			Constructor<ModelFactory> factoryConstructor = modelFactoryClass
					.getDeclaredConstructor();
			factoryConstructor.setAccessible(true);
			modelFactory = factoryConstructor.newInstance();
		} catch (Exception e) {
			throw new IllegalArgumentException(
					"Could not find or instantiate ModelFactory by class name: "
							+ modelFactoryClassName, e);
		}

		try {
			Class<ModelDistanceFunction> distanceFunctionClass = (Class<ModelDistanceFunction>) Class
					.forName(distanceFunctionClassName);
			Constructor<ModelDistanceFunction> functionConstructor = distanceFunctionClass
					.getDeclaredConstructor();
			functionConstructor.setAccessible(true);
			distanceFunction = functionConstructor.newInstance();
		} catch (Exception e) {
			throw new IllegalArgumentException(
					"Could not find or instantiate ModelDistanceFunction by class name: "
							+ distanceFunctionClassName, e);
		}

	    	return new DefaultExceptionalModelPatternBuilder(logic, new ArrayList<>(attributes),
			modelFactory, distanceFunction).build(description);

/*
		return new ExceptionalModelPattern(logic,
				description, new ArrayList<>(attributes), modelFactory,
				distanceFunction, localModel, globalModel, measures);
*/
	}
}

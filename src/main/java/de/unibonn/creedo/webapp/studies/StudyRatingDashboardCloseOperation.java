package de.unibonn.creedo.webapp.studies;

import java.util.List;

import de.unibonn.creedo.studies.RatingDAO;
import de.unibonn.creedo.webapp.dashboard.DashboardModel;
import de.unibonn.creedo.webapp.dashboard.closehandlers.CloseOperation;
import de.unibonn.creedo.webapp.dashboard.study.RatingDashboardModel;
import de.unibonn.creedo.webapp.studies.rating.ResultRating;

/**
 * @author bkang
 */
public class StudyRatingDashboardCloseOperation implements CloseOperation {

    private final int numberOfResults;
    
    private final String studyName;

    public StudyRatingDashboardCloseOperation(String studyName, int numberOfResult) {
        this.numberOfResults = numberOfResult;
        this.studyName = studyName;
    }


    @Override
    public boolean isApplicable(DashboardModel dashboardModel) {
        return (dashboardModel instanceof RatingDashboardModel && ((RatingDashboardModel) dashboardModel).haveCollectedRequiredNumberOfRatings())? true: false;
    }

    @Override
    public void apply(DashboardModel dashboardModel) {
        // Get RatingDashboardModel
        if (!(dashboardModel instanceof RatingDashboardModel)) {
            throw new IllegalArgumentException(
                    "can only be applied to rating dashboard");
        }
        RatingDashboardModel ratingDashboardModel = (RatingDashboardModel) dashboardModel;

        List<ResultRating> resultRatings = ratingDashboardModel.getResultRatings();

//		new ResultRatingDAO().create(resultRatings);
		RatingDAO.INSTANCE.saveRating(studyName, resultRatings);
	}

    @Override
    public String getMessage() {
        return "Please rate for all " + numberOfResults + " results.";
    }
}

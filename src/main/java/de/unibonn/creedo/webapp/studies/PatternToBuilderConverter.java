package de.unibonn.creedo.webapp.studies;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import de.unibonn.realkd.patterns.Description;
import de.unibonn.realkd.patterns.DescriptionBuilder;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.TargetAttributesBuilder;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.emm.ModelDistanceFunction;
import de.unibonn.realkd.patterns.emm.ModelFactory;
import de.unibonn.realkd.patterns.patternset.PatternSet;

/**
 * @author bjacobs bkang
 */

public class PatternToBuilderConverter {

	public static List<PatternBuilder> getBuilders(List<Pattern> patterns) {
		List<PatternBuilder> patternBuilders = new ArrayList<>();
		for (Pattern pattern : patterns) {
			patternBuilders.add(getBuilder(pattern));
		}
		return patternBuilders;
	}

	public static PatternBuilder getBuilder(Pattern pattern) {
		if (pattern instanceof Association) {
			return getAssociationPatternBuilder((Association) pattern);
		} else if (pattern instanceof ExceptionalModelPattern) {
			try {
				return getEMMPatternBuilder((ExceptionalModelPattern) pattern);
			} catch (NoSuchFieldException | IllegalAccessException e) {
				e.printStackTrace();
			}
		} else if (pattern instanceof PatternSet) {
			return getPatternSetBuilder((PatternSet) pattern);
		}
		throw new IllegalArgumentException("Unsupported Pattern type: "
				+ pattern.getClass().getSimpleName());
	}

	private static PatternBuilder getEMMPatternBuilder(
			ExceptionalModelPattern emPattern) throws NoSuchFieldException,
			IllegalAccessException {
		// Acquire model factory
		Field factoryField = ExceptionalModelPattern.class
				.getDeclaredField("modelFactory");
		factoryField.setAccessible(true);

		ModelFactory factory = (ModelFactory) factoryField.get(emPattern);

		// Acquire distance function
		Field functionField = ExceptionalModelPattern.class
				.getDeclaredField("modelDistanceFunction");
		functionField.setAccessible(true);

		ModelDistanceFunction function = (ModelDistanceFunction) functionField
				.get(emPattern);

		// Get classes

		Class<? extends ModelFactory> modelFactoryClass = factory.getClass();
		Class<? extends ModelDistanceFunction> functionClass = function
				.getClass();

		return new ExceptionalModelPatternBuilder(new DescriptionBuilder(
				emPattern.getDescription().getElements()),
				new TargetAttributesBuilder(emPattern.getPropositionalLogic()
						.getDatatable(), emPattern.getAttributes()),
				modelFactoryClass, functionClass);
	}

	private static PatternBuilder getAssociationPatternBuilder(
			Association pattern) {
		Description description = pattern.getDescription();

		return new AssociationPatternBuilder(new DescriptionBuilder(
				description.getElements()));
	}

	private static PatternBuilder getPatternSetBuilder(PatternSet patternSet) {
		List<PatternBuilder> patternBuilders = new ArrayList<>();
		for (Pattern pattern : patternSet.getPatterns()) {
			patternBuilders.add(getBuilder(pattern));
		}
		return new PatternSetBuilder(patternBuilders);
	}
}

package de.unibonn.creedo.webapp;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.RepositoryEntry;
import de.unibonn.creedo.ui.indexpage.DashboardLink;
import de.unibonn.creedo.ui.indexpage.DashboardLinkBuilder;


/**
 * Compiles the list of demo links available to a user.
 * 
 * @author bjacobs
 * 
 */
public class DemoProvider {

	public static DemoProvider INSTANCE=new DemoProvider();

	private DemoProvider() {
	}

	public List<DashboardLink> build(User user) {
		List<DashboardLink> results = new ArrayList<>();
		for (RepositoryEntry<DashboardLinkBuilder> demoEntry: ApplicationRepositories.DEMO_REPOSITORY.getAll()) {
			results.add(demoEntry.getContent().build(user));
		}
		return results;
	}
	
}

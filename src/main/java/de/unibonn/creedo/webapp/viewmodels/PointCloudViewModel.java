package de.unibonn.creedo.webapp.viewmodels;

import static de.unibonn.realkd.common.logger.LogChannel.UI;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.unibonn.realkd.common.logger.LogMessageType;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.CategoricalAttribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import weka.core.matrix.EigenvalueDecomposition;

public class PointCloudViewModel {

	public static class JsonPointCloudDataEntry {
		private final Double x;
		private final Double y;
		private final String objName;
		private final int id;

		public JsonPointCloudDataEntry(int id, Double x, Double y,
				String objName) {
			this.x = x;
			this.y = y;
			this.objName = objName;
			this.id = id;
		}

		public Double getX() {
			return x;
		}

		public Double getY() {
			return y;
		}

		public String getObjName() {
			return objName;
		}

		public int getId() {
			return id;
		}
	}

	private List<List<Double>> transactionFeatureList;
	private List<Double> xCoordinates;
	private List<Double> yCoordinates;
	private List<Double> normXCoordinates;
	private List<Double> normYCoordinates;
	private final DataTable dataTable;

	public PointCloudViewModel(DataTable dataTable) {
		UI.log("Creating new point cloud view model",
				LogMessageType.INTER_COMPONENT_MESSAGE);
		this.transactionFeatureList = getTransactionFeatureList(dataTable);
		doPrincipalComponentAnalysis();
		this.dataTable=dataTable;
	}

	private void doPrincipalComponentAnalysis() {
		int maxRowSize = 0;
		for (List<Double> aTransactionFeatureList : transactionFeatureList) {
			if (maxRowSize < aTransactionFeatureList.size())
				maxRowSize = aTransactionFeatureList.size();
		}
		weka.core.matrix.Matrix dataMatrix = new weka.core.matrix.Matrix(
				transactionFeatureList.size(), maxRowSize);
		weka.core.matrix.Matrix centralizedDataMatrix = new weka.core.matrix.Matrix(
				transactionFeatureList.size(), maxRowSize);
		weka.core.matrix.Matrix covarianceMatrix = new weka.core.matrix.Matrix(
				maxRowSize, maxRowSize);
		double[] columnMean = new double[dataMatrix.getColumnDimension()];
		for (int i = 0; i < transactionFeatureList.size(); i++) {
			for (int j = 0; j < maxRowSize; j++) {
				dataMatrix.set(i, j, transactionFeatureList.get(i).get(j));
			}
		}
		for (int i = 0; i < dataMatrix.getColumnDimension(); i++) {
			double columnTotal = 0.0;
			for (int j = 0; j < dataMatrix.getRowDimension(); j++) {
				columnTotal += dataMatrix.get(j, i);
			}
			columnMean[i] = columnTotal / dataMatrix.getRowDimension();
		}

		for (int i = 0; i < dataMatrix.getColumnDimension(); i++) {
			for (int j = 0; j < dataMatrix.getRowDimension(); j++) {
				centralizedDataMatrix.set(j, i, dataMatrix.get(j, i)
						- columnMean[i]);
			}
		}
		covarianceMatrix = ((centralizedDataMatrix.transpose())
				.times(centralizedDataMatrix)).times(1.0 / (maxRowSize - 1.0));
		EigenvalueDecomposition eigValDecomp = new EigenvalueDecomposition(
				covarianceMatrix);
		weka.core.matrix.Matrix eigenVectors = eigValDecomp.getV();
		weka.core.matrix.Matrix highestEigenVectors = eigenVectors.getMatrix(0,
				(eigenVectors.getRowDimension() - 1),
				(eigenVectors.getColumnDimension() - 2),
				(eigenVectors.getColumnDimension() - 1));
		weka.core.matrix.Matrix finalData = centralizedDataMatrix
				.times(highestEigenVectors);
		xCoordinates = new ArrayList<>(finalData.getRowDimension());
		yCoordinates = new ArrayList<>(finalData.getRowDimension());

		for (int i = 0; i < finalData.getRowDimension(); i++) {
			xCoordinates.add(finalData.get(i, 1));
		}
		for (int i = 0; i < finalData.getRowDimension(); i++) {
			yCoordinates.add(finalData.get(i, 0));
		}

		normXCoordinates = normalizeZeroToHundred(xCoordinates);
		normYCoordinates = normalizeZeroToHundred(yCoordinates);

	}

	public List<Double> getNormalizedXCoordinates() {
		return normXCoordinates;
	}

	public List<Double> getNormalizedYCoordinates() {
		return normYCoordinates;
	}

	public List<Double> getAbsoluteXCoordinates() {
		return xCoordinates;
	}

	public List<Double> getAbsoluteYCoordinates() {
		return yCoordinates;
	}

	private List<Double> normalizeZeroToHundred(List<Double> data) {
		List<Double> result = new ArrayList<>(data.size());

		double range = Collections.max(data) - Collections.min(data);
		for (int i = 0; i < data.size(); i++) {
			// double normalized = 100.0 * (0.05 * range + data.get(i) -
			// Collections.min(data)) / (1.1 * range);
			double normalized = 100.0 * (data.get(i) - Collections.min(data))
					/ (range);

			result.add(normalized);
		}

		return result;
	}

	public List<List<Double>> getTransactionFeatureList(DataTable dataTable) {
		List<List<Double>> transFeatureList = new ArrayList<>();
		for (Integer row : dataTable.getObjectIds()) {
			transFeatureList.add(createFeatureVectorFromTrans(row, dataTable));
		}
		return transFeatureList;
	}

	/**
	 * Returns numeric vector for object. Missing numerical values are replaced
	 * by attribute median. Categorical values are replaced by a number of
	 * numeric elements corresponding to the different categories (with a 1
	 * entry for the matching category and 0 elsewhere).
	 */
	private List<Double> createFeatureVectorFromTrans(Integer row,
			DataTable dataTable) {
		List<Double> transFeature = new ArrayList<>();
		for (Attribute attribute : dataTable.getAttributes()) {
			if (attribute instanceof CategoricalAttribute) {

				List<String> categories = ((CategoricalAttribute) attribute)
						.getCategories();

				for (String category : categories) {
					if (!attribute.isValueMissing(row)
							&& attribute.getValue(row).equals(category)) {
						transFeature.add(1.);
					} else {
						transFeature.add(0.);
					}
				}

			} else if (attribute instanceof MetricAttribute) {
				if (attribute.isValueMissing(row)) {
					transFeature.add(((MetricAttribute) attribute).getMedian());
				} else {
					transFeature.add(((MetricAttribute) attribute)
							.getValue(row));
				}
			}
		}
		return transFeature;
	}

	public List<PointCloudViewModel.JsonPointCloudDataEntry> getPoints() {
		List<String> objNames = dataTable.getObjectNames();

		List<PointCloudViewModel.JsonPointCloudDataEntry> result = new ArrayList<>();
		// Zip
		for (int i = 0; i < this.getNormalizedXCoordinates().size(); i++) {
			double x = this.getNormalizedXCoordinates().get(i);
			double y = this.getNormalizedYCoordinates().get(i);
			String objName = objNames.get(i);

			result.add(new PointCloudViewModel.JsonPointCloudDataEntry(i, x, y,
					objName));
		}

		return result;
	}

}

package de.unibonn.creedo.webapp.viewmodels;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.common.ConfigurationProperties;
import de.unibonn.creedo.ui.ResourceUploadPage;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.webapp.CreedoSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Controller that serves and stores content from the resources folder,
 * especially html files and images.
 *
 * @author bjacobs
 */
@Controller
//@RequestMapping(value = "/")
public class ResourceController {
    private final Pattern filenamePattern = Pattern.compile("^[\\.0-9a-zA-Z_-]+$");

    @Autowired
    private HttpSession httpSession;

    @RequestMapping(value = "file.htm")
    public ResponseEntity<FileSystemResource> serveImage(@RequestParam("filename") String filename) {
        Matcher matcher = filenamePattern.matcher(filename);

        if (!matcher.matches()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            String fullpath = ConfigurationProperties.get().USER_FOLDER + "/" + filename;

            try {
                String s = Files.probeContentType(FileSystems.getDefault().getPath(fullpath));
                HttpHeaders responseHeaders = new HttpHeaders();
                responseHeaders.setContentType(MediaType.parseMediaType(s));

                return new ResponseEntity<>(new FileSystemResource(fullpath), responseHeaders, HttpStatus.OK);
            } catch (IOException e) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        }

    }

    /**
     * Handler that delegates the post request content to the ResourceUploadPage
     * component which in turn is responsible for storing the content.
     *
     * @see ResourceUploadPage
     */
    @RequestMapping(value = "uploadResource.htm", method = RequestMethod.POST)
    public String uploadResource(
            @RequestParam("componentId") Integer componentId,
            @RequestParam("file") MultipartFile[] files,
            @RequestHeader(value = "referer", required = false) final String referer) {

        try {
            CreedoSession creedoSession = Creedo.getCreedoSession(httpSession);
            UiComponent uiComponent = creedoSession.getUiComponent(componentId);
            ((ResourceUploadPage) uiComponent).fileUploadPerformed(files);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:" + referer;
    }

}

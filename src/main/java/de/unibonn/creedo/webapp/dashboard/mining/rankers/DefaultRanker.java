package de.unibonn.creedo.webapp.dashboard.mining.rankers;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.realkd.patterns.Pattern;

public class DefaultRanker implements Ranker {
	@Override
	public List<Pattern> rank(List<Pattern> patterns) {
		return new ArrayList<>(patterns);
	}
}

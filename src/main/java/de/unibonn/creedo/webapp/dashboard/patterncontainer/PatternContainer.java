package de.unibonn.creedo.webapp.dashboard.patterncontainer;


/**
 * Interface for all dashboards that contains patterns. Conceptually, a
 * webPattern can be identified by the dashboard it is contained in and a
 * unique id within that dashboard.
 * 
 * @author mboley
 * 
 */
public interface PatternContainer {

	/**
	 * Retrieves pattern by id. Note that ids are potentially only unique within
	 * the context of this dashboard.
	 * 
	 * @return webPattern matching the id
	 * @throws IllegalArgumentException
	 *             if no pattern with matching id
	 * @param id
	 */
	public abstract WebPattern getPattern(int id);

}

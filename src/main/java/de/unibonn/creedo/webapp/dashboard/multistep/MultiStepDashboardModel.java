package de.unibonn.creedo.webapp.dashboard.multistep;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.ActionLink;
import de.unibonn.creedo.ui.core.ActionProvider;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.ui.core.UiRegister;
import de.unibonn.creedo.ui.standard.Navbar;
import de.unibonn.creedo.webapp.dashboard.AnalyticsDashboardBuilder;
import de.unibonn.creedo.webapp.dashboard.CloseHandler;
import de.unibonn.creedo.webapp.dashboard.DashboardModel;
import de.unibonn.creedo.webapp.dashboard.mining.CloseAction;

/**
 * Dashboard that aggregates a number of sub-dashboard through which the user
 * can progress linearly.
 * 
 * @author bjacobs, mboley
 * 
 */
public class MultiStepDashboardModel implements DashboardModel, ActionProvider {
	private final List<AnalyticsDashboardBuilder> builders;
	private final List<String> nextAndCloseNames;
	private final HttpSession session;
	private final int id;
	private final CloseHandler closeHandler;
	private int step = 0;
	private DashboardModel current;
	private final UiRegister frameFactory;
	private Navbar navbar;

	/**
	 * 
	 * @param builders
	 *            list of dashboard builders for each step
	 * @param nextAndCloseNames
	 *            list of names to be used for next button in each step
	 *            (respectively for close button on last step)
	 * @param session
	 *            httpSession in which dashboard is to be displayed
	 * @param closeHandler
	 *            handler that handles the close button requests from the user
	 */
	public MultiStepDashboardModel(int id, UiRegister frameFactory,
			List<AnalyticsDashboardBuilder> builders,
			List<String> nextAndCloseNames, HttpSession session,
			CloseHandler closeHandler) {
		if (builders == null) {
			throw new IllegalArgumentException("Builders must not be null");
		}
		if (nextAndCloseNames == null) {
			throw new IllegalArgumentException("Names must not be null");
		}
		if (builders.size() != nextAndCloseNames.size()) {
			throw new IllegalArgumentException(
					"Number of builders must match number of button names");
		}
		this.builders = builders;
		this.nextAndCloseNames = nextAndCloseNames;
		this.session = session;
		this.id = id;
		// this.id = session.getId() + System.currentTimeMillis();
		this.closeHandler = closeHandler;
		this.frameFactory = frameFactory;

		current = frameFactory.createFrameFromBuilder(builders.get(step));
		initNavbar();
		// current = builders.get(step).build(session);
	}

	private void initNavbar() {
		if (step < builders.size() - 1) {
			this.navbar = new Navbar(frameFactory.getNextId(),
					Arrays.asList(new ActionLink(new Action() {
						
						private final int id=Creedo.getCreedoSession(session)
								.getUiRegister().getNextId();

						@Override
						public String getReferenceName() {
							return nextAndCloseNames.get(step);
						}

						@Override
						public ResponseEntity<String> activate(String... params) {
							current.tearDown();

							step++;
							if (step == builders.size()) {
								throw new IllegalStateException(
										"No next step available");
							}

							// current = builders.get(step).build(session);
							current = frameFactory
									.createFrameFromBuilder(builders.get(step));
							initNavbar();
							return new ResponseEntity<String>(HttpStatus.OK);
						}

						@Override
						public ClientWindowEffect getEffect() {
							return ClientWindowEffect.REFRESH;
						}

						@Override
						public int getId() {
							return id;
						}
					})));
			// navbarModelConfigurer.setShowNextButton(true);
			// navbarModelConfigurer
			// .setNextButtonName(nextAndCloseNames.get(step));
			// } else {
			// navbarModelConfigurer.setShowCloseButton(true);
			// navbarModelConfigurer.setCloseButtonName(nextAndCloseNames
			// .get(step));
		} else {
			this.navbar = new Navbar(this.frameFactory.getNextId(),
					Arrays.asList(new ActionLink(new CloseAction(session,
							closeHandler, current))));
		}
	}

	@Override
	public void tearDown() {
		current.tearDown();
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public ModelAndView getModelAndView() {
		// ModelAndView view = current.getModelAndView();
		// ModelMap modelMap = view.getModelMap();
		// PageModelConfigurer navbarModelConfigurer = new PageModelConfigurer(
		// modelMap);
		//
		// // navbarModelConfigurer.setShowCloseButton(step == builders.size() -
		// // 1);
		//
		// if (step < builders.size() - 1) {
		// navbarModelConfigurer.setShowNextButton(true);
		// navbarModelConfigurer
		// .setNextButtonName(nextAndCloseNames.get(step));
		// } else {
		// navbarModelConfigurer.setShowCloseButton(true);
		// navbarModelConfigurer.setCloseButtonName(nextAndCloseNames
		// .get(step));
		// }
		// navbarModelConfigurer.addAttributesToModelMap(modelMap);
		// return view;
		return new ModelAndView(getView(), getModel().asMap());
	}

	public void nextStep() {
		current.tearDown();

		step++;
		if (step == builders.size()) {
			throw new IllegalStateException("No next step available");
		}

		// current = builders.get(step).build(session);
		current = frameFactory.createFrameFromBuilder(builders.get(step));
		initNavbar();
	}

	public DashboardModel getCurrentDashboard() {
		return current;
	}

	@Override
	public DashboardModel getDashboard(int id) {
		if (id == this.id) {
			// if (id.equals(this.id)) {
			return this;
		} else {
			return current.getDashboard(id);
		}
	}

	@Override
	public String getView() {
		return current.getView();
	}

	@Override
	public Model getModel() {
		Model model = current.getModel();
		model.addAttribute("frameId", getId());
		model.addAllAttributes(navbar.getModel().asMap());
		return model;
	}

	@Override
	public List<UiComponent> getComponents() {
		return Arrays.asList();
	}

	@Override
	public Collection<Integer> getActionIds() {
		return navbar.getActionIds();
	}

	@Override
	public ResponseEntity<String> performAction(int id, String... params) {
		return navbar.performAction(id, params);
	}

	@Override
	public boolean isActionAvailable(int id) {
		return navbar.isActionAvailable(id);
	}
}

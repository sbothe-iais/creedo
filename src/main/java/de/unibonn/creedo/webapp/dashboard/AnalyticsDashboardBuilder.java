package de.unibonn.creedo.webapp.dashboard;

import javax.servlet.http.HttpSession;

/**
 * Interface implemented by classes that represent configurations of dashboards.
 *
 * @author bjacobs
 */
public interface AnalyticsDashboardBuilder {
	
	public DashboardModel build(int frameId, HttpSession session);

	public AnalyticsDashboardBuilder setCloseHandler(CloseHandler closeHandler);
	
}

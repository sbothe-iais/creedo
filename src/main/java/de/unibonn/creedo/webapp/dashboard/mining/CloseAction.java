package de.unibonn.creedo.webapp.dashboard.mining;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.webapp.dashboard.CloseHandler;
import de.unibonn.creedo.webapp.dashboard.DashboardModel;
import de.unibonn.creedo.webapp.dashboard.closehandlers.CloseHandlerResponse;

public class CloseAction implements Action {

	private final HttpSession httpSession;

	private final CloseHandler closeHandler;

	private final DashboardModel dashboardModel;

	private final int id;

	public CloseAction(HttpSession httpSession, CloseHandler closeHandler,
			DashboardModel dashboardModel) {
		this.id = Creedo.getCreedoSession(httpSession).getUiRegister()
				.getNextId();
		this.httpSession = httpSession;
		this.closeHandler = closeHandler;
		this.dashboardModel = dashboardModel;
	}

	@Override
	public String getReferenceName() {
		return "Close";
	}

	@Override
	public ResponseEntity<String> activate(String... params) {
		CloseHandlerResponse response = closeHandler
				.attemptToClose(dashboardModel);
		if (response.isSuccess()) {
			Creedo.getCreedoSession(httpSession).tearDownDashboard();
			return new ResponseEntity<String>(HttpStatus.OK);
		} else {
			return new ResponseEntity<String>(response.getMessage(),
					HttpStatus.EXPECTATION_FAILED);
		}
	}

	@Override
	public ClientWindowEffect getEffect() {
		return Action.ClientWindowEffect.CLOSE;
	}

	@Override
	public int getId() {
		return id;
	}
}
package de.unibonn.creedo.webapp.dashboard.patterncontainer;

/**
 * Wraps a core DiscoveryProcess and wraps the contained Patterns into
 * WebPatterns, which contain communication id and html representation.
 * <p/>
 * It provides mutation of the state based on these ids (only ids are valid that
 * come from WebPatterns currently retrievable by the accessor methods.)
 * 
 * @author bkang
 * 
 */
//public class WebDiscoveryProcess {
//
//
//
////	private final DiscoveryProcess discoveryProcess;
////
////	public WebDiscoveryProcess() {
////		this.discoveryProcess = new DiscoveryProcess();
////	}
////
////
////	public void endRound() {
////		this.discoveryProcess.endRound();
////	}
////
////
////	/**
////	 * 
////	 * @param patternId
////	 *            of WebPattern currently in WebDiscoveryProcessState
////	 */
////	public void saveToResults(long patternId) {
////		WebPattern webPattern = idToWebPattern.get(patternId);
////		webPattern.setHtmlGenerator(ResultPatternGenerator.INSTANCE);
////		this.discoveryProcess.addCandidateToResults(webPattern.getPattern());
////	}
////
////	public void discardFromResults(long patternId) {
////		WebPattern webPattern = idToWebPattern.get(patternId);
////		this.discoveryProcess.deletePatternFromResults(webPattern.getPattern());
////	}
////
////	public void discardFromCandidates(long patternId) {
////		WebPattern webPattern = idToWebPattern.get(patternId);
////		this.discoveryProcess.deletePatternFromCandidates(webPattern
////				.getPattern());
////	}
////
////	public boolean isInResults(WebPattern p) {
////		return this.discoveryProcess.getDiscoveryProcessState().isInResults(
////				p.getPattern());
////	}
////
////	public List<WebPattern> getCandidates() {
////		return compileWebPatternList(discoveryProcess
////				.getDiscoveryProcessState().getCandidatePatterns());
////	}
////
////	private List<WebPattern> compileWebPatternList(List<Pattern> patterns) {
////		ArrayList<WebPattern> result = new ArrayList<>(patterns.size());
////		for (Pattern pattern : patterns) {
////			result.add(patternToWebPattern.get(pattern));
////		}
////		return result;
////	}
////
////	public List<WebPattern> getResults() {
////		return compileWebPatternList(discoveryProcess
////				.getDiscoveryProcessState().getResultPatterns());
////	}
////
////	public List<WebPattern> getDiscarded() {
////		return compileWebPatternList(discoveryProcess
////				.getDiscoveryProcessState().getDiscardedPatterns());
////	}
////
////	public DiscoveryProcess getDiscoveryProcess() {
////		return discoveryProcess;
////	}
////
////	public WebPattern getWebPatternById(long patternId) {
////		return idToWebPattern.get(patternId);
////	}
////
////	public String getCandidatePatternsAsHTML(HttpSession httpSession) {
////		StringBuilder html = new StringBuilder();
////		for (WebPattern webPattern : getCandidates()) {
////			html.append(webPattern.getHtml(httpSession));
////		}
////
////		return html.toString();
////	}
//
//}

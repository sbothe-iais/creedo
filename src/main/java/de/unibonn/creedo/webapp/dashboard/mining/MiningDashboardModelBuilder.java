package de.unibonn.creedo.webapp.dashboard.mining;

import de.unibonn.creedo.webapp.dashboard.AnalyticsDashboardBuilder;

/**
 * @author mboley, bjacobs
 */
public interface MiningDashboardModelBuilder extends AnalyticsDashboardBuilder {

}

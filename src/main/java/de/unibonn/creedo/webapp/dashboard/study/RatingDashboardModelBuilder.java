package de.unibonn.creedo.webapp.dashboard.study;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.creedo.webapp.dashboard.DashboardModel;
import de.unibonn.creedo.webapp.dashboard.mining.AbstractDashboardModelBuilder;
import de.unibonn.creedo.webapp.dbaccess.DataTableDAO;
import de.unibonn.creedo.webapp.studies.Result;
import de.unibonn.creedo.webapp.studies.rating.RatingMetric;
import de.unibonn.realkd.data.table.DataFormatException;
import de.unibonn.realkd.data.table.DataTable;

/**
 * Represents the configuration of one RatingDashboard
 * 
 * @author bjacobs, mboley
 */
public class RatingDashboardModelBuilder extends AbstractDashboardModelBuilder {

	private List<RatingMetric> metrics = new ArrayList<>();
	private final int dataTableId;
	private List<Result> results;

	public RatingDashboardModelBuilder(int dataTableId, List<Result> results) {
		this.dataTableId = dataTableId;
		this.results = results;
	}

	@Override
	public DashboardModel build(int frameId, HttpSession session) {
		fixCloseHandler();

		DataTable dataTable = null;
		try {
			dataTable = new DataTableDAO().getDataTableById(dataTableId);
		} catch (DataFormatException e) {
			e.printStackTrace();
			System.exit(1);
		}

		return new RatingDashboardModel(frameId, results, metrics, session,
				dataTable, closeHandler);
	}

	public RatingDashboardModelBuilder setRatingMetrics(
			List<RatingMetric> metrics) {
		this.metrics = metrics;
		return this;
	}

	public void setResultsToEvaluate(List<Result> resultsToEvaluate) {
		this.results = resultsToEvaluate;
	}
}

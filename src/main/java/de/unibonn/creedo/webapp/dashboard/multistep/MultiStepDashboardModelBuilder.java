package de.unibonn.creedo.webapp.dashboard.multistep;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.webapp.dashboard.DashboardModel;
import de.unibonn.creedo.webapp.dashboard.AnalyticsDashboardBuilder;
import de.unibonn.creedo.webapp.dashboard.mining.AbstractDashboardModelBuilder;

/**
 * @author bjacobs
 */
public class MultiStepDashboardModelBuilder extends
		AbstractDashboardModelBuilder {

	private static final String DEFAULT_CLOSE_NAME = "Close";

	private static final String DEFAULT_NEXT_NAME = "Next";

	private final List<AnalyticsDashboardBuilder> builders;

	private List<String> nextAndCloseNames;

	public MultiStepDashboardModelBuilder(
			List<AnalyticsDashboardBuilder> builders) {
		this.builders = builders;
		this.nextAndCloseNames = new ArrayList<>();
		for (int i = 0; i < builders.size() - 1; i++) {
			this.nextAndCloseNames.add(DEFAULT_NEXT_NAME);
		}
		this.nextAndCloseNames.add(DEFAULT_CLOSE_NAME);
	}

	/**
	 * Sets the names used by the multistep dashboard for the next button on
	 * each step (respectively for the closed button on the last step).
	 */
	public void setNextAndCloseNames(List<String> names) {
		this.nextAndCloseNames = names;
	}

	@Override
	public DashboardModel build(int frameId, HttpSession session) {
		fixCloseHandler();
		return new MultiStepDashboardModel(frameId, Creedo
				.getCreedoSession(session).getUiRegister(), builders,
				nextAndCloseNames, session, closeHandler);
	}
}

package de.unibonn.creedo.webapp.dashboard.study;

import java.util.List;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.webapp.CreedoSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Handles all requests specifically operating on a rating dashboard.
 * 
 * @author bkang, mboley
 * 
 */
@Controller
@RequestMapping(value = "/ratingdashboard")
public class RatingDashboardController {

	@RequestMapping(value = "submit_rating.htm", method = RequestMethod.POST)
	@ResponseBody
	public String submitRating(
			HttpSession session,
			@RequestParam(value = "dashboardId", required = true) int dashboardId,
			@RequestParam(value = "patternSelect", required = true) String patternSelect,
			@RequestParam(value = "metricSelect", required = true) String metricSelect,
			@RequestParam(value = "optionSelect", required = true) int optionSelect)
			throws Exception {
		CreedoSession uiSession = Creedo.getCreedoSession(session);
		if (uiSession == null) {
			return "redirect:index.htm";
		}
		RatingDashboardModel ratingDashboard = (RatingDashboardModel) uiSession
				.getDashboard(dashboardId);
		ratingDashboard.updateRating(Integer.valueOf(patternSelect), metricSelect,
				optionSelect);

		return "0";
	}

	@RequestMapping(value = "retrieve_rating.htm", method = RequestMethod.GET)
	@ResponseBody
	public String retrieveRating(HttpSession session,
			@RequestParam(value = "dashboardId", required = true) int dashboardId,
			@RequestParam(value = "patternSelect", required = true) int patternSelect) throws Exception {
		CreedoSession uiSession = Creedo.getCreedoSession(session);
		if (uiSession == null) {
			return "redirect:index.htm";
		}
		RatingDashboardModel ratingDashboard = (RatingDashboardModel) uiSession
				.getDashboard(dashboardId);
		List<Integer> ratings = ratingDashboard.getPatternRating(patternSelect);
		String result = ratings.toString();
		return result.substring(1, result.length()-1);
	}

}

package de.unibonn.creedo.webapp.dashboard.mining;

import static de.unibonn.realkd.common.logger.LogChannel.DEFAULT;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.creedo.webapp.dashboard.DashboardModel;
import de.unibonn.creedo.webapp.utils.JsonTransferParameterContainer;
import de.unibonn.creedo.webapp.utils.ParameterTransfer;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.common.logger.LogMessageType;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/manualmining")
public class ManualMiningController {

	public ManualMiningController() {
		;
	}

	/**
	 * @return All algorithm categories in a JSON object
	 * @throws Exception
	 */
	@RequestMapping(value = "/getcategories.json", method = RequestMethod.POST)
	public @ResponseBody
	List<ManualMiningModel.CategoryResultContainer> getCategories(HttpSession httpSession,
			@RequestParam("dashboardId") int dashboardId) throws Exception {
		DEFAULT.log("ref_system", "Algorithm Categories requested",
				LogMessageType.INTER_COMPONENT_MESSAGE);

		MiningDashboardModel dashboard = getMiningDashboardModel(httpSession,
				dashboardId);
		return dashboard.getManualMiningModel().getCategories();
	}

	@RequestMapping(value = "/getcategoryalgorithms.json", method = RequestMethod.POST)
	public @ResponseBody
	List<ManualMiningModel.AlgorithmResultContainer> getCategoryAlgorithms(
			@RequestParam("algorithmCategory") AlgorithmCategory category,
			@RequestParam("dashboardId") int dashboardId,
			HttpSession httpSession) throws IllegalAccessException {
		DEFAULT.log("ref_system",
				"Algorithms of category " + category.toString() + " requested",
				LogMessageType.INTER_COMPONENT_MESSAGE);


		MiningDashboardModel dashboard = getMiningDashboardModel(httpSession,
				dashboardId);

		return dashboard.getManualMiningModel().getCategoryAlgorithms(category);
	}

	/**
	 * Gets called when an algorithm is newly selected from the RefControlBox
	 *
	 * @param algorithmName
	 *            String identifier of algorithm
	 * @return List of parameters for this algorithm with default values set
	 */
	@RequestMapping(value = "/getalgorithmparameters.json", method = RequestMethod.POST)
	public @ResponseBody
	List<JsonTransferParameterContainer> getAlgorithmParams(
			@RequestParam("algorithmName") String algorithmName,
			HttpSession httpSession,
			@RequestParam("dashboardId") int dashboardId) throws IllegalAccessException {

		MiningDashboardModel dashboard = getMiningDashboardModel(httpSession,
				dashboardId);

		dashboard.getManualMiningModel().setCurrentAlgorithmByName(algorithmName);

		return dashboard.getManualMiningModel().getCurrentAlgorithmParameters();
	}

	@RequestMapping(value = "/applyandgetalgorithmparameters.json", method = RequestMethod.POST)
	public
	@ResponseBody
	List<JsonTransferParameterContainer> applyAndGetAlgorithmParameters(
			HttpSession httpSession, HttpServletRequest request) throws IOException, IllegalAccessException {

		ParameterTransfer execParamSetup = new Gson().fromJson(
				request.getReader(), ParameterTransfer.class);

		MiningDashboardModel dashboard = getMiningDashboardModel(httpSession,
				execParamSetup.getDashboardId());

		dashboard.getManualMiningModel().applyAlgorithmSettings(execParamSetup);

		return dashboard.getManualMiningModel().getCurrentAlgorithmParameters();
	}

	@RequestMapping(value = "/stopcurrentmining.json", method = RequestMethod.POST)
	public void stopCurrentMiningExecution(HttpSession httpSession,
			@RequestParam("dashboardId") int dashboardId) {
		DEFAULT.log("ref_system",
				"Stop of execution of current algorithm requested.",
				LogMessageType.INTER_COMPONENT_MESSAGE);

		CreedoSession session = Creedo.getCreedoSession(httpSession);

		DashboardModel miningDashboardModel = session.getDashboard(dashboardId);
		if (!(miningDashboardModel instanceof MiningDashboardModel)) {
			throw new IllegalStateException(
					"Request cannot be sent from non-mining dashboard.");
		}

		((MiningDashboardModel) miningDashboardModel).requestStopOfAlgorithm();
	}

	private MiningDashboardModel getMiningDashboardModel(HttpSession httpSession, int dashboardId) throws IllegalAccessException {
		CreedoSession session = Creedo.getCreedoSession(httpSession);
		DashboardModel dashboard = session.getDashboard(dashboardId);
		if (dashboard instanceof MiningDashboardModel) {
			return (MiningDashboardModel) dashboard;
		}
		throw new IllegalAccessException(
				"Request can not be sent from non-MiningDashboardModel");
	}
}

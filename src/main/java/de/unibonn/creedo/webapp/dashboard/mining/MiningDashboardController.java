package de.unibonn.creedo.webapp.dashboard.mining;

import static de.unibonn.realkd.common.logger.LogChannel.UI;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.creedo.webapp.dashboard.DashboardModel;
import de.unibonn.realkd.common.logger.LogMessageType;

@Controller
public class MiningDashboardController {

	@RequestMapping(value = "/mine.htm", method = RequestMethod.POST)
	public ResponseEntity<String> mine(HttpSession session,
			@RequestHeader(value = "analysisSessionId") int analysisSessionId,
			HttpServletRequest request) {
		CreedoSession uiSession = Creedo.getCreedoSession(session);
		UI.log("mine button request received (session: " + analysisSessionId
				+ ")", LogMessageType.INTER_COMPONENT_MESSAGE);

		DashboardModel dashboardModel = uiSession.getDashboard(analysisSessionId);
		if (!(dashboardModel instanceof  MiningDashboardModel)) {
			return new ResponseEntity<>("redirect:index.htm", HttpStatus.BAD_REQUEST);
		}

		try {

			String htmlPatterns = ((MiningDashboardModel) dashboardModel)
					.mineClicked(request);
			return new ResponseEntity<>(htmlPatterns, HttpStatus.OK);

		} catch (RuntimeException e) {
			return new ResponseEntity<>(
					e.getMessage() + "\n" + Arrays.toString(e.getStackTrace()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/promotePattern.htm", method = RequestMethod.POST)
	public @ResponseBody
	String promotePattern(@RequestParam("promotedItem") int promotedItemID,
			@RequestParam("analysisSessionId") int analysisSessionId,
			HttpSession session) throws Exception {
		CreedoSession uiSession = Creedo.getCreedoSession(session);

		DashboardModel dashboardModel = uiSession.getDashboard(analysisSessionId);
		if (!(dashboardModel instanceof  MiningDashboardModel)) {
			return "redirect:index.htm";
		}

		((MiningDashboardModel) dashboardModel).saveToResults(promotedItemID);

		UI.log("Promoted pattern: ID: " + promotedItemID + " Description: "
				+ ((MiningDashboardModel) dashboardModel).getPattern(promotedItemID),
				LogMessageType.INTER_COMPONENT_MESSAGE);

		return ((MiningDashboardModel) dashboardModel).getPattern(promotedItemID).getHtml(
				session);
	}

	@RequestMapping(value = "/deletePattern.htm", method = RequestMethod.POST)
	public @ResponseBody
	String deletePattern(@RequestParam("deletedItem") int deletedItemID,
			@RequestParam("analysisSessionId") int analysisSessionId,
			HttpSession session) throws Exception {
		CreedoSession uiSession = Creedo.getCreedoSession(session);

		DashboardModel dashboardModel = uiSession.getDashboard(analysisSessionId);
		if (!(dashboardModel instanceof  MiningDashboardModel)) {
			return "redirect:index.htm";
		}

		WebPattern deletedWebPattern = ((MiningDashboardModel) dashboardModel)
				.getPattern(deletedItemID);
		boolean inResults = ((MiningDashboardModel) dashboardModel).isInResults(deletedWebPattern);

		if (inResults) {
			((MiningDashboardModel) dashboardModel).discardFromResults(deletedItemID);
		} else {
			((MiningDashboardModel) dashboardModel).discardFromCandidates(deletedItemID);
		}

		UI.log("Deleted pattern: ID: " + deletedItemID + " Description: "
						+ deletedWebPattern.toString() + " from "
						+ (inResults ? "results" : "candidates"),
				LogMessageType.INTER_COMPONENT_MESSAGE);

		return "1";
	}

	
}

package de.unibonn.creedo.webapp.dashboard.mining;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.webapp.dashboard.CloseHandler;
import de.unibonn.creedo.webapp.dashboard.DashboardModel;
import de.unibonn.creedo.webapp.dashboard.closehandlers.GeneralCloseHandler;
import de.unibonn.creedo.webapp.dbaccess.DataTableDAO;
import de.unibonn.creedo.webapp.viewmodels.DeveloperViewModel;
import de.unibonn.realkd.data.DataWorkspace;
import de.unibonn.realkd.data.DataWorkspaceFactory;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.DataFormatException;
import de.unibonn.realkd.data.table.DataTable;

/**
 * 
 * @author bjacobs
 * 
 */
public class MiningDashboardModelBuilderImplementation implements
		MiningDashboardModelBuilder {

	private final DataTableProvider dataTableProvider;
	private Integer dataTableId;
	private final MiningSystemBuilder system;
	private transient CloseHandler closeHandler;

	public MiningDashboardModelBuilderImplementation(
			DataTableProvider dataTableProvider, MiningSystemBuilder system) {
		this.dataTableProvider = dataTableProvider;
		this.system = system;
	}

	@Override
	public DashboardModel build(int frameId, HttpSession session) {
		fixCloseHandler();

		DataTable table;
		try {
			// Temporary workaround: The configuration of AnalysisTasks in the
			// database contain a dataTableId. Until there is a solution how
			// to store DataTableProvider in the configuration, there is this
			// following temporary fix:
			if (dataTableId == null) {
				table = dataTableProvider.get();
			} else {
				System.out.println("THIS MUST NEVER BE EXECUTED");
				table = new DataTableDAO().getDataTableById(dataTableId);
			}
		} catch (DataFormatException e) {
			throw new IllegalArgumentException("Error when loading DataTable: "
					+ e.getMessage());
		}

		DataWorkspace workspace = DataWorkspaceFactory.INSTANCE
				.createDataWorkspace();
		workspace.add(table);
		workspace.add(new PropositionalLogic(table));

		DeveloperViewModel developerViewModel = new DeveloperViewModel();

		MiningSystem miningSystem = system.build(developerViewModel, table,
				workspace);

		return new MiningDashboardModel(frameId, workspace, session, miningSystem,
				closeHandler, developerViewModel);

	}

	@Override
	public MiningDashboardModelBuilder setCloseHandler(CloseHandler closeHandler) {
		this.closeHandler = closeHandler;
		return this;
	}

	private void fixCloseHandler() {
		if (closeHandler == null) {
			closeHandler = GeneralCloseHandler.DEFAULT;
		}
	}
}

package de.unibonn.creedo.webapp.dashboard.study;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.unibonn.creedo.webapp.studies.rating.RatingMetric;
import de.unibonn.creedo.webapp.studies.rating.RatingOption;

/**
 * Holds an updates ratings according to a specified set of RatingMetrics for a
 * set of objects that can be referenced by an id.
 * 
 * @author mboley
 * 
 */
public class RatingSystem {

	private final Map<Integer, List<RatingOption>> ratings = new HashMap<>();
	private final List<RatingMetric> metrics;

	public RatingSystem(List<RatingMetric> metrics) {
		this.metrics = metrics;
	}
	
	public void updateRating(int patternId, String metricName, int optionId) {
		List<RatingOption> ratingOfOnePattern = ratings.get(patternId);
		int metricId = 0;
		for (RatingMetric metric : metrics) {
			if (metric.getName().equals(metricName)) {
				metricId = metrics.indexOf(metric);
				break;
			}
		}
		ratingOfOnePattern.set(metricId, metrics.get(metricId)
				.getRatingOptions()[optionId]);
	}
	/**
	 * Get the rating values per metric for a result pattern.
	 * A rating value is null, if this pattern is not rated using the corresponding metric.
	 *
	 * @return list of rating values.
	 */
	public List<Integer> getPatternRating(int patternId) {
		List<Integer> ratingOptionIds = new ArrayList<>();
		for (RatingMetric ratingMetric : metrics) {
			int ratingMetricId = metrics.indexOf(ratingMetric);
			if (ratings.get(patternId).get(ratingMetricId) == null) {
				ratingOptionIds.add(null);
			} else {
				for (int i = 0; i < ratingMetric.getRatingOptions().length; i++) {
					if (ratingMetric.getRatingOptions()[i].equals(ratings.get(
							patternId).get(ratingMetricId))) {
						ratingOptionIds.add(i);
						break;
					}
				}
			}

		}
		return ratingOptionIds;
	}

	public void init(int id) {
		List<RatingOption> ratingOptions = new ArrayList<>();
		for (int i = 0; i < metrics.size(); i++) {
			ratingOptions.add(metrics.get(i).getDefault());
		}
		ratings.put(id, ratingOptions);
	}

    public List<RatingMetric> getMetrics() {
        return metrics;
    }


}

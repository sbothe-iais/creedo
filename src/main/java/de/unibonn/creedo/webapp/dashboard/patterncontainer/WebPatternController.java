package de.unibonn.creedo.webapp.dashboard.patterncontainer;

import static de.unibonn.realkd.common.logger.LogChannel.UI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.webapp.patternviews.CandidateAssociationPatternMapper;
import de.unibonn.creedo.webapp.patternviews.CandidateExceptionalModelPatternMapper;
import de.unibonn.creedo.webapp.utils.VisualizationProvider;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.creedo.webapp.dashboard.DashboardModel;
import de.unibonn.realkd.common.logger.LogMessageType;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.outlier.Outlier;

@Controller
public class WebPatternController {

	@RequestMapping(value = "/getSupportSet.json", method = RequestMethod.GET)
	public @ResponseBody
	List<Integer> getSupportSet(
			@RequestParam(value = "patternId", required = true) int patternId,
			@RequestParam(value = "analysisSessionId") int analysisSessionId,
			HttpServletResponse response, HttpSession session)
			throws IOException {

		CreedoSession uiSession = Creedo.getCreedoSession(session);

		DashboardModel dashboardModel = uiSession
				.getDashboard(analysisSessionId);

		PatternContainer patternContainerDashboard = (PatternContainer) dashboardModel;

		WebPattern webPattern = patternContainerDashboard.getPattern(patternId);

		return new ArrayList<>(webPattern.getPattern().getSupportSet());
	}

	@RequestMapping(value = "/viewPattern.htm", method = { RequestMethod.GET })
	public ModelAndView viewPatternNew(
			HttpSession session,
			@RequestParam(value = "patternId", required = true) int patternId,
			@RequestParam(value = "dashboardId", required = true) int dashboardId) {
		CreedoSession uiSession = Creedo.getCreedoSession(session);

		DashboardModel miningDashboardModel = uiSession
				.getDashboard(dashboardId);
		if (!(miningDashboardModel instanceof PatternContainer)) {
			// throw new IllegalStateException(
			// "Request cannot be sent from non-mining dashboard.");
			return new ModelAndView("redirect:index.htm");
		}

		// WebPattern webPattern =
		// ((MiningDashboardModel)miningDashboardModel).getWebDiscoveryProcess().getWebPatternById(patternId);
		WebPattern webPattern = ((PatternContainer) miningDashboardModel)
				.getPattern(patternId);

		List<String> vImages = VisualizationProvider
				.createDetailedPatternImages(session, webPattern);

		ModelAndView mav = new ModelAndView();
		mav.addObject("vImages", vImages);
		mav.addObject("showNavBar", true);

		if (webPattern.getPattern() instanceof ExceptionalModelPattern) {
			ExceptionalModelPattern emmPattern = (ExceptionalModelPattern) webPattern
					.getPattern();
			String annotation = webPattern.getAnnotationText();
			return getEmmPatternVisualization(mav, emmPattern, annotation);
		} else if (webPattern.getPattern() instanceof Association) {
			Association assocPattern = (Association) webPattern.getPattern();
			String annotation = webPattern.getAnnotationText();
			return getAssociationPatternVisualization(mav, assocPattern,
					annotation);
		} else if (webPattern.getPattern() instanceof Outlier) {
			Outlier outlier = (Outlier) webPattern.getPattern();
			String annotation = webPattern.getAnnotationText();
			return getOutlierPatternVisualization(mav, outlier);
		} else {
			return getGenericPatternVisualization(mav, webPattern.getPattern());
		}
	}

	@RequestMapping(value = "/annotatePattern.htm", method = RequestMethod.POST)
	public @ResponseBody
	String annotatePattern(@RequestParam("annotatedPatternId") int patternID,
			@RequestParam("annotation") String annotation,
			@RequestParam("analysisSessionId") int analysisSessionId,
			HttpSession session) throws Exception {
		CreedoSession uiSession = Creedo.getCreedoSession(session);

		DashboardModel dashboardModel = uiSession
				.getDashboard(analysisSessionId);
		if (!(dashboardModel instanceof PatternContainer)) {
			return "redirect:index.htm";
		}

		// To prevent insertion of html tags
		annotation = annotation.replaceAll("<", " smaller ").replaceAll(">",
				" larger ");

		WebPattern pattern = ((PatternContainer) dashboardModel)
				.getPattern(patternID);
		pattern.setAnnotationText(annotation);

		UI.log("Received pattern annotation: ID: " + patternID
				+ " Annotation: " + annotation,
				LogMessageType.INTER_COMPONENT_MESSAGE);

		return "1";
	}

	private ModelAndView getEmmPatternVisualization(ModelAndView mav,
			ExceptionalModelPattern pattern, String annotation) {
		mav.addObject("targets", pattern.getTargetAttributes());
		mav.addObject("propositions", pattern.getDescription().getElements());
		mav.addObject("measures",
				CandidateExceptionalModelPatternMapper.getMeasures(pattern));
		mav.addObject("annotation", annotation);

		mav.setViewName("patterns/extendedEmmPatternView");
		return mav;
	}

	private ModelAndView getAssociationPatternVisualization(ModelAndView mav,
			Association pattern, String annotation) {
		mav.addObject("propositions", pattern.getDescription().getElements());
		mav.addObject("measures",
				CandidateAssociationPatternMapper.getMeasures(pattern));
		mav.addObject("annotation", annotation);

		mav.setViewName("patterns/extendedAssociationView");
		return mav;
	}

	private ModelAndView getOutlierPatternVisualization(ModelAndView mav,
			Outlier pattern) {
		List<String> elementNames = new ArrayList<String>();
		for (Integer rowIndex : pattern.getSupportSet()) {
			elementNames.add(pattern.getDatatable().getObjectName(
					rowIndex));
		}
		mav.addObject("elementNames", elementNames);
		mav.addObject("attributes", pattern.getAttributes());

		// mav.addObject("measures",
		// pattern.);

		mav.setViewName("patterns/outlierPatternView");
		return mav;
	}

	private ModelAndView getGenericPatternVisualization(ModelAndView mav,
			Pattern pattern) {
		List<String> elementNames = new ArrayList<String>();
		for (Integer rowIndex : pattern.getSupportSet()) {
			elementNames.add(pattern.getDataArtifact().getObjectName(rowIndex));
		}
		mav.addObject("elementNames", elementNames);
		mav.addObject("attributes", pattern.getAttributes());

		// mav.addObject("measures",
		// pattern.);

		mav.setViewName("patterns/extendedGenericPatternView");
		return mav;
	}
}

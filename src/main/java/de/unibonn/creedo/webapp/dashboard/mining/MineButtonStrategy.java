package de.unibonn.creedo.webapp.dashboard.mining;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import de.unibonn.creedo.webapp.utils.ParameterTransfer;
import de.unibonn.realkd.algorithms.MiningAlgorithm;
import de.unibonn.realkd.patterns.Pattern;

/**
 * @author bjacobs
 * 
 */
public enum MineButtonStrategy {

	ONECLICKMINING {

		@Override
		public String getClientJSFileName() {
			return "mineButtonOCM.js";
		}

		@Override
		public void clicked(MiningSystem miningSystem, HttpServletRequest request,
				List<MiningAlgorithm> algorithms) {

			MiningAlgorithm metaMiningAlgorithm = algorithms.get(0);

			miningSystem.requestStopOfAlgorithm();
			miningSystem.startNextDiscoveryRoundWithCurrentAlgorithmResult();
			miningSystem.startAlgorithm(metaMiningAlgorithm);

			//return miningDashboard.getCandidatePatternsAsHTML(session);
		}

		@Override
		public String getName() {
			return "One-Click Mining";
		}
	},

	MANUALMINING {
		@Override
		public String getClientJSFileName() {
			return "mineButtonREF.js";
		}

		@Override
		public String getName() {
			return "Manual Mining";
		}

		@Override
		public void clicked(MiningSystem miningSystem, HttpServletRequest request,
				List<MiningAlgorithm> algorithms)
				throws RuntimeException {

			try {
				ParameterTransfer execParamSetup = new Gson().fromJson(
						request.getReader(), ParameterTransfer.class);

				ManualMiningModel manualMiningModel = miningSystem.getManualMiningModel();
				manualMiningModel.applyAlgorithmSettings(execParamSetup);

				miningSystem.startAlgorithm(manualMiningModel.getSelectedAlgorithm());
				miningSystem.startNextDiscoveryRoundWithCurrentAlgorithmResult();

				// return miningDashboard.getCandidatePatternsAsHTML(session);

			} catch (IOException e) {
				e.printStackTrace();
			}

			// return "";
		}

//		private MiningAlgorithm getMiningAlgorithmByName(String algorithmName,
//				List<MiningAlgorithm> algorithms) {
//
//			for (MiningAlgorithm algorithm : algorithms) {
//				if (algorithm.getName().equals(algorithmName)) {
//					return algorithm;
//				}
//			}
//
//			return null;
//		}
	};

	/*public abstract String clicked(HttpSession session, MiningDashboardModel miningDashboard,
			HttpServletRequest request, List<MiningAlgorithm> algorithms) throws IllegalStateException;*/


	public abstract void clicked(MiningSystem miningSystem, HttpServletRequest request,
								 List<MiningAlgorithm> algorithms) throws IllegalStateException;

	public abstract String getClientJSFileName();

	public abstract String getName();

}

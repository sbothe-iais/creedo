package de.unibonn.creedo.webapp.dashboard;

import de.unibonn.creedo.webapp.dashboard.closehandlers.CloseHandlerResponse;

/**
 * Closehandlers represent operations to be carried out on closing attempts of Dashboards. 
 * 
 * @author bjacobs, bkang, mboley
 *
 */
public interface CloseHandler {
		
    public CloseHandlerResponse attemptToClose(DashboardModel dashboardModel);
    
}

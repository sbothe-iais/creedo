package de.unibonn.creedo.webapp.dashboard.closehandlers;

import de.unibonn.creedo.webapp.dashboard.CloseHandler;
import de.unibonn.creedo.webapp.dashboard.DashboardModel;

/**
 * General implementation of ClosedHandler that either applies a set of
 * CloseOperations to a dashboard if all of them are applicable or non of them.
 * 
 * @author bkang, bjacobs, mboley
 * 
 */
public class GeneralCloseHandler implements CloseHandler {

	/**
	 * Default close handler that does not perform any check.
	 */
	public static CloseHandler DEFAULT = new GeneralCloseHandler(
			DefaultCloseHandlerOperation.INSTANCE);

	private CloseOperation[] operations;

	public GeneralCloseHandler(CloseOperation... functions) {
		this.operations = functions;
	}

	@Override
	public CloseHandlerResponse attemptToClose(DashboardModel dashboardModel) {
		for (CloseOperation operation : operations) {
			if (!operation.isApplicable(dashboardModel)) {
				return new Fail(operation.getMessage());
			}
		}
		for (CloseOperation operation : operations) {
			operation.apply(dashboardModel);
		}
		return new Success("Everything Is OK");
	}
}

package de.unibonn.creedo.webapp.dashboard.mining;

import de.unibonn.creedo.webapp.dbaccess.DataTableDAO;
import de.unibonn.realkd.data.table.DataFormatException;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.DataTableFromCSVBuilder;

/**
 * @author bjacobs
 */
public interface DataTableProvider {
	public DataTable get() throws DataFormatException;

	public class DbBasedDataTableProvider implements DataTableProvider {
		private final int dataTableId;

		public DbBasedDataTableProvider(int dataTableId) {
			this.dataTableId = dataTableId;
		}

		@Override
		public DataTable get() throws DataFormatException {
			return new DataTableDAO().getDataTableById(dataTableId);
		}
	}

	public class FileBasedDataTableProvider implements DataTableProvider {

		private final String dataContent;
		private final String attributeContent;
		private final String attributeGroupContent;
		private final String dataDelimiter;
		private final String missingSymbol;

		public FileBasedDataTableProvider(String dataContent,
				String attributeContent, String attributeGroupContent,
				String dataDelimiter, String missingSymbol) {

			this.dataContent = dataContent;
			this.attributeContent = attributeContent;
			this.attributeGroupContent = attributeGroupContent;
			this.dataDelimiter = dataDelimiter;
			this.missingSymbol = missingSymbol;
		}

		@Override
		public DataTable get() throws DataFormatException {
			DataTableFromCSVBuilder builder = new DataTableFromCSVBuilder()
					.setDelimiter(dataDelimiter.charAt(0))
					.setMissingSymbol(missingSymbol)
					.setName("Uploaded database")
					.setAttributeGroupCSV(attributeGroupContent)
					.setAttributeMetadataCSV(attributeContent)
					.setDataCSV(dataContent);
			return builder.build();
//			return builder.build(attributeContent, attributeGroupContent,
//					dataContent);
		}
	}
}

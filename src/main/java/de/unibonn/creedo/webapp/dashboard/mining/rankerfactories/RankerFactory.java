package de.unibonn.creedo.webapp.dashboard.mining.rankerfactories;

import de.unibonn.creedo.webapp.dashboard.mining.rankers.Ranker;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.discoveryprocess.DiscoveryProcess;

public interface RankerFactory {
	public Ranker getRanker(DataTable dataTable,
							DiscoveryProcess discoveryProcess);
}

package de.unibonn.creedo.webapp.dashboard;

import de.unibonn.creedo.ui.core.Frame;
import de.unibonn.creedo.webapp.dashboard.closehandlers.CloseHandlerResponse;

/**
 * Interface implemented by all Dashboards that can be opened from the main
 * page.
 * 
 * @author bjacobs
 * 
 */
public interface DashboardModel extends Frame {

	public static final String DASHBOARD_ID_ATTRIBUTENAME = "analysisSessionId";

	/**
	 * frees up all resources used by this Dashboard, e.g., running mining
	 * algorithms
	 */
	public void tearDown();

	/**
	 * Retrieves reference to dashboard objects that are potentially nested
	 * within this dashboard (or this dashboard itself). That is, method
	 * performs a recursive search for dashboard object with matching id.
	 * 
	 * @param id
	 *            of dashboard to be retrieved
	 * @return reference to dashboard object of target dashboard if id refers to
	 *         this dashboard or one of its sub-dashboards (recursively) or null
	 *         otherwise
	 */
	public DashboardModel getDashboard(int id);
}

package de.unibonn.creedo.webapp.patternviews;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.unibonn.creedo.common.Pair;
import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.InterestingnessMeasure;
import de.unibonn.realkd.patterns.LogicallyDescribedLocalPattern;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.emm.AbstractModel;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.emm.GaussianModel;

public class CandidateExceptionalModelPatternMapper extends
		AbstractCandidatePatternGenerator implements PatternHTMLGenerator {

	public CandidateExceptionalModelPatternMapper(List<String> optionalActions) {
		super(optionalActions);
	}

	@Override
	protected List<String> getExplanationElements(Pattern pattern) {
		List<String> result = new ArrayList<>();
		result.add("has <b>unusual distribution of "
				+ getTargetsString(pattern)
				+ "</b> "
				+ "(dev. "
				+ String.format(Locale.ENGLISH, "%.3f",
						((ExceptionalModelPattern) pattern).getModelDeviation())
				+ ")");

		result.add("and <b>contains</b> "
				+ pattern.getSupportSet().size()
				+ " rows"
				+ String.format(Locale.ENGLISH, " (freq. %.3f)",
						((ExceptionalModelPattern) pattern).getFrequency()));
		return result;
	}

	private static String getTargetsString(Pattern pattern) {
		StringBuilder sb = new StringBuilder();
		for (Attribute<?> attribute : ((ExceptionalModelPattern) pattern)
				.getTargetAttributes()) {
			sb.append(attribute.getName() + ", ");
		}
		return sb.substring(0, sb.length() - 2);
	}

	@Override
	protected String getTooltip(Pattern pattern) {
		StringBuilder sb = new StringBuilder();
		for (int j = 0; j < ((LogicallyDescribedLocalPattern) pattern)
				.getDescription().getElementsAsStringList().size(); j++) {
			sb.append(((LogicallyDescribedLocalPattern) pattern)
					.getDescription().getElementsAsStringList().get(j));
			sb.append("<br/>");
		}
		sb.append("-------------------------<br/>");

		for (Pair<String, Double> measure : getMeasures((ExceptionalModelPattern) pattern)) {
			sb.append(measure.getLhs())
					.append(": ")
					.append(String.format(Locale.ENGLISH, "%.4f",
							measure.getRhs())).append("<br/>");
		}

		return sb.toString();
	}

	@Override
	protected String getTitle(WebPattern webPattern) {
		return "Subgroup of Rows with";
	}

	@Override
	protected String getHTMLClass() {
		return "subgroup";
	}

	public static List<Pair<String, Double>> getMeasures(
			ExceptionalModelPattern pattern) {
		List<Pair<String, Double>> results = new ArrayList<>();

		for (InterestingnessMeasure measure: pattern.getMeasures()) {
			results.add(new Pair<>(measure.getName(), pattern.getValue(measure)));
		}
		
//		results.add(new Pair<>("Frequency", pattern.getFrequency()));

//		results.add(new Pair<>("Deviation of " + getTargetsString(pattern),
//				pattern.getModelDeviation()));

		AbstractModel globalModel = pattern.getGlobalModel();
		AbstractModel localModel = pattern.getLocalModel();

		if (globalModel instanceof GaussianModel) {
			results.add(new Pair<>("pattern mean", ((GaussianModel) localModel)
					.getMean()));

			results.add(new Pair<>("global mean", ((GaussianModel) globalModel)
					.getMean()));

		}

		return results;
	}

	@Override
	protected List<String> getDescriptorElements(Pattern pattern) {
		return ((LogicallyDescribedLocalPattern) pattern).getDescription()
				.getElementsAsStringList();
	}

}

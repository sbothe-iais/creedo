package de.unibonn.creedo.webapp.patternviews;

import java.util.List;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.realkd.patterns.Pattern;

public abstract class AbstractCandidatePatternGenerator extends
		GeneralPattenGenerator implements PatternHTMLGenerator {

	public AbstractCandidatePatternGenerator(List<String> optionalActions) {
		super(optionalActions);
	}

	private String getTitleDiv(String title) {
        return "<div class=\"panel-heading\"><div class=\"title pull-left\">"
                + title + "</div>";
    }

	@Override
	protected void fillHtmlStringBuffer(StringBuilder result, HttpSession session, WebPattern webPattern) {
		result.append("<div class=\"panel panel-default pattern "
				+ getHTMLClass()
				+ "\" "
				+ "data-toggle=\"tooltip\" data-html=\"true\" data-placement=\"left\" id=\""
				+ webPattern.getId() + "\" title=\""
				+ getTooltip(webPattern.getPattern()) + "\">");
		// header
		result.append(getTitleDiv(getTitle(webPattern)));
		result.append(getActionsDiv(webPattern));

		// panel body
		result.append("<div class=\"panel-body\">");

		// description
		result.append(getDescription(webPattern.getPattern()));

		// explanation
		result.append(getExplanation(webPattern.getPattern()));

		// visualization
		result.append(getIllustration(session, webPattern.getPattern()));

		// close panel-body and panel div tags
		result.append("</div></div>");

	}

	private String getDescription(Pattern pattern) {
		List<String> descriptorList = getDescriptorElements(pattern);

		StringBuilder sb = new StringBuilder();
		sb.append("<div class='description'>");

		if (!descriptorList.isEmpty()) {
			sb.append(descriptorList.get(0));
			for (int dscrpCount = 1; dscrpCount < VISIBLE_DESCRIPTOR_NUMBER
					&& dscrpCount < descriptorList.size(); dscrpCount++) {
				sb.append("<br/>");
				sb.append(descriptorList.get(dscrpCount));
			}
		}
		if (descriptorList.size() > VISIBLE_DESCRIPTOR_NUMBER) {
			sb.append("<span id = \"hasMore\"><br/>... </span>");
		}

		sb.append("</div>");
		return sb.toString();
	}

	protected abstract String getHTMLClass();

	protected abstract String getTitle(WebPattern webPattern);

	protected abstract String getTooltip(Pattern pattern);

	protected String getExplanation(Pattern pattern) {
		StringBuilder sb = new StringBuilder();
		sb.append("<div class='explanation'>");
		for (String element : getExplanationElements(pattern)) {
			sb.append(element);
			sb.append("<br/>");
		}
		sb.append("</div>");
		return sb.toString();
	}

	protected abstract List<String> getExplanationElements(Pattern pattern);

	protected abstract List<String> getDescriptorElements(Pattern pattern);

}

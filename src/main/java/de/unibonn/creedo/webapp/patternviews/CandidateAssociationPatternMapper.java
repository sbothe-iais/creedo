package de.unibonn.creedo.webapp.patternviews;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.unibonn.creedo.common.Pair;
import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.realkd.patterns.Description;
import de.unibonn.realkd.patterns.InterestingnessMeasure;
import de.unibonn.realkd.patterns.LogicallyDescribedLocalPattern;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.association.Association;

public class CandidateAssociationPatternMapper extends
		AbstractCandidatePatternGenerator implements PatternHTMLGenerator {

	public CandidateAssociationPatternMapper(List<String> optionalActions) {
		super(optionalActions);
	}

	@Override
	protected String getTooltip(Pattern pattern) {
		StringBuilder sb = new StringBuilder();

		Association localPattern = (Association) pattern;

		for (String element : localPattern.getDescription().getElementsAsStringList()) {
			sb.append(element).append("<br/>");
		}

		sb.append("-------------------------<br/>");

		for (Pair<String, Double> measure : getMeasures(localPattern)) {
			sb.append(measure.getLhs())
					.append(": ")
					.append(String.format(Locale.ENGLISH, "%.4f",
							measure.getRhs())).append("<br/>");
		}

		return sb.toString();
	}

	/*
	 * currently this is duplication from ResultAssociationMapper
	 */
	@Override
	protected List<String> getDescriptorElements(Pattern pattern) {
		Description description = ((Association) pattern).getDescription();
		List<String> sb = new ArrayList<>(description.size());

		for (int i = 0; i < description.size(); i++) {
			sb.add(description.getElement(i)
					+ " ("
					+ String.format(Locale.ENGLISH, "%.4f",
							((Association) pattern).getDescription()
									.getElement(i).getSupportCount()
									/ (double) ((LogicallyDescribedLocalPattern) pattern).getPropositionalLogic()
											.getSize()) + ")");
		}

		return sb;
	}

	@Override
	protected List<String> getExplanationElements(Pattern pattern) {
		List<String> result = new ArrayList<>();

		result.add("<b>occur together in</b> "
				+ pattern.getSupportSet().size()
				+ " rows"
				+ String.format(Locale.ENGLISH, " (freq. %.3f)",
						((Association) pattern).getFrequency()));

		result.add(String.format(Locale.ENGLISH,
				"<b>compared to</b> %.1f rows, which are ",
				((Association) pattern).getExpectedFrequency()
						* ((LogicallyDescribedLocalPattern) pattern).getPropositionalLogic().getSize())
				+ String.format(Locale.ENGLISH,
						"expected assuming independence (lift %.3f)",
						((Association) pattern).getLift()));

		return result;
	}

	public static List<Pair<String, Double>> getMeasures(Association pattern) {
		List<Pair<String, Double>> results = new ArrayList<>();

		for (InterestingnessMeasure measure : pattern.getMeasures()) {
			results.add(new Pair<>(measure.getName(), pattern.getValue(measure)));
		}

		return results;
	}

	@Override
	protected String getTitle(WebPattern webPattern) {
		// double liftMeasurement = ((Association)
		// webPattern.getPattern()).getLift();
		boolean patternHasPositiveLift = webPattern.getPattern().hasMeasure(
				InterestingnessMeasure.LIFT);
		if (patternHasPositiveLift) {
			return "Positively Associated Attribute Values";
		} else {
			return "Negatively Associated Attribute Values";
		}
	}

	@Override
	protected String getHTMLClass() {
		return "association";
	}

}

package de.unibonn.creedo.webapp.patternviews;

import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.patternset.PatternSet;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author bkang
 */
public class ResultPatternSetMapper extends GeneralPattenGenerator {

    private static class PatternWithinPatternSetHtmGenerator implements PatternHTMLGenerator {
        private static PatternWithinPatternSetHtmGenerator INSTANCE = new PatternWithinPatternSetHtmGenerator();

        private static List<String> ACTIONS = new ArrayList<String>();

        private static ResultDefaultPatternGenerator DEFAULT_GENERATOR = new ResultDefaultPatternGenerator(
                ACTIONS, AbstractResultPatternMapper.AnnotationVisibility.READONLY);

        private static ResultExceptionalModelPatternMapper EMM_GENERATOR = new ResultExceptionalModelPatternMapper(
                ACTIONS, AbstractResultPatternMapper.AnnotationVisibility.READONLY);

        private static ResultAssociationPatternMapper ASSOCIATION_GENERATOR = new ResultAssociationPatternMapper(
                ACTIONS, AbstractResultPatternMapper.AnnotationVisibility.READONLY);

        private static ResultPatternSetMapper PATTERN_SET_GENERATOR = new ResultPatternSetMapper(ACTIONS);

        private PatternWithinPatternSetHtmGenerator() {
            ;
        }

        @Override
        public String getHTML(HttpSession session, WebPattern webPattern) {
            if (webPattern.getPattern() instanceof Association) {
                return PatternWithinPatternSetHtmGenerator.ASSOCIATION_GENERATOR
                        .getHTML(session, webPattern);
            } else if (webPattern.getPattern() instanceof ExceptionalModelPattern) {
                return PatternWithinPatternSetHtmGenerator.EMM_GENERATOR.getHTML(
                        session, webPattern);
            } else if (webPattern.getPattern() instanceof PatternSet) {
                return PatternWithinPatternSetHtmGenerator.PATTERN_SET_GENERATOR.getHTML(session, webPattern);
            } else {
                return PatternWithinPatternSetHtmGenerator.DEFAULT_GENERATOR.getHTML(
                        session, webPattern);
            }
        }
    }

    public ResultPatternSetMapper(List<String> optionalActions) {
        super(optionalActions);
    }

    @Override
    protected void fillHtmlStringBuffer(StringBuilder buffer, HttpSession session, WebPattern webPattern) {
        if (!(webPattern.getPattern() instanceof PatternSet)) {
            throw new IllegalArgumentException(
                    "can only be applied to PatternSet");
        }
        Set<Pattern> patternSet = ((PatternSet) webPattern.getPattern()).getPatterns();

        buffer.append("<div class=\"panel panel-default pattern "
                + "pattern-set" + "\" id=\"" + webPattern.getId() + "\">");
        buffer.append("<div class=\"panel-heading\"><div class=\"title pull-left\">"
                + "Pattern Set" + "</div>");
        buffer.append(getActionsDiv(webPattern));
        for (Pattern pattern : patternSet) {
            buffer.append(PatternWithinPatternSetHtmGenerator.INSTANCE.getHTML(session, new WebPattern(webPattern.getDashboard(), webPattern.getId(), pattern, PatternWithinPatternSetHtmGenerator.INSTANCE)));

        }

        buffer.append("</div>");
    }
}

package de.unibonn.creedo.webapp.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: bjacobs Date: 25.03.14 Time: 15:47
 */

public class ParameterTransfer {
	private int dashboardId;
	private String algorithmName;
	private List<JsonReceiveParameterContainer> parameters = new ArrayList<>();

	public ParameterTransfer(String algorithmName,
			List<JsonReceiveParameterContainer> parameters) {
		this.algorithmName = algorithmName;
		this.parameters = parameters;
	}

	public String getAlgorithmName() {
		return algorithmName;
	}

	public void setAlgorithmName(String algorithmName) {
		this.algorithmName = algorithmName;
	}

	public List<JsonReceiveParameterContainer> getParameters() {
		return parameters;
	}

	public void setParameters(List<JsonReceiveParameterContainer> parameters) {
		this.parameters = parameters;
	}

	public int getDashboardId() {
		return dashboardId;
	}

	public void setDashboardId(int dashboardId) {
		this.dashboardId = dashboardId;
	}

	public Map<String, String[]> getParametersAsKeyValueMap() {
		Map<String, String[]> result = new HashMap<>();
		for (JsonReceiveParameterContainer parameterContainer : parameters) {
			result.put(parameterContainer.getId(),
					parameterContainer.getValue());
		}
		return result;
	}
}

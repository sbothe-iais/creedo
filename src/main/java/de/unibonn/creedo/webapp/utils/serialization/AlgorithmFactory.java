package de.unibonn.creedo.webapp.utils.serialization;

import de.unibonn.realkd.algorithms.MiningAlgorithm;
import de.unibonn.realkd.algorithms.MiningAlgorithmFactory;
import de.unibonn.realkd.algorithms.association.AssociationMiningBeamSearch;
import de.unibonn.realkd.algorithms.association.AssociationSampler;
import de.unibonn.realkd.algorithms.association.DummyCrashMiner;
import de.unibonn.realkd.algorithms.emm.ExceptionalModelBeamSearch;
import de.unibonn.realkd.algorithms.emm.ExceptionalModelSampler;
import de.unibonn.realkd.algorithms.emm.dssd.DiverseSubgroupSetDiscovery;
import de.unibonn.realkd.algorithms.outlier.OneClassModelMiner;
import de.unibonn.realkd.data.DataWorkspace;

/**
 * Enum of all configured algorithms. This type is used to deserialize
 * MiningAlgorithmFactories that are stored in the DB. This means they are of
 * type AlgorithmFactory in the corresponding classes. This will probably be
 * replaced by a more robust system in the future.
 * 
 * @author bjacobs
 * 
 */
public enum AlgorithmFactory implements MiningAlgorithmFactory {

	// core
	EXCEPTIONAL_MODEL_BEAMSEARCH {
		@Override
		public MiningAlgorithm create(DataWorkspace dataTub) {
			return new ExceptionalModelBeamSearch(dataTub);
		}
	},

	// core
	ASSOCIATION_BEAMSEARCH {
		@Override
		public MiningAlgorithm create(DataWorkspace dataTub) {
			AssociationMiningBeamSearch algorithm = new AssociationMiningBeamSearch(
					dataTub);
			// algorithm.setComparator(Association.POSITIVE_LIFT_COMPARATOR);
			// algorithm.setComparator(Association.ABSOLUTE_LIFT_COMPARATOR);
			return algorithm;
		}
	},

/*
	FREQUENCY_ASSOCIATION_SAMPLER {
		@Override
		public MiningAlgorithm create(DataWorkspace dataTub) {
			AssociationSampler sampler = new AssociationSampler(dataTub);
			sampler.setDistributionFactory(new FrequencyDistributionFactory(1));
			List<ParameterSelector> parameterSelectors = new ArrayList<>();
			parameterSelectors
					.add(new ParameterSelector.FixAssociationOptimizationFunctionToAbsoluteLift(
							sampler.getOptimizationOrderParameter()));
			ParameterChoosingMetaAlgorithm algorithm = new ParameterChoosingMetaAlgorithm(
					dataTub, sampler, parameterSelectors);
			return algorithm;
		}

	},
*/

/*
	ABSOLUTE_LIFT_ASSOCIATION_BEAMSEARCH {
		@Override
		public MiningAlgorithm create(DataWorkspace dataTub) {
			AssociationMiningBeamSearch beamSearch = new AssociationMiningBeamSearch(
					dataTub);
			List<ParameterSelector> parameterSelectors = new ArrayList<>();
			parameterSelectors
					.add(new ParameterSelector.FixAssociationOptimizationFunctionToAbsoluteLift(
							beamSearch.getOptimizationFunctionParameter()));
			ParameterChoosingMetaAlgorithm algorithm = new ParameterChoosingMetaAlgorithm(
					dataTub, beamSearch, parameterSelectors);
			return algorithm;
		}
	},
*/

	// "core"
//	RARE_TIMES_FREQ_2_SAMPLER {
//		@Override
//		public MiningAlgorithm create(DataWorkspace dataTub) {
//			AssociationSampler algorithm = new AssociationSampler(dataTub);
//			algorithm
//					.setDistributionFactory(new RareDistributionTimesPowerOfFrequencyFactory(
//							2));
//			return algorithm;
//		}
//	},

	// core
	EMM_SAMPLER {
		@Override
		public MiningAlgorithm create(DataWorkspace dataTub) {
			return new ExceptionalModelSampler(dataTub);
		}
	},

	// core
	ASSOCIATION_SAMPLER {
		@Override
		public MiningAlgorithm create(DataWorkspace workspace) {
			return new AssociationSampler(workspace);
		}
		
	},

/*
	DISCRIMINATIVITY_TIMES_POS_FREQ_2_SAMPLER {
		@Override
		public MiningAlgorithm create(DataWorkspace workspace) {
			final ExceptionalModelSampler modelSampler = new ExceptionalModelSampler(
					workspace);
			// DiscriminativityDistributionFactory distributionFactory = new
			// DiscriminativityDistributionFactory(
			// modelSampler.getPropositionalLogicParameter()
			// .getCurrentValue(), modelSampler
			// .getTargetAttributesParameter().getCurrentValue(),
			// 0, 2, 1);
			// modelSampler.setDistributionFactory(distributionFactory);

			ParameterSelector.ExponentialSingleTargetProposer exponentialSingleTargetProposer = new ParameterSelector.ExponentialSingleTargetProposer(
					modelSampler.getTargetAttributesParameter());

			ParameterSelector.SuitableSingleTargetEMFactorySelector suitableSingleTargetEMFactorySelector = new ParameterSelector.SuitableSingleTargetEMFactorySelector(
					modelSampler.getModelClassParameter());

			ParameterSelector.FirstInRageSelector<ModelDistanceFunction> firstDistanceFunctionInRangeSelector = new ParameterSelector.FirstInRageSelector<>(
					modelSampler.getModelDistanceFunctionParameter());

			List<ParameterSelector> list = new ArrayList<>();
			list.add(exponentialSingleTargetProposer);
			list.add(new ParameterSelector() {
				@Override
				public void setParameter(PatternUtilityModel patternUtilityModel) {
					DiscriminativityDistributionFactory distributionFactory = new DiscriminativityDistributionFactory(
							modelSampler.getPropositionalLogicParameter()
									.getCurrentValue(), modelSampler
									.getTargetAttributesParameter()
									.getCurrentValue(), 0, 2, 1);
					modelSampler.setDistributionFactory(distributionFactory);
				}

				@Override
				public Parameter<?> getParameter() {
					return modelSampler.getDistributionFactoryParameter();
				}
			});
			list.add(suitableSingleTargetEMFactorySelector);
			list.add(firstDistanceFunctionInRangeSelector);

			ParameterChoosingMetaAlgorithm algorithm = new ParameterChoosingMetaAlgorithm(
					workspace, modelSampler, list);

			return algorithm;
		}
	},
*/

/*
	DEFAULT_SUBGROUP_BEAMSEARCH {
		@Override
		public MiningAlgorithm create(DataWorkspace dataTub) {

			ExceptionalModelBeamSearch beamSearch = new ExceptionalModelBeamSearch(
					dataTub);
			beamSearch.getOptimizationFunctionParameter().set(
					ExceptionalModelPattern.FREQUENCYDEVIATION_COMPARATOR);
			// .setComparator(ExceptionalModelPattern.FREQUENCYDEVIATION_COMPARATOR);

			ParameterSelector.ExponentialSingleTargetProposer exponentialSingleTargetProposer = new ParameterSelector.ExponentialSingleTargetProposer(
					beamSearch.getTargetAttributesParameter());

			ParameterSelector.SuitableSingleTargetEMFactorySelector suitableSingleTargetEMFactorySelector = new ParameterSelector.SuitableSingleTargetEMFactorySelector(
					beamSearch.getModelClassParameter());

			ParameterSelector.FirstInRageSelector<ModelDistanceFunction> firstDistanceFunctionInRangeSelector = new ParameterSelector.FirstInRageSelector<>(
					beamSearch.getModelDistanceFunctionParameter());

			List<ParameterSelector> list = new ArrayList<>();
			// the next two lines are switched in order to provoke an error in
			// the old implementation of ParameterChoosing Meta
			list.add(suitableSingleTargetEMFactorySelector);
			list.add(exponentialSingleTargetProposer);
			list.add(firstDistanceFunctionInRangeSelector);

			ParameterChoosingMetaAlgorithm algorithm = new ParameterChoosingMetaAlgorithm(
					dataTub, beamSearch, list);

			return algorithm;
		}
	},
*/

/*
	DEFAULT_EMM_BEAMSEARCH {
		@Override
		public MiningAlgorithm create(DataWorkspace dataTub) {
			ExceptionalModelBeamSearch beamSearch = new ExceptionalModelBeamSearch(
					dataTub);
			beamSearch.getOptimizationFunctionParameter().set(
					ExceptionalModelPattern.FREQUENCYDEVIATION_COMPARATOR);

			ParameterSelector.ExponentialTwoTargetsProposer proposer = new ParameterSelector.ExponentialTwoTargetsProposer(
					beamSearch.getTargetAttributesParameter());

			ParameterSelector.SuitableDoubleTargetEMFactorySelector emFactorySelector = new ParameterSelector.SuitableDoubleTargetEMFactorySelector(
					beamSearch.getModelClassParameter());

			ParameterSelector.FirstInRageSelector<ModelDistanceFunction> firstDistanceFunctionInRangeSelector = new ParameterSelector.FirstInRageSelector<>(
					beamSearch.getModelDistanceFunctionParameter());

			List<ParameterSelector> list = new ArrayList<>();
			list.add(proposer);
			list.add(emFactorySelector);
			list.add(firstDistanceFunctionInRangeSelector);

			ParameterChoosingMetaAlgorithm algorithm = new ParameterChoosingMetaAlgorithm(
					dataTub, beamSearch, list);

			return algorithm;
		}
	},
*/

/*
	DISCRIMINATIVITY_TIMES_POS_FREQ_2_SAMPLER_DOUBLE_TARGET {
		@Override
		public MiningAlgorithm create(DataWorkspace dataWorkspace) {
			final ExceptionalModelSampler modelSampler = new ExceptionalModelSampler(
					dataWorkspace);
			// WeightedDiscriminativityDistributionFactory factory = new
			// WeightedDiscriminativityDistributionFactory(modelSampler.getPropositionalLogicParameter()
			// .getCurrentValue(), modelSampler
			// .getTargetAttributesParameter().getCurrentValue(),
			// 0, 2, 1, new RowWeightComputer.UniformRowWeightComputer());
			// // factory.setRowWeightComputer(new
			// // RowWeightComputer.UniformRowWeightComputer());
			//
			// modelSampler.setDistributionFactory(factory);

			ParameterSelector.ExponentialTwoTargetsProposer proposer = new ParameterSelector.ExponentialTwoTargetsProposer(
					modelSampler.getTargetAttributesParameter());

			ParameterSelector.SuitableDoubleTargetEMFactorySelector selector = new ParameterSelector.SuitableDoubleTargetEMFactorySelector(
					modelSampler.getModelClassParameter());

			ParameterSelector.FirstInRageSelector<ModelDistanceFunction> firstDistanceFunctionInRangeSelector = new ParameterSelector.FirstInRageSelector<>(
					modelSampler.getModelDistanceFunctionParameter());

			List<ParameterSelector> list = new ArrayList<>();
			list.add(proposer);
			list.add(new ParameterSelector() {

				@Override
				public void setParameter(PatternUtilityModel patternUtilityModel) {
					WeightedDiscriminativityDistributionFactory factory = new WeightedDiscriminativityDistributionFactory(
							modelSampler.getPropositionalLogicParameter()
									.getCurrentValue(), modelSampler
									.getTargetAttributesParameter()
									.getCurrentValue(), 0, 2, 1,
							new RowWeightComputer.UniformRowWeightComputer());

					modelSampler.setDistributionFactory(factory);
				}

				@Override
				public Parameter<?> getParameter() {
					return modelSampler.getDistributionFactoryParameter();
				}
			});
			list.add(selector);
			list.add(firstDistanceFunctionInRangeSelector);

			ParameterChoosingMetaAlgorithm algorithm = new ParameterChoosingMetaAlgorithm(
					dataWorkspace, modelSampler, list);

			return algorithm;
		}
	},
*/

	// core
	ONE_CLASS_OUTLIER_DETECTION {

		@Override
		public MiningAlgorithm create(DataWorkspace dataTub) {
			return new OneClassModelMiner(dataTub);
		}

	},

/*
	ONE_CLICK_ONE_CLASS_OUTLIER_DETECTION {
		@Override
		public MiningAlgorithm create(DataWorkspace dataTub) {
			OneClassModelMiner entailedAlgorithm = new OneClassModelMiner(
					dataTub);

			List<ParameterSelector> parameterSelectors = new ArrayList<>();
			parameterSelectors
					.add(new ParameterSelector.ExponentialTwoNumericTargetsProposer(
							entailedAlgorithm.getTargetAttributesParameter()));
			parameterSelectors.add(new ParameterSelector.RandomPercentChooser(
					entailedAlgorithm.getFractionOfOutlierParameter()));
			return new ParameterChoosingMetaAlgorithm(dataTub,
					entailedAlgorithm, parameterSelectors);
		}
	},
*/

	DIVERSE_SUBGROUP_SET_DISCOVERY {
		@Override
		public MiningAlgorithm create(DataWorkspace workspace) {
			return new DiverseSubgroupSetDiscovery(workspace);
		}

	},

	DUMMY_CRASH_MINER {
		@Override
		public MiningAlgorithm create(DataWorkspace dataTub) {
			return new DummyCrashMiner(dataTub);
		}
	},

/*
	DUMMY_CRASH_PARAMCHOOSING_MINER {
		@Override
		public MiningAlgorithm create(DataWorkspace dataTub) {
			List<ParameterSelector> list = new ArrayList<>();
			return new ParameterChoosingMetaAlgorithm(dataTub,
					new DummyCrashMiner(dataTub), list);
		}
	};
*/
	;

	@Override
	public abstract MiningAlgorithm create(DataWorkspace workspace);

}

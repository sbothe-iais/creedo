package de.unibonn.creedo.webapp.utils;

/**
 * User: bjacobs
 * Date: 10.06.14
 * Time: 13:47
 */

public class JsonReceiveParameterContainer {
    private String[] value;
    private String id;

    public JsonReceiveParameterContainer() {
    }

    public String[] getValue() {
        return value;
    }

    public void setValue(String[] value) {
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

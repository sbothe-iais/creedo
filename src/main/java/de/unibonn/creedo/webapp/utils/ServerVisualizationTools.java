package de.unibonn.creedo.webapp.utils;

import java.io.IOException;

import javax.servlet.http.HttpSession;

import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.servlet.ServletUtilities;

public class ServerVisualizationTools {

	private static final String NO_PIC_URL = "client/images/nopic.png";
	private static final String SERVLET_CHART_URL_PREFIX = "/servlet/DisplayChart?filename=";

	/**
	 * Serves a JFreeChart as png under a specific url, which is returned by the
	 * method.
	 * 
	 * @return the url where the picture is available for web clients
	 */
	public static String serveChartAsPNG(HttpSession session, JFreeChart chart,
			int width, int height) {
		String result;
		try {
			result = ServletUtilities.saveChartAsPNG(chart, width, height,
					new ChartRenderingInfo(), session);
		} catch (IOException e) {
			e.printStackTrace();
			return NO_PIC_URL;
		}
		catch (NumberFormatException e) {
			e.printStackTrace();
			return NO_PIC_URL;
		}
		result = session.getServletContext().getContextPath()
				+ SERVLET_CHART_URL_PREFIX + result;
		return result;
	}

}

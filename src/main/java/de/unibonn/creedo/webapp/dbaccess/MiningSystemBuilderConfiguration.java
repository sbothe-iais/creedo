package de.unibonn.creedo.webapp.dbaccess;

import java.util.List;

/**
 * Simple Java Bean bundling all entities related to
 * a MiningSystemBuilder. Used for persisting to the
 * database by "Create Dashboard" functionality.
 *
 * @author bjacobs
 */

public class MiningSystemBuilderConfiguration {
    private List<String> algorithms;
    private String builderClass;

    public void setAlgorithms(List<String> algorithms) {
        this.algorithms = algorithms;
    }

    public void setBuilderClass(String builderClass) {
        this.builderClass = builderClass;
    }

    public List<String> getAlgorithms() {
        return algorithms;
    }

    public String getBuilderClass() {
        return builderClass;
    }
}

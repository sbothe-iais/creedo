package de.unibonn.creedo.webapp.dbaccess;

public class IdEntryPair {
	
	public IdEntryPair(int id, String entry) {
		this.id = id;
		this.entry = entry;
	}
	
	private int id;
	private String entry;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEntry() {
		return entry;
	}
	public void setEntry(String entry) {
		this.entry = entry;
	}
}

package de.unibonn.creedo.webapp.dbaccess;

import de.unibonn.creedo.common.ConnectionFactory;
import de.unibonn.realkd.common.logger.LogMessageType;
import de.unibonn.realkd.data.table.DataFormatException;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.DataTableFromCSVBuilder;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

import static de.unibonn.realkd.common.logger.LogChannel.DATA_INITIALIZATION;

public class DataTableDAO {

	public static final DataTableDAO INSTANCE = new DataTableDAO();

	private SqlSessionFactory sqlSessionFactory;

	// constructor will receive a myBatis sessionFactory object
	public DataTableDAO() {
		//ConnectionFactory factory = (ConnectionFactory) ContextProvider.getApplicationContext().getBean("connectionFactory");

		sqlSessionFactory = ConnectionFactory.get().getSession();
	}

	public List<Integer> getDatatableIds() {
		SqlSession session = sqlSessionFactory.openSession();
		DataTableMapper mapper = session.getMapper(DataTableMapper.class);

		List<Integer> result = mapper.getDatatableIds();

		session.close();
		return result;
	}

	public String getNameById(int datatableId) {
		SqlSession session = sqlSessionFactory.openSession();
		DataTableMapper mapper = session.getMapper(DataTableMapper.class);

		String result = mapper.getNameById(datatableId);

		session.close();
		return result;
	}

	public DataTable getDataTableById(int datatableId)
			throws DataFormatException {
		DATA_INITIALIZATION.log("Creating datatable from database",
				LogMessageType.INTER_COMPONENT_MESSAGE);

		DATA_INITIALIZATION.log("Loading raw data from database",
				LogMessageType.INTRA_COMPONENT_MESSAGE);

		SqlSession session = sqlSessionFactory.openSession();
		DataTableMapper mapper = session.getMapper(DataTableMapper.class);

		DataTableMapper.DataTableDataContainer container = mapper
				.getDataTableData(datatableId);

		DataTableFromCSVBuilder builder = new DataTableFromCSVBuilder()
				.setName(container.getName())
				.setDescription(container.getDescription())
				.setDelimiter(container.getDelimiter())
				.setMissingSymbol(container.getMissingSymbol())
				.setAttributeGroupCSV(container.getAttributeGroups())
				.setAttributeMetadataCSV(container.getAttributes())
				.setDataCSV(container.getData());

		try {
//			DataTable table = builder.build(container.getAttributes(),
//					container.getAttributeGroups(), container.getData());
			DataTable table = builder.build();
			return table;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public void storeDatabase(String databaseName, String databaseDescription,
			String dataDelimiter, String missingSymbol, String dataFileContent,
			String attributeFileContent, String attributeGroupsFileContent) {

		SqlSession session = sqlSessionFactory.openSession();
		DataTableMapper mapper = session.getMapper(DataTableMapper.class);

		mapper.storeDataTable(databaseName, databaseDescription, dataDelimiter, missingSymbol,
				dataFileContent, attributeFileContent, attributeGroupsFileContent);

		session.commit();
		session.close();
	}
}

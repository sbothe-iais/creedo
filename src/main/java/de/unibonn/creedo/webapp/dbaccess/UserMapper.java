package de.unibonn.creedo.webapp.dbaccess;

import java.util.List;

import de.unibonn.creedo.webapp.User;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {
	// select
	User selectUserByUsername(@Param("username") String username);
    	List<User> selectUsersByUsername(@Param("username") String username);
    	List<User> selectAllUsers();
	List<User> selectAdministrators();
	
	List<Integer> selectAllUserIds();
	
	// update
	void updateActiveStatus(@Param("username") String username, @Param("isActive") int isActive);
	
	// insert
	void insertUser(User user);

    	void updateUser(User user);

	User selectUserById(@Param("userid") int userId);
}

package de.unibonn.creedo.webapp.dbaccess;

import de.unibonn.creedo.common.ConnectionFactory;
import de.unibonn.creedo.webapp.User;
import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;



@Repository
public class UserDAO {

	private SqlSessionFactory sqlSessionFactory;

	// constructor will receive a myBatis sessionFactory object
	public UserDAO() {
		//ConnectionFactory factory = (ConnectionFactory) ContextProvider.getApplicationContext().getBean("connectionFactory");
		sqlSessionFactory = ConnectionFactory.get().getSession();
	}
	
	public User getUserByUsername(String username) throws PersistenceException {
		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			return mapper.selectUserByUsername(username);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

    public boolean isUserExists(String username) throws PersistenceException {
		SqlSession session = sqlSessionFactory.openSession();

		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			return mapper.selectUsersByUsername(username).size() > 0;
		} finally {
			session.close();
		}
	}
	
	public List<User> getAdministrators() throws PersistenceException {
		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			return mapper.selectAdministrators();
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			return null;
		} finally {
			session.close();
		}

	}
	
	public List<User> getAllUsers() throws PersistenceException {
		SqlSession session = sqlSessionFactory.openSession();

		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			return mapper.selectAllUsers();
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			return null;
		} finally {
			session.close();
		}

	}
	
	public List<Integer> getAllUserIds() throws PersistenceException {
		SqlSession session = sqlSessionFactory.openSession();

		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			return mapper.selectAllUserIds();
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			return null;
		} finally {
			session.close();
		}

	}

	public void updateActiveStatus(String username, int isActive) throws PersistenceException {
		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			mapper.updateActiveStatus(username, isActive);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
	}
	
	public void saveUser(User user) throws PersistenceException {
		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			mapper.insertUser(user);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
	}

	public User getUserById(int userId) {
		SqlSession session = sqlSessionFactory.openSession();

		User user = null;

		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			user = mapper.selectUserById(userId);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}

		return user;
	}

	public List<Integer> getDatatableIdsForUser(User user) {
		SqlSession session = sqlSessionFactory.openSession();
		List<Integer> ids = null;

		try {
			UserDatatableMapper mapper = session.getMapper(UserDatatableMapper.class);
			ids = mapper.getDatatableIdsForUser(user.getId());
		} catch (PersistenceException pe) {
			pe.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}

		return ids;
	}

    public void updateUser(User user) {
	SqlSession session = sqlSessionFactory.openSession();

	try {
	    UserMapper mapper = session.getMapper(UserMapper.class);
	    mapper.updateUser(user);
	} catch (PersistenceException pe) {
	    pe.printStackTrace();
	} finally {
	    session.commit();
	    session.close();
	}
    }
}

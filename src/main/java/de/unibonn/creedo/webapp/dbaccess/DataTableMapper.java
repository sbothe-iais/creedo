package de.unibonn.creedo.webapp.dbaccess;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface DataTableMapper {
	
	List<Integer> getDatatableIds();
	
	String getNameById(@Param("id") int id);

	String getAttributeCSVStringById(@Param("id") int id);
	
	String getDataCSVStringById(@Param("id") int id);
	
	String getAttributeGroupsCSVStringById(@Param("id") int id);

	Integer storeDataTable(
			@Param("databaseName") String databaseName,
			@Param("databaseDescription") String databaseDescription,
			@Param("delimiter") String delimiter,
			@Param("missingSymbol") String missingSymbol,
			@Param("dataFileContent") String dataFileContent,
			@Param("attributeFileContent") String attributeFileContent,
			@Param("attributeGroupsFileContent") String attributeGroupsFileContent);

	DataTableDataContainer getDataTableData(@Param("id") int id);

	class DataTableDataContainer {
		private int id;
		private String name;
		private String description;
		private String delimiter;
		private String missingSymbol;
		private String attributes;
		private String data;
		private String attributeGroups;
		private DataTableDataContainer() {
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public char getDelimiter() {
			return delimiter.charAt(0);
		}

		public void setDelimiter(String delimiter) {
			this.delimiter = delimiter;
		}

		public String getMissingSymbol() {
			return missingSymbol;
		}

		public void setMissingSymbol(String missingSymbol) {
			this.missingSymbol = missingSymbol;
		}

		public String getAttributes() {
			return attributes;
		}

		public void setAttributes(String attributes) {
			this.attributes = attributes;
		}

		public String getData() {
			return data;
		}

		public void setData(String data) {
			this.data = data;
		}

		public String getAttributeGroups() {
			return attributeGroups;
		}

		public void setAttributeGroups(String attributeGroups) {
			this.attributeGroups = attributeGroups;
		}
	}

}


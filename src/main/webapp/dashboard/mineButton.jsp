<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${not empty jsFileName}">
    <script src="client/js/${jsFileName}?v=13"></script>
</c:if>

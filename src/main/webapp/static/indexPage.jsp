<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="col-xs-8 col-xs-offset-2">

	${mainContent}

	<%-- <c:choose> --%>
	<%--     <c:when test="${not empty demos}"> --%>
	<c:if test="${not empty demos}">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 text-center">
				<h3>Interactive demos</h3>
			</div>
		</div>
		<div class="container-fluid">
			<c:forEach items="${actions}" var="action" varStatus="status">
				<c:if test="${status.count % 3 == 1}">
					<div class="row">
				</c:if>
				<div class="col-lg-4 col-md-12 col-sm12">
					<div class="bs-component">
						<div class="well actionLink" frameId="${frameId}"
							linkId="${action.id}" resultTarget="${action.effect}">
							<a href="#"> <img class="img-responsive img-home-portfolio"
								src="file.htm?filename=${action.dashboardLink.imgPath}">
							</a>
							<p>
								<b>${action.dashboardLink.title}</b>
								${action.dashboardLink.description}
							</p>
							<div style="color: #808080; font-size: 6pt">${action.dashboardLink.imgCredits}</div>
						</div>
					</div>
				</div>
				<c:if test="${status.count % 3 == 0}">
		</div>
	</c:if>
	</c:forEach>
</div>
</c:if>
<%--     </c:when> --%>
<%-- <c:otherwise> --%>
<c:if test="${not empty loginInvitationHtml}">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 text-center">
			<h3>${loginInvitationHtml}</h3>
		</div>
	</div>
</c:if>
<%-- </c:otherwise> --%>
<%-- </c:choose> --%>

</div>
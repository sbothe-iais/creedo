var failCounter = 0;
var failLimit = 30;
var requestTimeout = 10000;
var pingInterval = 3000;

function pingToServer() {
    setTimeout(function () {
            $.ajax({
                url: "ping.htm",
                data: { analysisSessionId: window.analysisSessionId },
                error: function (jqxhr, textStatus, errorThrown) {
                    if (errorThrown == 'Unauthorized') {
                        window.location = "index.htm";
                    }
                    failCounter++;
                },
                success: function (data, textStatus, request) {
                    if (request.getResponseHeader('CLOSE_REQUESTED') == '1') {
                        window.close();
                    }
                    failCounter = 0;
                },
                timeout: requestTimeout
            });

            if (failCounter > failLimit) {
                alert('Connection to server seems to be lost!');
            } else {
                pingToServer();
            }
        }
        , pingInterval);
}

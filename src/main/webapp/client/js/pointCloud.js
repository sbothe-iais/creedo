window.drawPointCloud = function() {
	var $container = $('#content-point-cloud');
	if ($container.length == 0) {
		return;
	}
	// $container.height($('#content-table').height());
	$container.height(calcTableHeight());

	// Only request point-cloud-data if not already present
	if (window.pointCloudData == null) {
		// $.getJSON("getPointCloud.json", {analysisSessionId:
		// window.analysisSessionId}, function (data) {
		$.getJSON("getPointCloud.json", {
			dataViewContainerId : window.dataViewContainerId
		}, function(data) {
			window.pointCloudData = data;
			processPointCloudData(data);
		});
	} else {
		processPointCloudData(window.pointCloudData);
	}

	function processPointCloudData(data) {
		var globalSupportSet = getGlobalSupportSet();
		var seriesData = splitSeriesBySupportSet(data, globalSupportSet);

		$container.highcharts({
			chart : {
				type : 'scatter',
				zoomType : 'xy'
			},
			title : {
				text : ''
			},
			xAxis : {
				title : {
					enabled : false
				}
			},
			yAxis : {
				title : {
					enabled : false
				}
			},
			legend : {
				enabled : true
			},

			tooltip : {
				formatter : function() {
					return this.point.objName;
				}
			},
			series : [ {
				turboThreshold : 0,
				name : 'Data',
				color : 'rgba(83, 83, 233, .5)',
				data : seriesData.series1
			}, {
				turboThreshold : 0,
				name : 'Selected',
				color : 'rgba(233, 83, 83, .5)',
				data : seriesData.series2
			} ]
		});
	}

	function splitSeriesBySupportSet(data, globalSupportSet) {
		var series1 = [];
		var series2 = [];

		$.each(data, function(index, item) {
			if ($.inArray(item.id, globalSupportSet) == -1) {
				series1.push(item);
			} else {
				series2.push(item);
			}
		});
		return {
			series1 : series1,
			series2 : series2
		}
	}
};
/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Created by bjacobs on 28.01.15.
 */

/**
 * Iterates over the given parameters and creates a tree of html elements that
 * represent the given parameters visually. This tree of elements is appended to
 * the provided target element.
 * 
 * @param targetElement
 *            Tag to append visual representation to
 * @param parameters
 *            List of parameters to render
 * @return boolean is true if parameter information provided by the server
 *         indicates that all parameters are valid.
 */
function renderParameters(targetElement, parameters) {
    var allOk = true;

    // Iterate over received parameter configurations
    $.each(parameters, function (index, current) {

        var leftColWidth = 4 + current.depth;
        var rightColWidth = 8 - current.depth;

        // Build columns
        var paramColLeft = $('<div/>', {class: 'col-xs-' + leftColWidth + ' text-right'});
        var paramColRight = $('<div/>', {class: 'col-xs-' + rightColWidth});

        // Create a new row
        var row = $('<div/>', {class: 'row paramMargin'});
        row.append(paramColLeft);
        row.append(paramColRight);

        // Create label and attach it to left column
        var newLabel = $('<label/>', {for: current.name});
        newLabel.html(current.name + ":&nbsp;");

        $(newLabel).attr({
            'data-toggle': 'tooltip',
            'data-placement': 'top',
            title: " " + current.description
        });

        paramColLeft.append(newLabel);

        // Create input div on the right column
        var newDiv = $('<div/>', {class: 'input-group col-xs-12'});

        // Generate solution hint (only use it when parameter active but not
		// valid)
        var invalidValueSign = $('<span/>', {class: 'glyphicon glyphicon-exclamation-sign red-tooltip red-icon'});
        invalidValueSign.attr({
            'data-toggle': 'tooltip',
            'data-placement': 'bottom',
            title: " " + current.solutionHint
        });

        // Generate not-verifiable-sign sign (only use it when parameter not
		// active because of depending parameters)
        var notVerifiableSign = $('<span/>', {class: 'glyphicon glyphicon-exclamation-sign red-tooltip red-icon'});
        notVerifiableSign.attr({
            'data-toggle': 'tooltip',
            'data-placement': 'bottom',
            title: " " + current.dependsOnNotice
        });

        // Now differentiate between extensional and intentional
        // if (current.multiplicity != null) {
        if (current.type == 'Subset') {
            var newSelect = $('<select/>', {class: 'form-control subset-parameter', id: current.name});

            // Extract selected value(s)
            var selectedValues = Object.create(null);
            
            newSelect.prop('multiple', "multiple");
            newSelect.addClass('multiple-select');
            if (current.values != null) {
            	var tmpSelections = current.values// arrayStringToSet(current.values);
                $.each(tmpSelections, function (index, current) {
                	selectedValues[current] = true;
                })
            }
            
            newDiv.append(newSelect);
            paramColRight.append(newDiv);
            
            renderOptionsInSelectHTMLElement(current, newSelect, newDiv, paramColLeft, notVerifiableSign, allOk, selectedValues, invalidValueSign);
            
        } else if (current.type == 'RangeEnumerable') {
            var newSelect = $('<select/>', {class: 'form-control range-enumerable-parameter', id: current.name});

            // Extract selected value(s)
            var selectedValues = Object.create(null);
            selectedValues[current.values[0]] = true;

            newDiv.append(newSelect);
            paramColRight.append(newDiv);
            
            renderOptionsInSelectHTMLElement(current, newSelect, newDiv, paramColLeft, notVerifiableSign, allOk, selectedValues, invalidValueSign);
            
        } else if (current.type == 'Text') {
            var newInput = $('<input/>', {
                type: 'text',
                class: 'form-control text-parameter',
                id: current.name,
                val: current.values[0]
            });
            newInput.html(current.name);
            newDiv.append(newInput);
            paramColRight.append(newDiv);

            if (!current.active) {
                newInput.prop('disabled', true);
                newDiv.addClass('has-error');
                $(paramColLeft).append(notVerifiableSign);
                allOk = false;
            } else {
                if (!current.valid) {
                    newDiv.addClass('has-error');
                    $(paramColLeft).append(invalidValueSign);
                    allOk = false;
                }
            }
        } else {;}

        targetElement.append(row);
    });

    // Initialize tooltips
    targetElement.find("[data-toggle=tooltip]").tooltip();
    targetElement.find("[data-toggle=popover]").popover();

    return allOk;
}

function renderOptionsInSelectHTMLElement(current, newSelect, newDiv, paramColLeft, notVerifiableSign, allOk, selectedValues, invalidValueSign) {
	if (!current.active) {
        newSelect.prop('disabled', true);
        newDiv.addClass('has-error');
        $(paramColLeft).append(notVerifiableSign);
        allOk = false;
    } else {
        $.each(current.range, function (index, current) {
            var newOption = $('<option/>', {text: current, value: current});
            if (current in selectedValues) {
                newOption.prop('selected', true);
            }
            newSelect.append(newOption);
        });
        if (!current.valid) {
            newDiv.addClass('has-error');
            $(paramColLeft).append(invalidValueSign);
            allOk = false;
        }
    }
}

// function renderParametersWithActionId(targetElement, parameters, componentId)
// {
// var allOk = true;
//
// // Iterate over received parameter configurations
// $.each(parameters, function (index, current) {
//
// var leftColWidth = 4 + current.depth;
// var rightColWidth = 8 - current.depth;
//
// // Build columns
// var paramColLeft = $('<div/>', {class: 'col-xs-' + leftColWidth + '
// text-right'});
// var paramColRight = $('<div/>', {class: 'col-xs-' + rightColWidth});
//
// // Create a new row
// var row = $('<div/>', {class: 'row paramMargin'});
// row.append(paramColLeft);
// row.append(paramColRight);
//
// // Create label and attach it to left column
// var newLabel = $('<label/>', {for: current.name});
// newLabel.html(current.name + ":&nbsp;");
//        
// $(newLabel).attr({
// 'data-toggle': 'tooltip',
// 'data-placement': 'top',
// title: " " + current.description
// });
//
// paramColLeft.append(newLabel);
//
// // Create input div on the right column
// var newDiv = $('<div/>', {class: 'input-group col-xs-12'});
//
// // Generate solution hint (only use it when parameter active but not
// // valid)
// var invalidValueSign = $('<span/>', {class: 'glyphicon
// glyphicon-exclamation-sign red-tooltip red-icon'});
// invalidValueSign.attr({
// 'data-toggle': 'tooltip',
// 'data-placement': 'bottom',
// title: " " + current.solutionHint
// });
//
// // Generate not-verifiable-sign sign (only use it when parameter not
// // active because of depending parameters)
// var notVerifiableSign = $('<span/>', {class: 'glyphicon
// glyphicon-exclamation-sign red-tooltip red-icon'});
// notVerifiableSign.attr({
// 'data-toggle': 'tooltip',
// 'data-placement': 'bottom',
// title: " " + current.dependsOnNotice
// });
//
// // Now differentiate between extensional and intentional
// if (current.multiplicity != null) {
// var newSelect = $('<select/>', {class: 'form-control extALParameter', id:
// current.name, actionId: current.actionId, componentId: componentId});
//
// // Extract selected value(s)
// var selectedValues = Object.create(null);
// if (current.multiplicity == 'multiple') {
// newSelect.prop('multiple', "multiple");
// newSelect.addClass('multiple-select');
// if (current.values != null) {
// var tmpSelections = arrayStringToSet(current.values);
// $.each(tmpSelections, function (index, current) {
// selectedValues[current] = true;
// })
// }
// } else {
// selectedValues[current.values[0]] = true;
// }
//
// newDiv.append(newSelect);
// paramColRight.append(newDiv);
//
// if (!current.active) {
// newSelect.prop('disabled', true);
// newDiv.addClass('has-error');
// $(paramColLeft).append(notVerifiableSign);
// allOk = false;
// } else {
// $.each(current.range, function (index, current) {
// var newOption = $('<option/>', {text: current, value: current});
// if (current in selectedValues) {
// newOption.prop('selected', true);
// }
// newSelect.append(newOption);
// });
// if (!current.valid) {
// newDiv.addClass('has-error');
// $(paramColLeft).append(invalidValueSign);
// allOk = false;
// }
// }
// } else {
// var newInput = $('<input/>', {
// type: 'text',
// class: 'form-control intALParameter',
// id: current.name,
// val: current.values[0],
// actionId: current.actionId,
// componentId: componentId
// });
// newInput.html(current.name);
// newDiv.append(newInput);
// paramColRight.append(newDiv);
//
// if (!current.active) {
// newInput.prop('disabled', true);
// newDiv.addClass('has-error');
// $(paramColLeft).append(notVerifiableSign);
// allOk = false;
// } else {
// if (!current.valid) {
// newDiv.addClass('has-error');
// $(paramColLeft).append(invalidValueSign);
// allOk = false;
// }
// }
// }
//
// targetElement.append(row);
// });
//
// // Initialize tooltips
// targetElement.find("[data-toggle=tooltip]").tooltip();
// targetElement.find("[data-toggle=popover]").popover();
//
// return allOk;
// }

// function arrayStringToSet(value) {
// if (value.indexOf('[') == 0 && value.lastIndexOf(']') == value.length - 1) {
// value = value.substr(1, value.length - 2);
// }
// return value.split(', ');
// }

function registerParameterHasChanged(param) {
    var result = getParameterStateWithActionId(param);

    successFunction = function() {
        var currentLocation = window.location.href;
        window.location.href = currentLocation;
    };

    // get actionId and componentId, post to controller with actionId and value
    performAction(result.parameterComponentId, result.parameterActionId, result.parameterValue);
}

function getParameterStateWithActionId(current) {
    var name = current.attr('paramName');
    var actionId = current.attr('actionId');
    var componentId = current.attr('componentId');

    // Do not listen on change-event of disabled parameters
    if (current.prop('disabled')) {
        return;
    }

    var value;

    if ($(current).hasClass('subset-parameter')) {
        value = $.map($(current).find('option:selected'), function(option) {
            return option.value;
        });

    } else if ($(current).hasClass('range-enumerable-parameter')) {
    	value = [$(current).find(':selected').val()];
    } else if ($(current).hasClass('text-parameter')) {
    	 value = [current[0].value];
    } else {
    	value = null;
    }
    return {
        parameterName: name,
        parameterValue: value,
        parameterActionId: actionId,
        parameterComponentId: componentId
    };
}

// function getParameterState(parameter) {
// var current = $(parameter);
// var name = current.attr('id');
//
// // Do not listen on change-event of disabled parameters
// if (current.prop('disabled')) {
// return;
// }
//
// var value;
//
// if ($(current).hasClass('select-multiple')) {
// // Only select the actual values of selected options
// value = $.map(current.selectedOptions, function (option) {
// return option.value;
// });
// } else {
// if ($(current).hasClass('extParameter')) {
// value = $(current).find(':selected').val();
// } else {
// value = current.value;
// }
// }
//
// return {
// parameterName: name,
// parameterValue: value
// };
// }


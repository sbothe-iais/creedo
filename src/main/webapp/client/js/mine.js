$(document).ready(function() {
	
	$("#btn-start-mining").on("click", function(e) {
		var $that = $(this);
		var $container = $("#candidate-patterns");
		$container.html("<div class=\"text-center\"><img src=\"client/images/loader.gif\" /></div>");
		e.preventDefault();
		$that.addClass('disabled');
		$.post("mine.htm", function(data) {
			$container.html(data);
			// activate tooltip on patterns
			$container.find("[data-toggle=tooltip]").tooltip();
			
			// make patterns draggable
			$container.find(".pattern").draggable({
				cancel: ".visualization",
				revert: 'invalid',
				appendTo: "body",
				helper: function( event, ui) {
			        var helper = $(this).clone();
			        helper.addClass("being-dragged");
			        helper.css('width', $(this).css('width'));
			        helper.css('height', $(this).css('height'));
			        return helper;
				},
				start: function(evt,ui){
					$(this).css("display","none");
				},
				revert: function(valid){
					if (!valid) {
						$(this).css("display","block");
						return true;
					} else {
						return false;
					}
				}
			});
		}).always(function() {
			$that.removeClass('disabled');
		});
	}).click(); 
	
});
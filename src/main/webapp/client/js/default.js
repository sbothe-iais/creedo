$(document)
		.ready(
				function() {

					window.selectedPatternIdsForFiltering = [];
					// toggle between showing support set only and full data
					$("body").on("click", ".btn-toggle-support-set",
							function(e) {
								e.preventDefault();

								var $pattern = $(this).parents(".pattern");
								// var $icon = $(this).find("span");

								if (isPatternSelectedForFiltering($pattern)) {
									// Is already selected for filtering, should
									// be undone
									unselectPatternForFiltering($pattern);
								} else {
									// Should be selected for filtering
									selectPatternForFiltering($pattern);
								}
							});

					// custom filter to show rows corresponding
					// to the support set of selected pattern
					$.fn.dataTable.ext.search
							.push(function(settings, data, dataIndex) {
								// Iteration through all "data rows"

								if (settings.oInstance.selector !== "#content-table .datatable"
										|| window.selectedPatternIdsForFiltering.length == 0) {
									// we apply filtering only to main table
									return true;
								}

								// Now check if the current data is allowed,
								// meaning that it should
								// be displayed in the table
								return checkDataItemSupportedByAllSelectedPatterns(dataIndex);
							});

					// BEGIN - resizable columns and rows implementation
					var $leftColumn = $(".left-column");
					var $rightColumn = $(".right-column");
					// this 1px is necessary for some browsers,
					// otherwise columns will break after resize
					if ($leftColumn.length > 0) {
						$leftColumn.width($leftColumn.width() - 1);
					}
					if ($rightColumn.length > 0) {
						// just to have fixed width property
						$rightColumn.width($rightColumn.width() - 15);
					}
					var resizeDashboardColumns = function(delta) {
						$leftColumn.width($leftColumn.width() - delta);
						$rightColumn.width($rightColumn.width() + delta);
						window.drawPointCloud();
					};

					$("#column-resizer")
							.draggable(
									{
										axis : "x",
										stop : function(e, ui) {
											resizeDashboardColumns(ui.originalPosition.left
													- ui.position.left);
											$("#column-resizer").css("left",
													"-2px");
										}
									});

					var resizeDashboardRows = function(delta) {
						var $bottomRow = $("#analysis-patterns");
						$bottomRow.height($bottomRow.height() + delta);

						ensureUnscrollableLayout();

						/*
						 * var tables = [ window.mainTable, window.metadataTable ]; $
						 * .each( tables, function(idx, table) { if (table) {
						 * var oSettings = table .fnSettings();
						 * oSettings.oScroll.sY = calcTableHeight() + 'px';
						 * table.fnDraw(); } });
						 */
						window.drawPointCloud();
					};
					$("#row-resizer").draggable(
							{
								axis : "y",
								stop : function(e, ui) {
									resizeDashboardRows(ui.originalPosition.top
											- ui.position.top);
									$("#row-resizer").css("top", "-4px");
								}
							});
					// END - resizable columns and rows implementation

					$("body").on("click", ".btn-remove-pattern", function(e) {
						e.preventDefault();
						var $pattern = $(this).parents(".pattern");
						var patternId = $pattern.prop("id");
						$.post("deletePattern.htm", {
							deletedItem : patternId,
							analysisSessionId : window.analysisSessionId
						}, function() {
							if (isPatternSelectedForFiltering($pattern)) {
								unselectPatternForFiltering($pattern);
							}
							$pattern.hide("slow");
						});
					});

					$("body").on(
							"click",
							".detailed-view",
							function(e) {
								e.preventDefault();
								// var $pattern = $(this).parents(".pattern");
								// var patternId = $pattern.prop("id");
								var link = $(this).prop("href");
								window.open(link, "_blank",
										"scrollbars=1,toolbar=0,location=0,menubar=0,status=0,height="
												+ screen.height + ",width="
												+ screen.width);
							});

					// make analysis patterns sortable
					$("#analysis-patterns").sortable({
						items : "> .pattern",
						tolerance : "pointer"
					});

					// drag drop functionality in mining dashboard
					$("#analysis-patterns")
							.droppable(
									{
										accept : "#candidate-patterns .pattern",
										drop : function(event, ui) {
											var patternId = ui.draggable
													.prop("id");
											$
													.post(
															"promotePattern.htm",
															{
																promotedItem : patternId,
																analysisSessionId : window.analysisSessionId
															},
															function(data) {
																var $oldPatternBeforeMove = $(".pattern[id="
																		+ patternId
																		+ "]");
																var wasSelected = isPatternSelectedForFiltering($oldPatternBeforeMove);

																$(
																		"#analysis-patterns")
																		.append(
																				data);
																ui.draggable
																		.remove();

																if (wasSelected) {
																	var $newPatternAfterMove = $(".pattern[id="
																			+ patternId
																			+ "]");
																	setPatternSelectedForFilteringProperties($newPatternAfterMove);
																}

																bindAnnotationHandlers();
															});
										}
									});

					// enable bootstrap tooltip and popover
					$("[data-toggle=tooltip]").tooltip();
					$("[data-toggle=popover]").popover();

					// calculate remaining height for table considering other
					// elements
					/*
					 * var calcTableHeight = function() { // ensure that there
					 * is no scroll in mining dashboard // need to be adjusted
					 * when there is change in the // design var footerHeight =
					 * $("#footer").height(); var headerHeight =
					 * $("#sub-header").height() + $(".navbar").height(); var
					 * analysisHeight = $("#analysis-patterns").height(); var
					 * theRest = 135; // margins, etc
					 * 
					 * return $(window).height() - headerHeight - analysisHeight -
					 * footerHeight - theRest; };
					 */

					// adjust #candidate-patterns container's height
					var adjustCandidatePatternsHeight = function() {
						var theRest = 143; // margins, etc
						$("#candidate-patterns").css(
								"max-height",
								$(window).height() - $("#header").height()
										- $("#footer").height() - theRest);

					};

					// adjust candidate patterns container height
					// on load
					adjustCandidatePatternsHeight();

					// get current window width
					var currentWindowWidth = $(window).width();

					// automatically resize elements in the dashboard
					// to fit current window height
					var ensureUnscrollableLayout = function() {
						// adjust heights
						var newTableHeight = calcTableHeight() + 'px';
						var tables = [ window.mainTable ];
						// var tables = [ window.mainTable, window.metadataTable
						// ];
						$.each(tables, function(idx, table) {
							if (table) {
								var oSettings = table.fnSettings();
								$('.dataTables_scrollBody').css('height',
										newTableHeight);
								table.fnAdjustColumnSizing();
								table.fnDraw();
							}
						});

						adjustCandidatePatternsHeight();

						// adjust widths
						var newWindowWidth = $(window).width();
						var delta = currentWindowWidth - newWindowWidth;
						$leftColumn.width($leftColumn.width() - delta);
						currentWindowWidth = newWindowWidth;
					};

					// whenever browser window resize
					// re-adjust element heights
					$(window).resize(ensureUnscrollableLayout);

					// render content(dataset) datatable
					window.mainTable = $("#content-table .datatable");
					if (mainTable.length > 0) {
						console.time('datatable render');
						$
								.getJSON(
										// "getDataTable.json",
										// {
										// analysisSessionId :
										// window.analysisSessionId,
										// analyticsDashboardId :
										// window.analyticsDashboardId
										// },
										"getDataTable.json?dataViewContainerId="
												+ window.dataViewContainerId,
										function(data) {
											window.numberOfDataItems = data.length;
											window.mainTable = $("#content-table .datatable");
											mainTable
													.removeClass("hide")
													.dataTable(
															{
																"scrollX" : true,
																"scrollY" : calcTableHeight()
																		+ 'px',
																"scrollCollapse" : true,
																"bSortClasses" : false,
																"bLengthChange" : false,
																"iDisplayLength" : 40,
																"bScrollCollapse" : false,
																"fnDrawCallback" : function() {
																	if (window.fixedColumnsTable != null) {
																		window.fixedColumnsTable
																				.fnRedrawLayout();
																	}
																}
															});
											var fixedColumns = new $.fn.dataTable.FixedColumns(
													mainTable);
											window.fixedColumnsTable = fixedColumns;
											// new
											// $.fn.dataTable.FixedColumns(mainTable);

											/*
											 * Here an index is given to each
											 * row in the table The index
											 * corresponds to the index of the
											 * data received from the server
											 * side
											 */
											$.each(data,
													function(idx, row) {
														mainTable.fnAddData(
																row, false);
													});
											$("#content-table .loader")
													.addClass("hide");
											mainTable.fnDraw();
											fixedColumns.fnUpdate();
											fixedColumns.fnRedrawLayout();
										});
						console.timeEnd('datatable render');

						// load visualization/statistics of column header
						$.get("getColumnTooltips.json", {
							dataViewContainerId : window.dataViewContainerId
						// analysisSessionId : window.analysisSessionId
						}, function(tooltips) {
							var $columns = $(".datatable-column");
							$.each($columns, function(index, column) {
								$(column).tooltip({
									container : "body",
									placement : "bottom",
									title : tooltips[index],
									trigger : "hover",
									html : "true"
								});
							});
						});
					}

					$('#tab-content').on('shown.bs.tab', function(e) {
						window.mainTable.fnDraw();
					});
					$('#tab-point-cloud').on('shown.bs.tab', function(e) {
						window.drawPointCloud();
					});
					// render metadata table
					$('#tab-metadata')
							.on(
									'shown.bs.tab',
									function(e) {
										$target = $(e.target);
										if (!$target.data('tableRendered')) {
											window.metadataTable = $(
													"#content-metadata .datatable")
													.dataTable(
															{
																"scrollY" : calcTableHeight()
																		+ 'px',
																"scrollCollapse" : true,
																"bSortClasses" : false,
																"bLengthChange" : false,
																"bFilter" : false,
																"paging" : false,
																"bScrollCollapse" : false
															});
											$target.data('tableRendered', true);
										}
										window.metadataTable.fnDraw();
									});

					var bindAnnotationHandlers = function() {
						$('.annotation')
								.on(
										'keypress',
										function(e) {
											var code = e.keyCode || e.which;
											if (code == 13) { // Enter keycode
												var annotationText = $(this)
														.val();
												var $pattern = $(this).parents(
														".pattern");
												var patternId = $pattern
														.prop("id");
												console
														.log("Submitting annotation to server...");
												$
														.post(
																"annotatePattern.htm",
																{
																	annotatedPatternId : patternId,
																	annotation : annotationText,
																	analysisSessionId : window.analysisSessionId
																}, function() {
																	/*
																	 * alert("Annotation
																	 * sent");
																	 */
																});
											}
										});
					};

					$('.pattern')
							.click(
									function(e) {
										if ($(this).find(".selectable").length > 0) {

											if ($(this).hasClass(
													"selected-for-rating")) {
												;
											} else {
												$(".selected-for-rating")
														.removeClass(
																"selected-for-rating");

												$(this).addClass(
														"selected-for-rating");

												var $patternSelect = $(this)
														.attr('id');
												$
														.get(
																'ratingdashboard/retrieve_rating.htm',
																{
																	dashboardId : window.analysisSessionId,
																	patternSelect : $patternSelect
																},
																function(data) {
																	var ratings = data
																			.split(",");
																	$(
																			'.btn-group')
																			.each(
																					function(
																							counter) {
																						$(
																								'.ratingOption_'
																										+ counter)
																								.each(
																										function(
																												e) {
																											$(
																													this)
																													.parent()
																													.removeClass(
																															"active");
																											if ($(
																													this)
																													.attr(
																															'id') == ratings[counter]
																													.trim()) {
																												$(
																														this)
																														.parent()
																														.addClass(
																																"active");
																												$(
																														this)
																														.attr(
																																'checked',
																																'checked');
																											}
																										});
																					});
																});
											}
										} else {
											;
										}

									});

					// set the first pattern in the analysis area as default
					$('.ratingPatterns').children().first().trigger('click');

					$('input:radio[class^=ratingOption]').change(
							function() {
								var $patternSelect = $(".selected-for-rating")
										.attr('id');
								var $metricSelect = this.name;
								var $optionSelect = this.id;
								$.post('ratingdashboard/submit_rating.htm', {
									dashboardId : window.analysisSessionId,
									patternSelect : $patternSelect,
									metricSelect : $metricSelect,
									optionSelect : $optionSelect
								}, function(status) {
									if (status != "0") {
										alert(status);
									}
								});
							});

					// set the checkbox in the custom dashboard as buttons.
					/*
					 * $(function() { $("#miningAlgorithmSelect").buttonset();
					 * });
					 */

					$("form#customMiningForm")
							.submit(
									function() {

										var formData = new FormData($(this)[0]);

										/*
										 * The tab that was open when the form
										 * was submitted determines whether to
										 * try to load a datatable from the DB
										 * or to try to load from files.
										 */
										var activeTab = $(this).find(
												'.nav-tabs li.active');

										// 0 = Database, 1 = Files
										var selectedTabIndex = $(this).find(
												'.nav-tabs li')
												.index(activeTab);

										formData.append("selectedTabIndex",
												selectedTabIndex);

										var targetWin = getNewPopUpWindow("loading.htm");

										$
												.ajax({
													url : 'customDash/createCustomDashboard.htm',
													type : 'POST',
													data : formData,
													async : false,
													cache : false,
													contentType : false,
													processData : false,
													success : function(url) {
														targetWin.location
																.replace(url);
														targetWin.focus();
													},
													error : function(request,
															status, error) {
														targetWin.close();
														alert("Status: "
																+ status
																+ ": "
																+ request.responseText);
													}
												});

										return false;
									});

					$('.actionLink')
							.click(
									function(e) {
										e.preventDefault();
										// var abort;

										var actionElement = $(this);

										function resolveAction(actionElement) {

											if (actionElement
													.attr('resultTarget') == "POPUP") {
												performActionAndAwaitResultInNewPopUp(
														actionElement
																.attr('frameId'),
														actionElement
																.attr('linkId'));
											} else {
												var successFunction;
												if (actionElement
														.attr('resultTarget') == "REFRESH") {
													successFunction = function(
															url) {
														var currentLocation = window.location.href;
														window.location.href = currentLocation;
													};
												} else if (actionElement
														.attr('resultTarget') == "REDIRECT") {
													successFunction = function(
															url) {
														window.location.href = url
													};
												} else if (actionElement
														.attr('resultTarget') == "CLOSE") {
													successFunction = function() {
														window.close();
													};
												}

												performAction(
														actionElement
																.attr('frameId'),
														actionElement
																.attr('linkId'),
														[ actionElement
																.attr('actionParameter') ],
														successFunction,
														function(xhr) {
															showMessage(xhr.responseText);
														});

											}
										}

										if ($(this).hasClass("confirmAction")) {
											bootbox
													.confirm(
															"Are you sure?",
															function(result) {
																if (result) {
																	resolveAction(actionElement);
																}
															});
										} else {
											resolveAction(actionElement);
										}

									});

					$('.toggleActiveAction')
							.click(
									function(e) {
										e.preventDefault();
										var userId = $(this).attr('userid');
										$
												.post(
														'toggleActive.htm',
														{
															userId : userId
														},
														function(activeStatus) {
															alert("Status toggled. New status: "
																	+ activeStatus
																	+ " Reload to see changes in effect.");
														})
									});

					// Init Custom dashboard JS functions
					initCustomDashboardJs();

					// Start pinging the server
					pingToServer();
				});

/**
 * Display an error message below the mine-button
 * 
 * @param stackTrace
 *            Error message to be displayed
 */
function showMiningError(stackTrace) {
	$("#mining-error-notification-icon").click(function() {
		$("#mm-info-modal-content").html(stackTrace);
		$("#mm-info-modal").modal();
	});
	$("#mining-error-notification").removeClass("hidden");
};

function isPatternSelectedForFiltering($pattern) {
	var $icon = $pattern.find(".btn-toggle-support-set span");
	return $icon.hasClass("glyphicon-fullscreen");
}

function selectPatternForFiltering($pattern) {
	var id = $pattern.prop("id");
	var supportSet = ($pattern.data("support-set") || []);

	window.selectedPatternIdsForFiltering.push(id);
	if (supportSet.length == 0) {
		$.getJSON("getSupportSet.json", {
			patternId : id,
			analysisSessionId : window.analysisSessionId
		}, function(data) {
			$pattern.data("support-set", data);
			redrawFilteringDependentViews();
		});
	} else {
		redrawFilteringDependentViews();
	}
	setPatternSelectedForFilteringProperties($pattern);
}

function setPatternSelectedForFilteringProperties($pattern) {
	var $icon = getPatternToggleSupportSetIcon($pattern);
	$icon.removeClass("glyphicon-screenshot");
	$icon.addClass("glyphicon-fullscreen");
	$pattern.addClass("selected-for-data-filter");
}

function unselectPatternForFiltering($pattern) {
	var id = $pattern.prop("id");

	window.selectedPatternIdsForFiltering.splice($.inArray(id,
			window.selectedPatternIdsForFiltering), 1);
	window.mainTable.fnDraw();
	window.drawPointCloud();

	unsetPatternSelectedForFilteringProperties($pattern);
}

function unsetPatternSelectedForFilteringProperties($pattern) {
	var $icon = getPatternToggleSupportSetIcon($pattern);
	$icon.removeClass("glyphicon-fullscreen");
	$icon.addClass("glyphicon-screenshot");
	$pattern.removeClass("selected-for-data-filter");
}

function getPatternToggleSupportSetIcon($pattern) {
	return $pattern.find(".btn-toggle-support-set span");
}

/**
 * Checks whether the given data index (row index) is contained in all support
 * sets of the currently selected patterns.
 */
function checkDataItemSupportedByAllSelectedPatterns(dataIndex) {
	var dataIsAllowed = true;
	var i = 0;

	if (window.selectedPatternIdsForFiltering == null
			|| window.selectedPatternIdsForFiltering.length == 0) {
		return false;
	}

	while (dataIsAllowed && i < window.selectedPatternIdsForFiltering.length) {
		var supportSet = $("#" + window.selectedPatternIdsForFiltering[i])
				.data("support-set");
		if ($.inArray(dataIndex, supportSet) == -1) {
			dataIsAllowed = false;
		}
		i++;
	}

	return dataIsAllowed;
}

function getGlobalSupportSet() {
	var globalSupportSet = [];
	for (var i = 0; i < window.numberOfDataItems; i++) {
		if (checkDataItemSupportedByAllSelectedPatterns(i)) {
			globalSupportSet.push(i);
		}
	}
	return globalSupportSet;
}

function calcTableHeight() {
	// ensure that there is no scroll in mining dashboard
	// need to be adjusted when there is change in the
	// design
	var footerHeight = $("#footer").height();
	var headerHeight = $("#sub-header").height() + $(".navbar").height();
	var analysisHeight = $("#analysis-patterns").height();
	var theRest = 135; // margins, etc

	var total = $(window).height() - headerHeight - analysisHeight
			- footerHeight - theRest;

	return total;
}

function redrawFilteringDependentViews() {
	window.mainTable.fnDraw();
	window.drawPointCloud();
}

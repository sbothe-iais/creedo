/*USE `creedo`;*/

INSERT INTO datatables(id) VALUES(99);
UPDATE datatables SET name="MobileNet" WHERE id = 99;
UPDATE datatables SET data="0001;0.1;0.2;0.3;0.4;0.5;0.6
0002;0.1;0.2;0.3;0.4;0.5;0.6
0003;0.2;0.3;0.4;0.5;0.6;0.7
0004;0.2;0.1;0.3;0.5;0.9;0.6
0005;0.2;0.2;0.3;0.5;0.5;0.6
0006;0.1;0.2;0.3;0.5;0.5;0.6
0007;0.2;0.2;0.3;0.5;0.5;0.9
" WHERE id = 99 ;
UPDATE datatables SET attributes ="cellid;name;ID of Phone cell
20130101-00;numeric;Value at time t0
20130101-01;numeric;Value at time t1
20130101-02;numeric;Value at time t2
20130101-03;numeric;Value at time t3
20130101-04;numeric;Value at time t4
20130101-05;numeric;Value at time t5"
WHERE id = 99;

